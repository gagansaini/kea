package com.kickoffelite.model;

public class DailogLevelModel
{
    String id,level_name,description,is_locked,total_videos,seen_videos,percentage_seen;

    public DailogLevelModel(String id, String level_name, String description, String is_locked, String total_videos, String seen_videos, String percentage_seen) {
        this.id = id;
        this.level_name = level_name;
        this.description = description;
        this.is_locked = is_locked;
        this.total_videos = total_videos;
        this.seen_videos = seen_videos;
        this.percentage_seen = percentage_seen;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLevel_name() {
        return level_name;
    }

    public void setLevel_name(String level_name) {
        this.level_name = level_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIs_locked() {
        return is_locked;
    }

    public void setIs_locked(String is_locked) {
        this.is_locked = is_locked;
    }

    public String getTotal_videos() {
        return total_videos;
    }

    public void setTotal_videos(String total_videos) {
        this.total_videos = total_videos;
    }

    public String getSeen_videos() {
        return seen_videos;
    }

    public void setSeen_videos(String seen_videos) {
        this.seen_videos = seen_videos;
    }

    public String getPercentage_seen() {
        return percentage_seen;
    }

    public void setPercentage_seen(String percentage_seen) {
        this.percentage_seen = percentage_seen;
    }
}
