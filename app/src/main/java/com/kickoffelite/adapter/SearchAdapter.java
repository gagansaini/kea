package com.kickoffelite.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.kickoffelite.R;
import com.kickoffelite.activity.DrillThirdActivity;
import com.kickoffelite.activity.PlayVideoActivity;
import com.kickoffelite.activity.SwipeTechActivity;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.model.SearchModel;

import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.MyViewHolder> {

    private List<SearchModel> searchList;
    Context context;
    // MxVideoPlayerWidget videoPlayerWidget;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView txt_title, txt_videogroup;
        SelectableRoundedImageView img_video;
        RelativeLayout mainlayout;

        public MyViewHolder(View view)
        {
            super(view);
            // videoPlayerWidget = (MxVideoPlayerWidget) view.findViewById(R.id.videoplayer);
            txt_title = (TextView) view.findViewById(R.id.txt_title);
            txt_videogroup = (TextView) view.findViewById(R.id.txt_videogroup);
            mainlayout=(RelativeLayout)view.findViewById(R.id.mainlayout);
            Constants.overrideFonts(context,mainlayout);
        }
    }

    public SearchAdapter(Context context,List<SearchModel> searchList) {
        this.searchList = searchList;
        this.context = context;
    }

    @Override
    public SearchAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_recycler_item, parent, false);

        return new SearchAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SearchAdapter.MyViewHolder holder, int position) {
        final SearchModel movie = searchList.get(position);
        holder.txt_videogroup.setVisibility(View.VISIBLE);
        holder.txt_title.setText(movie.getName());
        if(movie.getSearchtype().equalsIgnoreCase("list"))
        {
            holder.txt_videogroup.setVisibility(View.INVISIBLE);
        }
        holder.txt_videogroup.setText(movie.getSearchtype());
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(movie.getSearchtype().equalsIgnoreCase("Drills"))
                {
                    String id= movie.getId();
                    String desc=movie.getDesc();
                    String image=movie.getImage();
                    String session=movie.getCount();
                    String title=movie.getName();
                    Log.e( "drilltags: ", id.toString());
                    Intent intent=new Intent(context,DrillThirdActivity.class);
                    intent.putExtra("name","drill");
                    intent.putExtra("id",id);
                    intent.putExtra("desc",desc);
                    intent.putExtra("image",image);
                    intent.putExtra("session",session);
                    intent.putExtra("title",title);
                    context.startActivity(intent);
                    ((Activity)context).finish();
                }

                else if(movie.getSearchtype().equalsIgnoreCase("Techniques"))
                {
                    String id= movie.getId();
                    String desc=movie.getDesc();
                    String image=movie.getImage();
                    String session=movie.getCount();
                    String title=movie.getName();
                    Log.e( "tech: ", id.toString());
                    Intent intent=new Intent(context,SwipeTechActivity.class);
                    intent.putExtra("name","tech");
                    intent.putExtra("id",id);
                    intent.putExtra("desc",desc);
                    intent.putExtra("image",image);
                    intent.putExtra("session",session);
                    intent.putExtra("title",title);
                    intent.putExtra(Constants.TechImage,image);
                    context.startActivity(intent);
                    ((Activity)context).finish();
                }

                else if(movie.getSearchtype().equalsIgnoreCase("list"))
                {
//                    String text=movie.getName();
//                    Intent intent=new Intent(context,SearchActivity.class);
//                    intent.putExtra("name",text);
//                    context.startActivity(intent);
                }

                else
                {
                    String id= movie.getId();
                    String desc=movie.getDesc();
                    String image=movie.getImage();
                    String session=movie.getCount();
                    String title=movie.getName();
                    String url=movie.getUrl();
                    Log.e( "video: ", id.toString());
                    Intent intent=new Intent(context,PlayVideoActivity.class);
                    intent.putExtra(Constants.name,title);
                    intent.putExtra(Constants.video_id,id);
                    intent.putExtra(Constants.desc,desc);
                    intent.putExtra(Constants.totaltime,image);
                    intent.putExtra(Constants.level_name,session);
                    intent.putExtra(Constants.image,movie.getImage());
                    intent.putExtra(Constants.url,url);
                    intent.putExtra(Constants.VideoType,"video");
                    intent.putExtra(Constants.video_status,movie.getVideo_status());
                    // intent.putExtra("title",title);
                    intent.putExtra("drilltagslist",movie.getDrill_id_tags());
                    intent.putExtra("techniquetagslist",movie.getTechnique_id_tags());
                    context.startActivity(intent);
                    ((Activity)context).finish();
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return searchList.size();
    }
}