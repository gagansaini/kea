package com.kickoffelite.tabactivityactivities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kickoffelite.R;
import com.kickoffelite.activity.ProfileActivity;
import com.kickoffelite.activity.SearchActivity;
import com.kickoffelite.adapter.DrillMainAdapter;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.model.DrillMainModel;
import com.kickoffelite.helper.GlobalConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class DrillsFragment extends AppCompatActivity {

    Dialog progressDialog;
    private RecyclerView recyclerView;
    private DrillMainAdapter mAdapter;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    AlertDialog builder;
    TextView txt_skill;
    RelativeLayout mainlayout;
    ImageView img_search;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_drills);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.appcolor));
        }
        init();
        setListener();
    }

    public void init()
    {
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        img_search=(ImageView)findViewById(R.id.img_search);
        txt_skill=(TextView)findViewById(R.id.txt_skill);
        mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
//        recyclerView.setItemAnimator(new DefaultItemAnimator());
//        mAdapter = new DrillMainAdapter(DrillsFragment.this,movieList);
//        recyclerView.setAdapter(mAdapter);
        if(Constants.isNetworkAvailable(getApplicationContext()))
        {
            progressBarDialog();
            get_All_Drills();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
        }

    }

    public void setListener()
    {
        txt_skill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(intent);
            }
        });

        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        Log.e( "onResume: ","resume" );
        //get_All_Drills();
        if(sp.getString(Constants.level_name,"").equalsIgnoreCase(""))
        {
            txt_skill.setText("Beginner");
        }
        else
        {
            txt_skill.setText(sp.getString(Constants.level_name,""));
        }
       // progressBarDialog();
    }

    @Override
    protected void onRestart()
    {
        Log.e( "onRestart: ","onRestart" );

        if(Constants.isNetworkAvailable(getApplicationContext()))
        {
            get_All_Drills();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
        }

        super.onRestart();
    }

    @Override
    public void onBackPressed() {
        alertDialogExit();
    }

    //Api

    public void get_All_Drills()
    {
         final List<DrillMainModel> movieList = new ArrayList<>();
        final String URL = GlobalConstants.DRILLSLIST+sp.getString(Constants.id,"") ;
        StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
               Log.e("response order", s);
                String m=s.substring(s.indexOf("{"),s.length());
                //Log.e("m value",m);

                try {
                    JSONObject jsob = new JSONObject(m);
                    Log.e("jsob", jsob.toString());
                    String response = jsob.getString("status");
                    if (response.equals("1"))
                    {
                        progressDialog.dismiss();
                        Log.e("status", response);
                        JSONObject jsonObject=jsob.getJSONObject("data");
                        String level_name=jsonObject.getString("level_name");
                        JSONArray jsonarr = jsonObject.getJSONArray("drill");
                        String complete_session="";
                        if(movieList.size()==0) {
                            for(int i=0;i<jsonarr.length();i++)
                            {
                                JSONObject jsonobj=jsonarr.getJSONObject(i);
                                String id = jsonobj.getString("id");
                                String name = jsonobj.getString("name");
                                String image = jsonobj.getString("image");
                                String description = jsonobj.getString("description");
                                String level_id=jsonobj.getString("level_id");
                                String sublevel_id=jsonobj.getString("sublevel_id");
                                String session_count = jsonobj.getString("session_count");
                                String is_locked = jsonobj.getString("is_locked");
                                complete_session=jsonobj.getString("complete_session");
                                //  String url="http://worksdelight.com/kickoff/Uploads/Users/"+sp.getString(Constants.id,"")+"/"+category_image;
                                Log.e("url drill",image);
                                movieList.add(new DrillMainModel(id,name, image, description, level_id,sublevel_id,session_count,is_locked,complete_session,level_name));
                            }
                        }
                        Log.e("listsize: ", String.valueOf(movieList.size()));

                        mAdapter = new DrillMainAdapter(DrillsFragment.this, movieList);
                        recyclerView.setAdapter(mAdapter);
                        mAdapter.notifyDataSetChanged();

                    }
                    else
                    {

                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
                Toast.makeText(DrillsFragment.this, "Some error occurred -> " + volleyError, Toast.LENGTH_LONG).show();

            }
        }) ;
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(DrillsFragment.this);
        rQueue.add(request);
    }

    public void progressBarDialog()
    {
        progressDialog= new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    @SuppressLint("ResourceAsColor")
    public void alertDialogExit()
    {
        final Dialog openDialog = new Dialog(DrillsFragment.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.background));
        //openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.mark_dilaog);
        RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        TextView txt_cancel=(TextView)openDialog.findViewById(R.id.txt_cancel);
        TextView txt_remove=(TextView)openDialog.findViewById(R.id.txt_remove);
        TextView txt_alertmsg=(TextView)openDialog.findViewById(R.id.txt_alertmsg);
        txt_alertmsg.setText("Do you really want to exit ?");
        txt_remove.setText("Exit");
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDialog.cancel();
            }
        });
        txt_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                openDialog.cancel();
            }
        });
        openDialog.show();
    }
}
