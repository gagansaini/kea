package com.kickoffelite.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.kickoffelite.R;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.model.DailogLevelModel;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.volley.VolleyMultipartRequest;
import com.onesignal.OneSignal;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.apache.http.HttpEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

//fb App ID: 225538098035193
public class RegisterActivity extends Activity implements View.OnClickListener,DatePickerDialog.OnDateSetListener {

    ArrayList<DailogLevelModel> dialoglevel = new ArrayList<>();
    HashMap<String,String> levels=new HashMap<>();
    ArrayList<HashMap<String,String>> levellist = new ArrayList<>();
    TextView txt_sign,edt_date;
    EditText edt_name, edt_password, edt_email, edt_phone;
    ImageView img_date;
    CircleImageView img_profile;
    Button btn_fb2, btn_create;
    String name = "", email = "", password = "", phone = "", dob = "", image = "";
    RelativeLayout bday;
    Dialog progressDialog;
    
    HttpEntity resEntity;
    JSONObject jsonObject = new JSONObject();
    Uri savedImageURI;
    File auxFile;
    String id, firstname, lastname, fbemail, fbbday;
    BottomSheetDialog bsd;
    File file1;

    //Profile pic
    Bitmap myBitmap;
    Uri picUri;
    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();
    private final static int ALL_PERMISSIONS_RESULT = 107;
    private int GALLERY = 1, CAMERA = 2,FB=3;
    AlertDialog builder;
     Bitmap bitmap;
    String selectedImagePath = "";
    public static final int RequestPermissionCode = 7;
    //DatePicker
    Calendar calendar ;
    DatePickerDialog datePickerDialog ;
    int Year, Month, Day ;
    String date,android_id;

    //fb
    CallbackManager callbackManager;
    private static final String EMAIL = "email";
    private static final String ProPic = "propic";
    LoginButton loginButton;
    AccessToken accessToken;
    static String encoded = null;
    static String imageURL = null;

    SharedPreferences sp;
    SharedPreferences.Editor ed;
    RelativeLayout mainlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }
        setContentView(R.layout.activity_register);
         android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        printKeyHash(RegisterActivity.this);
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        init();
        setListener();

        // printKeyHash(this);
    }

    public void init() {
        txt_sign = (TextView) findViewById(R.id.txt_signin);
        loginButton = (LoginButton) findViewById(R.id.btn_fb);
        btn_fb2 = (Button) findViewById(R.id.btn_fb2);
        btn_create = (Button) findViewById(R.id.btn_create);
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_password = (EditText) findViewById(R.id.edt_password);
        edt_name = (EditText) findViewById(R.id.edt_name);
        //edt_phone = (EditText) findViewById(R.id.edt_phone);
        edt_date = (TextView) findViewById(R.id.edt_date);
        img_date=(ImageView)findViewById(R.id.img_date);
        bday = (RelativeLayout) findViewById(R.id.rel9);
        img_profile=(CircleImageView)findViewById(R.id.img_profile);
        edt_email.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
    }

    public void setListener() {
        txt_sign.setOnClickListener(this);
        btn_fb2.setOnClickListener(this);
        loginButton.setOnClickListener(this);
        btn_create.setOnClickListener(this);
        bday.setOnClickListener(this);
        edt_date.setOnClickListener(this);
        img_date.setOnClickListener(this);
        img_profile.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_signin:
                Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(i);
                finish();
                break;

            case R.id.btn_fb:
                //LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
                break;
            case R.id.btn_fb2:
                if(Constants.isNetworkAvailable(RegisterActivity.this))
                {
                    facebook();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.rel9:

                break;
            case R.id.btn_create:
                email = edt_email.getText().toString();
                name = edt_name.getText().toString();
                password = edt_password.getText().toString();
                phone = "";
                dob = date;
                Validation(email, name, password, phone);
                break;
            case R.id.edt_date:
                birthDialog();
                break;
            case R.id.img_date:
                birthDialog();
                break;
            case R.id.img_profile:
                if (CheckingPermissionIsEnabledOrNot())
                {
                    dailog();
                }

                else
                {
                    RequestMultiplePermission();
                }
                break;
        }
    }

    //Validation

    public void Validation(String email, String name, String password, String phone) {
        if (name.equals("")) {
            edt_name.setError("name required");
        } else if (name.length() < 3) {
            edt_name.setError("name must contain 3 letters");
        } else if (email.equals("")) {
            edt_email.setError("email required");
        } else if (!Constants.isValid(email)) {
            edt_email.setError("email not valid");
        }
        else if (password.equals("")) {
            edt_password.setError("password required");
        } else if (password.length() < 6) {
            edt_password.setError("password must be atleast of 6 characters!");
        }
//        else if (phone.length() < 10) {
//            edt_phone.setError("phone invalid");
//        }
//        else if (selectedImagePath.equals("")) {
//            Toast.makeText(getApplicationContext(),"Please select image",Toast.LENGTH_SHORT).show();
//        }

        else {
            //Api
            Log.e("registerdate: ", name + "," + email + "," + password + "," + dob + "," + phone);
            progressBarDialog();
            file1 = new File(selectedImagePath);
            if(Constants.isNetworkAvailable(RegisterActivity.this))
            {
                SignupApi();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
            }

//            new Thread(null, address_request, "")
//                    .start();
        }
    }

    //Facebook
    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getApplicationContext().getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }
        return key;
    }

    public void facebook() {
        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email","user_birthday"));
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday"));
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("onSuccess: ", loginResult.toString());
                accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();
                progressBarDialog();
                fbgetProfile(accessToken);
//                Intent ii = new Intent(getApplicationContext(), HomeActivity.class);
//                startActivity(ii);
            }

            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException error) {

            }
        });

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//    }

    //fb getProfileMethod
    public void fbgetProfile(AccessToken accessToken)
    {
        GraphRequest request = GraphRequest.newMeRequest(accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e("getprofile: ", response.toString());
                        JSONObject js = new JSONObject();
                        try {
                            if (object.has("name")) {
                                js.put(Constants.name, object.getString("name"));
                            }
                            if (object.has("email")) {
                                js.put(Constants.email, object.getString("email"));
                            }
                            if (object.has("birthday")) {
                                js.put(Constants.date_of_birth, object.getString("birthday"));
                                Log.e("date_of_birth: ","" );
                            }
                            else
                            {
                                js.put(Constants.date_of_birth,"");
                                Log.e("date_of_birth22 ","" );
                            }
                            if (object.has("id")) {
                                js.put(Constants.token, object.getString("id"));
                                id=object.getString("id");
                                Log.e("id: ",object.getString("id") );
                            }

//                            if(object.has("picture"))
//                            {
//                                js.put(Constants.image, getFacebookProfilePicture(id).toString());
//                                Log.e("image: ", getFacebookProfilePicture(id).toString() );
//                            }
//                            else
//                            {
//                                js.put(Constants.image,"");
//                                Log.e("image22", "" );
//                            }
//                            Log.e("image: ", getFacebookProfilePicture(id).toString() );
                            //getFacebookProfilePicture(id);
                           // imageURL = new URL("https://graph.facebook.com/" + id + "/picture?type=large");
                            imageURL = "https://graph.facebook.com/" + id + "/picture?type=large";
                            // js.put(Constants.phone_no, "4567890876");
                            js.put(Constants.social_type, "facebook");
                            js.put(Constants.image,imageURL);
                            Log.e("setProfileToView: ", js.toString());
                            regfb(js);

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,birthday");//,email,gender,birthday"

        request.setParameters(parameters);
        request.executeAsync();
    }

    public static String getFacebookProfilePicture(String userID){

        Bitmap bitmap = null;



        return encoded;
    }

    //Api

    public void regfb(final JSONObject json1)
    {
        String URL=GlobalConstants.SOCIALLOGIN;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s) {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    String message = s.getString("message");
                    if(status.equalsIgnoreCase("1")) {
                        progressDialog.dismiss();
                        JSONObject json = s.getJSONObject("data");
                        String id = json.getString("id");
                        String level_id=json.getString("level_id");
                       // String level_name=json.getString("level_name");
                        String name=json.getString("name");
                        String date_of_birth=json.getString("date_of_birth");
                        String email=json.getString("email");
                        String gender=json.getString("gender");
                        String address=json.getString("address");
                        String image=json.getString("image");
                        String push_notification=json.getString("push_notification");
                        String user_level_for_api=json.getString("user_level_for_api");
                        // String level_name=json.getString("level_name");
                        // ed.putString(Constants.level_name,level_name);
                        JSONArray jsarr=json.getJSONArray("available_level");
                        for(int i=0;i<jsarr.length();i++)
                        {
                            JSONObject jsobj=jsarr.getJSONObject(i);
                            String idd=jsobj.getString("id");
                            String levelname=jsobj.getString("level_name");
                            String description=jsobj.getString("description");
                            String is_locked=jsobj.getString("is_locked");
                            String total_videos=jsobj.getString("total_videos");
                            String seen_videos=jsobj.getString("seen_videos");
                            String percentage_seen=jsobj.getString("percentage_seen");
                            levels.put("id",idd);
                            levels.put("level_name",levelname);
                            levels.put("description",description);
                            levels.put("is_locked",is_locked);
                            levels.put("total_videos",total_videos);
                            levels.put("seen_videos",seen_videos);
                            levels.put("percentage_seen",percentage_seen);
                            levellist.add(levels);
                            dialoglevel.add(new DailogLevelModel(idd, levelname,description,is_locked,total_videos,seen_videos,percentage_seen));
                            levels=new HashMap<>();
                        }
                        Gson gson = new Gson();
                        String json1 = gson.toJson(dialoglevel);
                        ed.putString("levels", json1);
                        ed.putString(Constants.pushnotification,push_notification);
                        ed.putString(Constants.image,image);
                        ed.putString(Constants.gender,gender);
                        ed.putString(Constants.address,address);
                        ed.putString(Constants.date_of_birth,date_of_birth);
                        ed.putString(Constants.email,email);
                        ed.putString(Constants.level_id,level_id);
                        ed.putString(Constants.id, id);
                        ed.putString(Constants.name, name);
                        //ed.putString(Constants.level_name, level_name);
                        ed.putString(Constants.regflag, "true");
                        ed.putString(Constants.loginflag, "true");
                        ed.putString(Constants.userapilevel,user_level_for_api);
                        ed.commit();
                        Log.e("onResponse: ", s.toString());
                        Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                        Intent ii = new Intent(getApplicationContext(), WelcomeActivity.class);
                        OneSignalinit();
                        startActivity(ii);
                        finish();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(RegisterActivity.this,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
               // headers.put(Constants.image, imageURL+"");
               // Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(RegisterActivity.this);
        rQueue.add(request);
    }

    ///RegisterApi
    public void SignupApi() {
        String URL = GlobalConstants.SIGNUP;
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                            String resultResponse = new String(response.data);
                            Log.e("onResponse: ",resultResponse );
                            try {
                                JSONObject result = new JSONObject(resultResponse);
                                String status = result.getString("status");
                                String message = result.getString("message");
                            if(status.equalsIgnoreCase("1"))
                            {
                                progressDialog.dismiss();
                                JSONObject jsonobj=result.getJSONObject("data");
                                String id=jsonobj.getString("id");
                                String name=jsonobj.getString("name");
                                String level_id=jsonobj.getString("level_id");
                                String level_name=jsonobj.getString("level_name");
                                String image=jsonobj.getString("image");
                                String date_of_birth=jsonobj.getString("date_of_birth");
                                String email=jsonobj.getString("email");
                                String gender=jsonobj.getString("gender");
                                String address=jsonobj.getString("address");
                                String push_notification=jsonobj.getString("push_notification");
                                String user_level_for_api=jsonobj.getString("user_level_for_api");
                                // String level_name=jsonobj.getString("level_name");
                                // ed.putString(Constants.level_name,level_name);
                                JSONArray jsarr=jsonobj.getJSONArray("available_level");
                                for(int i=0;i<jsarr.length();i++)
                                {
                                    JSONObject jsobj=jsarr.getJSONObject(i);
                                    String idd=jsobj.getString("id");
                                    String levelname=jsobj.getString("level_name");
                                    String description=jsobj.getString("description");
                                    String is_locked=jsobj.getString("is_locked");
                                    String total_videos=jsobj.getString("total_videos");
                                    String seen_videos=jsobj.getString("seen_videos");
                                    String percentage_seen=jsobj.getString("percentage_seen");
                                    levels.put("id",idd);
                                    levels.put("level_name",levelname);
                                    levels.put("description",description);
                                    levels.put("is_locked",is_locked);
                                    levels.put("total_videos",total_videos);
                                    levels.put("seen_videos",seen_videos);
                                    levels.put("percentage_seen",percentage_seen);
                                    levellist.add(levels);
                                    dialoglevel.add(new DailogLevelModel(idd, levelname,description,is_locked,total_videos,seen_videos,percentage_seen));
                                    levels=new HashMap<>();
                                }
                                Gson gson = new Gson();
                                String json1 = gson.toJson(dialoglevel);
                                ed.putString("levels", json1);
                                ed.putString(Constants.pushnotification,push_notification);
                                ed.putString(Constants.gender,gender);
                                ed.putString(Constants.address,address);
                                ed.putString(Constants.date_of_birth,date_of_birth);
                                ed.putString(Constants.email,email);
                                ed.putString(Constants.name, name);
                                ed.putString(Constants.level_id,level_id);
                                ed.putString(Constants.level_name,level_name);
                                ed.putString(Constants.id,id);
                                ed.putString(Constants.loginflag,"true");
                                ed.putString(Constants.image,image);
                                ed.putString(Constants.userapilevel,user_level_for_api);
                                ed.commit();

                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                                OneSignalinit();
                                Intent ii = new Intent(getApplicationContext(), WelcomeActivity.class);
                                startActivity(ii);
                                finish();

                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }
                                //progressDialog.dismiss();
                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof NetworkError) {
                        } else if (error instanceof ServerError) {
                        } else if (error instanceof AuthFailureError) {
                        } else if (error instanceof ParseError) {
                        } else if (error instanceof NoConnectionError) {
                        } else if (error instanceof TimeoutError) {
                            Toast.makeText(getApplicationContext(),
                                    "Oops. Timeout error!",
                                    Toast.LENGTH_LONG).show();
                        }
                        Log.e("onErrorResponse: ", error.toString());
                       // Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(Constants.name, name);
                params.put(Constants.email,email);
                params.put(Constants.password,password);
                params.put(Constants.date_of_birth,dob);
                return params;
            }

            /*
            * Here we are passing image by renaming it with a unique name
            * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                //Log.e("getByteData: ",String.valueOf(bitmap) );
                if(bitmap != null)
                {
                    long imagename = System.currentTimeMillis();
                    params.put(Constants.image, new DataPart(imagename + ".png",getFileDataFromDrawable(bitmap)) );
                }
                else
                {
                }
                return params;
            }
        };
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public void progressBarDialog()
    {
        progressDialog= new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    //
    public void birthDialog()
    {
        Calendar now = Calendar.getInstance();
        DatePickerDialog datepickerdialog = DatePickerDialog.newInstance(
                RegisterActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        datepickerdialog.setThemeDark(true); //set dark them for dialog?
        datepickerdialog.vibrate(true); //vibrate on choosing date?
        datepickerdialog.dismissOnPause(true); //dismiss dialog when onPause() called?
        datepickerdialog.showYearPickerFirst(false); //choose year first?
        datepickerdialog.setAccentColor(Color.parseColor("#FEDC18")); // custom accent color
        datepickerdialog.setTitle("Please select a date"); //dialog title
        datepickerdialog.show(getFragmentManager(), "Datepickerdialog"); //show dialog
    }


    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int dayOfMonth) {
         date =dayOfMonth + "-" + (++month) + "-" + year;
         edt_date.setText(date);
        Log.e("onDateSet: ",date );
    }

    public void dailog() {
        bsd = new BottomSheetDialog(RegisterActivity.this);
        bsd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bsd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        bsd.setContentView(R.layout.camera_dialog);
        bsd.show();
        onclick();

    }

    public void onclick() {
        LinearLayout camera, gallery,cancel_layout;

        camera = (LinearLayout) bsd.findViewById(R.id.camera_layout);
        gallery = (LinearLayout) bsd.findViewById(R.id.gallery_layout);
        cancel_layout=(LinearLayout) bsd.findViewById(R.id.cancel_layout);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePhotoFromCamera();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choosePhotoFromGallary();
            }
        });

        cancel_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bsd.dismiss();
            }
        });
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {

            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, CAMERA);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY)
        {
            if (data != null) {
                final Uri contentURI = data.getData();
                try {
                     Bitmap  bit = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                     bitmap = Bitmap.createScaledBitmap(bit, 120, 120, false);
                     Log.e( "onActivityResult: ",bitmap+"" );
                     // String path = saveImage(bitmap);
                     //  Toast.makeText(Edit_Profile_Activity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    builder = new AlertDialog.Builder(RegisterActivity.this).setMessage("Do You Want To Upload this Photo ?")
                            .setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    img_profile.setImageBitmap(bitmap);
                                       //   getStringFromBitmap(bitmap);
                                    bsd.dismiss();
                                    try
                                    {
                                        selectedImagePath=getFilePath(RegisterActivity.this,contentURI);
                                       // Toast.makeText(getApplicationContext(),selectedImagePath,Toast.LENGTH_SHORT).show();
                                    }
                                    catch (URISyntaxException e) {
                                        e.printStackTrace();
                                    }

                                }

                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub

                                    builder.dismiss();
                                }
                            }).setIcon(R.mipmap.ic_launcher).show();


                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(RegisterActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }
        }

        else if (requestCode == CAMERA) {
            bitmap = (Bitmap) data.getExtras().get("data");
            Log.e( "onActivityResult: ",bitmap+"" );
            ///////////
            builder = new AlertDialog.Builder(RegisterActivity.this).setMessage("Do You Want To Upload this Photo ?")
                    .setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            img_profile.setImageBitmap(bitmap);
                            //////////
                            // getStringImage(thumbnail);
                            //NOw storing String to SharedPreferences
                            // selectedImagePath=saveImage(thumbnail);
                         //   img_profile.setImageBitmap(bitmap);
                            //   getStringFromBitmap(bitmap);
                            bsd.dismiss();
                        }

                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub

                            builder.dismiss();
                        }
                    }).setIcon(R.mipmap.ic_launcher).show();
            /////////

        }

       // facebook
        else
        {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

    }

    public String getFilePath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }


    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    public String ImageToString(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArray=byteArrayOutputStream .toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public boolean CheckingPermissionIsEnabledOrNot()
    {
        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA);
        int SecondPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), INTERNET);
        int ThirdPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int ForthPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int FifthPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_PHONE_STATE);


        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                SecondPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThirdPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ForthPermissionResult == PackageManager.PERMISSION_GRANTED &&
                FifthPermissionResult == PackageManager.PERMISSION_GRANTED ;
    }

    private void RequestMultiplePermission()

    {
        // Creating String Array with Permissions.
        ActivityCompat.requestPermissions(RegisterActivity.this, new String[]
                {
                        Manifest.permission.CAMERA,
                        INTERNET,
                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE,
                        READ_PHONE_STATE

                }, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        switch (requestCode)
        {
            case RequestPermissionCode:

                if (grantResults.length > 0)
                {
                    boolean CameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean Internet = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean ReadExternalStorage = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean WriteExternalStorage = grantResults[3] == PackageManager.PERMISSION_GRANTED;
                    boolean ReadPhoneState = grantResults[4] == PackageManager.PERMISSION_GRANTED;

                    if (CameraPermission && Internet && ReadExternalStorage && WriteExternalStorage && ReadPhoneState)
                    {
                        dailog();
                    }

                    else
                    {
                       // Toast.makeText(RegisterActivity.this,"Permission Denied,You may not be able to use some of the features of this App",Toast.LENGTH_LONG).show();
                    }
                }
                else
                {
                    dailog();
                }
                break;
        }
    }

    public void OneSignalinit()
    {
        OneSignal.deleteTag("key1");
        OneSignal.deleteTag("key2");
//        OneSignal.deleteTag("user_token");
//        OneSignal.deleteTag("user_Level_id");
        JSONObject tags = new JSONObject();
        try {
            tags.put("user_Level", sp.getString(Constants.level_name,""));
            tags.put("user_Level_Id", sp.getString(Constants.level_id,""));
            tags.put("user_Token",android_id );
            tags.put("user_email",sp.getString(Constants.email,""));

        } catch (JSONException e) {
            e.printStackTrace();
        }
        OneSignal.sendTags(tags);
        //OneSignal.setEmail(sp.getString(Constants.email,""));
    }

}

//link of volley plus image load
//https://www.androidlearning.com/multipart-request-using-android-volley/