package com.kickoffelite.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.kickoffelite.R;
import com.kickoffelite.activity.SwipeTechActivity;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.model.DataModeltech;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

public class AdapterTech extends RecyclerView.Adapter<AdapterTech.ViewHolder>
{
    List<DataModeltech> mValues;
    Context mContext;
    View itemview;
    SharedPreferences sp;
    SharedPreferences.Editor ed;

    public AdapterTech(Context context, List<DataModeltech> values) {

        this.mValues = values;
        this.mContext = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;
        public ImageView imageView;
        SelectableRoundedImageView img_video;
        public RelativeLayout relativeLayout,mainlayout;
        DataModeltech item;

        public ViewHolder(View v) {
            super(v);
            textView = (TextView) v.findViewById(R.id.textView);
           // imageView = (ImageView) v.findViewById(R.id.imageView);
            img_video=(SelectableRoundedImageView)v.findViewById(R.id.img_video);
            mainlayout=(RelativeLayout)v.findViewById(R.id.mainlayout);
            Constants.overrideFonts(mContext,mainlayout);
            sp=mContext.getSharedPreferences(Constants.appname,MODE_PRIVATE);
            ed=sp.edit();
        }
    }

    @Override
    public AdapterTech.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        itemview = LayoutInflater.from(mContext).inflate(R.layout.recycler_tech, parent, false);
        return new ViewHolder(itemview);
    }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            final DataModeltech movie = mValues.get(position);
                holder.textView.setText(movie.getName());

                String image=movie.getImage();
                String imageurl="";

                if(!image.equalsIgnoreCase(""))
                {
                    if(image.contains("http"))
                    {
                        imageurl=image;
                    }
                    else
                    {
                        imageurl= GlobalConstants.ImageUrl+"/"+image;
                    }
                    Picasso.with(mContext).load(imageurl).into(holder.img_video);
                }
                else
                {
                    Picasso.with(mContext).load(R.color.grayback).into(holder.img_video);
                }

            //Picasso.with(mContext).load(movie.getImage()).into(holder.img_video);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                        Intent intent=new Intent(getApplicationContext(), SwipeTechActivity.class);
                        intent.putExtra(Constants.TechId,movie.getId());
                        Log.e("onClick: ", movie.getId());
                        intent.putExtra(Constants.TechName,movie.getName());
                        intent.putExtra(Constants.TechDesc,movie.getDescription());
                        intent.putExtra(Constants.TechImage,movie.getImage());
                        intent.putExtra(Constants.TechSession,movie.getSession_count());
                        intent.putExtra("name","");
                        mContext.startActivity(intent);

                }
            });
    }

    @Override
    public int getItemCount() {

        return mValues.size();
    }

}