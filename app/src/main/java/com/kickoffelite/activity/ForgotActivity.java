package com.kickoffelite.activity;

import android.app.Activity;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kickoffelite.R;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.GlobalConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotActivity extends Activity implements View.OnClickListener
{
    Button btn_link;
    EditText edt_email;
    String email;
    Dialog progressDialog;
    RelativeLayout mainlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }
        init();
        setListeners();
    }

    public void init()
    {
        mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        btn_link=(Button)findViewById(R.id.btn_link);
        edt_email=(EditText)findViewById(R.id.edt_email);
    }

    public void setListeners()
    {
        btn_link.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btn_link:
             Validation();
                break;
        }
    }

    public void Validation()
    {
        if(edt_email.getText().toString().equalsIgnoreCase(""))
        {
            edt_email.setError("Email required");
        }
        else if(!Constants.isValid(edt_email.getText().toString()))
        {
            edt_email.setError("Email not Valid");
        }
        else
        {
            email=edt_email.getText().toString();
            JSONObject js=new JSONObject();
            try
            {
                js.put(Constants.email,email);
                forgot(js);
            }
            catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    //API
    public void forgot(final JSONObject json1)
    {
        String URL= GlobalConstants.FORGOTPSWD;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    String message=s.getString("message");
                    if (status.equalsIgnoreCase("1"))
                    {
                       // progressDialog.dismiss();
                        Log.e("onResponse: ", s.toString());
                        Toast.makeText(ForgotActivity.this,message, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                       // progressDialog.dismiss();
                        Toast.makeText(ForgotActivity.this,message, Toast.LENGTH_SHORT).show();
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(ForgotActivity.this,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }


                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(ForgotActivity.this);
        rQueue.add(request);
    }

}
