package com.kickoffelite.model;

/**
 * Created by HP on 5/23/2018.
 */

public class DrillSecondModel
{
    public String getMainname() {
        return mainname;
    }

    public void setMainname(String mainname) {
        this.mainname = mainname;
    }

    public DrillSecondModel(String mainid, String id, String name, String time, String mainname,String session_status)
    {
        this.mainid = mainid;
        this.id = id;
        this.name = name;
        this.time = time;
        this.mainname = mainname;
        this.session_status=session_status;
    }

    public String getName() {
        return name;
    }

    public String getMainid() {
        return mainid;
    }

    public void setMainid(String mainid) {
        this.mainid = mainid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSession_status() {
        return session_status;
    }

    public void setSession_status(String session_status) {
        this.session_status = session_status;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    String name,time,id,mainid,mainname,session_status;
}
