package com.kickoffelite.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.kickoffelite.R;
import com.kickoffelite.activity.DrillThirdActivity;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.model.DrillMainModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class DrillMainAdapter extends RecyclerView.Adapter<DrillMainAdapter.MyViewHolder>
{
    private List<DrillMainModel> moviesList;
    View itemView;
    Context context;
    SharedPreferences sp;
    SharedPreferences.Editor ed;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        public TextView name, txt_total,txt_completed;
        SelectableRoundedImageView img_video;
        ImageView img_icon;
        RelativeLayout mainlayout,rel_progress;
        SeekBar seekBar;

        public MyViewHolder(View view) {
            super(view);
            mainlayout=(RelativeLayout)view.findViewById(R.id.mainlayout);
            Constants.overrideFonts(context,mainlayout);
            seekBar=(SeekBar)view.findViewById(R.id.seekbar);
            name = (TextView) view.findViewById(R.id.txt_name);
            txt_total = (TextView) view.findViewById(R.id.txt_total);
            txt_completed = (TextView) view.findViewById(R.id.txt_completed);
            img_video = (SelectableRoundedImageView) view.findViewById(R.id.img_video);
            img_icon = (ImageView) view.findViewById(R.id.img_unlock);
            rel_progress=(RelativeLayout)view.findViewById(R.id.rel_progress);
            sp=context.getSharedPreferences(Constants.appname,MODE_PRIVATE);
            ed=sp.edit();
        }

        @Override
        public void onClick(View v)
        {
        }
    }

    public DrillMainAdapter(Context context,List<DrillMainModel> moviesList) {
        this.moviesList = moviesList;
        this.context=context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
         itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drill_recycler_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("NewApi")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final DrillMainModel movie = moviesList.get(position);
        holder.name.setText(movie.getName());
//        if(movie.getIs_locked().equalsIgnoreCase("0"))
//        {
            holder.img_icon.setVisibility(View.INVISIBLE);
            holder.rel_progress.setVisibility(View.VISIBLE);
            holder.txt_total.setText(movie.getSession_count()+" Sessions");
            holder.txt_completed.setText(movie.getComplete_session()+"/");
            holder.seekBar.setMax(Integer.parseInt(movie.getSession_count()));
            holder.seekBar.setProgress(Integer.parseInt(movie.getComplete_session()));
            holder.seekBar.setFocusable(false);
            holder.seekBar.setEnabled(false);


        holder.img_video.setScaleType(ImageView.ScaleType.CENTER_CROP);
        // holder.img_video.setImageResource(movie.getImg_video());

        String image=movie.getImage();
        String imageurl="";

        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                imageurl=image;
            }
            else
            {
                imageurl= GlobalConstants.ImageUrl+"/"+image;
            }
            Picasso.with(context).load(imageurl).into(holder.img_video);
        }
        else
        {
            Picasso.with(context).load(R.color.grayback).into(holder.img_video);
        }
       // Picasso.with(context).load(movie.getImage()).into(holder.img_video);

        holder.seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                    Intent ii=new Intent(context, DrillThirdActivity.class);
                    ii.putExtra(Constants.video_id,movie.getId()) ;
                    ii.putExtra(Constants.image,movie.getImage()) ;
                    ii.putExtra(Constants.desc,movie.getDescription()) ;
                    ii.putExtra(Constants.total_session,movie.getSession_count()) ;
                    ii.putExtra(Constants.completed_session,movie.getComplete_session()) ;
                    ii.putExtra(Constants.name,movie.getName()) ;
                    ii.putExtra(Constants.is_locked,movie.getIs_locked());
                    ii.putExtra(Constants.level_name,movie.getLevelname());
                    ii.putExtra("name","");
                    context.startActivity(ii);

            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}