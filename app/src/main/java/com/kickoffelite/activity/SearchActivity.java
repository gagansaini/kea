package com.kickoffelite.activity;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kickoffelite.R;
import com.kickoffelite.adapter.SearchAdapter;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.model.DrillThirdModel;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.model.SearchModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchActivity extends Activity
{
    EditText ed_search;
    SearchAdapter searchAdapter;
    private RecyclerView recyclerView;
    TextView txt_resent;
    String keyword;
    ImageView img_icon;
    ArrayList<HashMap<String, String>> drill_list_tags=new ArrayList<>();
    HashMap<String, String> drill_hash_tags = new HashMap<String, String>();
    ArrayList<HashMap<String, String>> technique_list_tags=new ArrayList<>();
    HashMap<String, String> technique_hash_tags = new HashMap<String, String>();
    List<SearchModel> searchlist = new ArrayList<>();
     Handler mHandler;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    ArrayList<SearchModel> recentsearchlist=new ArrayList<>();
    HashMap<String, String> recentsearchhash = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        init();
        setListener();
    }

    public void init()
    {
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();

        ed_search=(EditText)findViewById(R.id.ed_search);
        txt_resent=(TextView)findViewById(R.id.txt_resent);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        img_icon=(ImageView)findViewById(R.id.img_icon);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recentsearchlist=searches();
        if(recentsearchlist!=null)
        {
            Log.e( "size: ", String.valueOf(recentsearchlist.size()));
            searchAdapter = new SearchAdapter(SearchActivity.this, recentsearchlist);
            searchAdapter.notifyDataSetChanged();
            recyclerView.setAdapter(searchAdapter);
        }



    }


    public void setListener()
    {
         mHandler = new Handler();
        ed_search.addTextChangedListener(textWatcher);
        img_icon.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                finish();
            }
        });
        ed_search.setOnEditorActionListener(
                new EditText.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                                actionId == EditorInfo.IME_ACTION_DONE ||
                                        event.getAction() == KeyEvent.ACTION_DOWN ||
                                        event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
//                            if (event == null || !event.isShiftPressed()) {


                                return true; // consume.
                           // }
                        }

                        return false; // pass on to other listeners.
                    }
                }
        );

        ed_search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        ed_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    keyword=ed_search.getText().toString();
                    JSONObject js=new JSONObject();
                    try {
                        js.put("keyword",keyword);
                        js.put("user_id",sp.getString(Constants.id,""));
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                    if(recentsearchlist !=null & recentsearchlist.size()<4)
                    {
                        recentsearchlist.add(new SearchModel("", keyword, "", "", "", "","list","","", drill_list_tags, technique_list_tags));
                     //   ed.putString("recentsearch",setDrill_id_tags(recentsearchlist));
                        Gson gson = new Gson();
                        String json = gson.toJson(recentsearchlist);
                        ed.putString("search", json);
                        ed.commit();
                        Log.e( "size: ", String.valueOf(recentsearchlist.size()));
                    }
                    else if(recentsearchlist !=null & recentsearchlist.size()>4)
                    {
                        Log.e( "size: ", String.valueOf(recentsearchlist.size()));
                        recentsearchlist.remove(0);
                        recentsearchlist.add(new SearchModel("", keyword, "", "", "", "","list","","", drill_list_tags, technique_list_tags));
                        Gson gson = new Gson();
                        String json = gson.toJson(recentsearchlist);
                        ed.putString("search", json);
                        ed.commit();
                    }
                    else
                    {
                        recentsearchlist.add(new SearchModel("", keyword, "", "", "", "","list","","", drill_list_tags, technique_list_tags));
                        Gson gson = new Gson();
                        String json = gson.toJson(recentsearchlist);
                        ed.putString("search", json);
                        ed.commit();
                    }

                    search(js);
                    return true;
                }
                return false;
            }
        });
    }

    private TextWatcher textWatcher = new TextWatcher() {

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() == 0)
            {
                searchlist.clear();
                txt_resent.setText("Recent Searches");
                searchAdapter = new SearchAdapter(SearchActivity.this, searchlist);
                searchAdapter.notifyDataSetChanged();
                recyclerView.setAdapter(searchAdapter);
            }
            else
            {
                Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            mHandler.removeCallbacksAndMessages(null);
            if(!ed_search.getText().toString().equalsIgnoreCase(""))
            {
                mHandler.postDelayed(userStoppedTyping, 2000);
            }

        }
        Runnable userStoppedTyping = new Runnable() {

            @Override
            public void run() {
                keyword=ed_search.getText().toString();
                JSONObject js=new JSONObject();
                try {
                    js.put("keyword",keyword);
                    js.put("user_id",sp.getString(Constants.id,""));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                search(js);
                // user didn't typed for 2 seconds, do whatever you want
            }
        };
    };

    public void search(final JSONObject json1)
    {
        final List<DrillThirdModel> drill_sub_List = new ArrayList<>();
        final ArrayList<HashMap<String, String>> drill_list_tags=new ArrayList<>();
        final ArrayList<HashMap<String, String>> technique_list_tags=new ArrayList<>();
        searchlist = new ArrayList<>();
        String URL= GlobalConstants.Search;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");

                    if (status.equalsIgnoreCase("1"))
                    {
                            JSONArray jsarr = s.getJSONArray("data");
                            for (int i = 0; i < jsarr.length(); i++)
                            {
                                    JSONObject json = jsarr.getJSONObject(i);
                                    String search_type=json.getString("search_type");
                                     if (search_type.equalsIgnoreCase("videos"))
                                     {
                                         Log.e("send 5", String.valueOf(s));
                                         String id = json.getString("id");
                                         String level_id = json.getString("level_id");
                                         String video_id = json.getString("video_id");
                                         String level_no = json.getString("level_no");
                                         String video_name = json.getString("video_name");
                                         String video_description = json.getString("video_description");
                                         String video_url = json.getString("video_url");
                                         String video_thumbnail = json.getString("video_thumbnail");
                                         String video_time = json.getString("video_time");
                                         String video_status = json.getString("video_status");
                                         String level_name = json.getString("level_name");
                                         JSONObject jsontag = json.getJSONObject("tags");
                                         if (jsontag.has("techniques")) {
                                             JSONArray jstech = jsontag.getJSONArray("techniques");
                                             for (int j = 0; j < jstech.length(); j++) {
                                                 JSONObject jsontech = jstech.getJSONObject(j);
                                                 String technique_id_tag = jsontech.getString("id");
                                                 String technique_name_tag = jsontech.getString("name");
                                                 String technique_description = jsontech.getString("description");
                                                 String technique_image = jsontech.getString("image");
                                                 String session_count = jsontech.getString("session_count");
                                                 String is_locked = jsontech.getString("is_locked");
                                                 String complete_session = jsontech.getString("complete_session");
                                                 technique_hash_tags.put("tech_id", technique_id_tag);
                                                 technique_hash_tags.put("tech_name", technique_name_tag);
                                                 technique_hash_tags.put("tech_desc", technique_description);
                                                 technique_hash_tags.put("tech_image", technique_image);
                                                 technique_hash_tags.put("tech_session", session_count);
                                                 technique_hash_tags.put("tech_locked", is_locked);
                                                 technique_hash_tags.put("tech_complete", complete_session);
                                                 technique_list_tags.add(technique_hash_tags);
                                                 technique_hash_tags = new HashMap<String, String>();
                                             }
                                         } else {
                                         }
                                         if (jsontag.has("drills")) {
                                             JSONArray jstech = jsontag.getJSONArray("drills");
                                             for (int j = 0; j < jstech.length(); j++) {
                                                 JSONObject jsontech = jstech.getJSONObject(j);
                                                 String drill_id_tag = jsontech.getString("id");
                                                 String drill_name_tag = jsontech.getString("name");
                                                 String drill_description = jsontech.getString("description");
                                                 String drill_image = jsontech.getString("image");
                                                 String session_count = jsontech.getString("session_count");
                                                 String is_locked = jsontech.getString("is_locked");
                                                 String complete_session = jsontech.getString("complete_session");
                                                 drill_hash_tags.put("drill_id", drill_id_tag);
                                                 drill_hash_tags.put("drill_name", drill_name_tag);
                                                 drill_hash_tags.put("drill_desc", drill_description);
                                                 drill_hash_tags.put("drill_image", drill_image);
                                                 drill_hash_tags.put("drill_session", session_count);
                                                 technique_hash_tags.put("drill_locked", is_locked);
                                                 technique_hash_tags.put("drill_complete", complete_session);
                                                 drill_list_tags.add(drill_hash_tags);
                                                 drill_hash_tags = new HashMap<String, String>();
                                             }
                                         }
                                         searchlist.add(new SearchModel(video_id, video_name, level_id, video_description, video_thumbnail, video_time,search_type,video_url, video_status,drill_list_tags, technique_list_tags));
                                     }
                                     else
                                    {
                                        String techid = json.getString("id");
                                        String techname = json.getString("name");
                                        String technique_description = json.getString("description");
                                        String technique_image = json.getString("image");
                                        String session_count = json.getString("session_count");
                                       // String search_typee = json.getString("search_type");
                                        searchlist.add(new SearchModel(techid, techname, "", technique_description, technique_image, session_count,search_type,"","", drill_list_tags, technique_list_tags));
                                    }

                            }

                        Log.e( "serachlist: ",searchlist.toString() );
                        searchAdapter = new SearchAdapter(SearchActivity.this, searchlist);
                        searchAdapter.notifyDataSetChanged();
                        recyclerView.setAdapter(searchAdapter);

                     }
                     else
                     {
                         String message=s.getString("message");
                         txt_resent.setText(message);
                         Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                     }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(SearchActivity.this,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }

                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(SearchActivity.this);
        rQueue.add(request);
    }


    public ArrayList<SearchModel> searches() {
        ArrayList< SearchModel> callLog = new ArrayList< SearchModel>();

        Gson gson = new Gson();
        String json = sp.getString("search", "");
        if (json.isEmpty()) {
            callLog = new ArrayList<SearchModel>();
        } else {
            Type type = new TypeToken<ArrayList<SearchModel>>() {
            }.getType();
            callLog = gson.fromJson(json, type);
        }
        return callLog;
    }

}
