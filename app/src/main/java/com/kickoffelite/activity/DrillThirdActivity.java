package com.kickoffelite.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kickoffelite.R;
import com.kickoffelite.adapter.DrillThirdAdapter;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.model.DrillThirdModel;
import com.kickoffelite.helper.GlobalConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DrillThirdActivity extends Activity implements View.OnClickListener{

    private RecyclerView recyclerView;
    private DrillThirdAdapter mAdapter;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    Dialog progressDialog;
    ImageView img_icon,img_video;
    RelativeLayout mainlayout;
    TextView pagetitle,txt_title,txt_session,txt_desc,txt_skill;
    String pgettle,id,desc,is_locked,totalsession,levelname,imageurl;
    ArrayList<HashMap<String, String>> drill_list_tags=new ArrayList<>();
    HashMap<String, String> drill_hash_tags = new HashMap<String, String>();
    ArrayList<HashMap<String, String>> technique_list_tags=new ArrayList<>();
    HashMap<String, String> technique_hash_tags = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drill_third);
        init();
        setListener();
    }

    public void init()
    {
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        Intent intent = getIntent();
        mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);

        desc=intent.getStringExtra(Constants.desc);
        totalsession=intent.getStringExtra(Constants.total_session);
        levelname=intent.getStringExtra(Constants.level_name);
        String image=intent.getStringExtra(Constants.image);
        id=intent.getStringExtra(Constants.video_id);
        is_locked=intent.getStringExtra(Constants.is_locked);
        pgettle=intent.getStringExtra(Constants.name);

        pagetitle=(TextView)findViewById(R.id.pagetitle);
        txt_session=(TextView)findViewById(R.id.txt_session);
        txt_desc=(TextView)findViewById(R.id.txt_desc);
        txt_title=(TextView)findViewById(R.id.txt_title);
        txt_skill=(TextView)findViewById(R.id.txt_skill);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        img_icon=(ImageView)findViewById(R.id.img_icon);
        img_video=(ImageView)findViewById(R.id.img_video);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        // txttitle=intent.getStringExtra(Constants.subname);
        // txt_time.setText(10+" minute workout");
        //  pagetitle.setText("Drills");
        Log.e( "session: ", totalsession);
        txt_skill.setText(levelname);
        txt_desc.setText(desc);
        txt_session.setText(totalsession+" Sessions");
        txt_title.setText(pgettle);
        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                imageurl=image;
            }
            else
            {
                imageurl= GlobalConstants.ImageUrl+"/"+image;
            }
            Picasso.with(DrillThirdActivity.this).load(imageurl).placeholder(R.color.grayback).into(img_video);
        }
        else
        {
            Picasso.with(DrillThirdActivity.this).load(R.color.grayback).placeholder(R.color.grayback).into(img_video);
        }

        //mainid=intent.getStringExtra(Constants.id);
//        mAdapter = new DrillThirdAdapter(DrillThirdActivity.this,drill_sub_List);
//        recyclerView.setAdapter(mAdapter);

        if(intent.getStringExtra("name").equalsIgnoreCase("drill"))
        {
            id=intent.getStringExtra("id");
            String drilltitle=intent.getStringExtra("title");
            String drilldesc=intent.getStringExtra("desc");
            String drillsession=intent.getStringExtra(Constants.total_session);
            String drillimage=intent.getStringExtra("image");
            txt_desc.setText(drilldesc);
            txt_session.setText(drillsession+" Sessions");
            txt_title.setText(drilltitle);
            txt_skill.setText(sp.getString(Constants.level_name,""));
            imageurl= GlobalConstants.ImageUrl+"/"+image;
            Picasso.with(getApplicationContext()).load(imageurl).into(img_video);
            //txt_head.setText(drilltitle);
            if(Constants.isNetworkAvailable(getApplicationContext()))
            {
                progressBarDialog();
                get_Sub_Drills();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
            }

        }
        else
        {
            if(Constants.isNetworkAvailable(getApplicationContext()))
            {
                progressBarDialog();
                get_Sub_Drills();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
            }
        }

    }
    public void setListener()
    {
        img_icon.setOnClickListener(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onResume: ","resume" );
        //get_Sub_Drills();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e("onStart: ","onStart" );

        if(Constants.isNetworkAvailable(getApplicationContext()))
        {
            get_Sub_Drills();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_icon:
                finish();
                break;
        }
    }

    public void get_Sub_Drills()
    {
       final List<DrillThirdModel> drill_sub_List = new ArrayList<>();
        final ArrayList<HashMap<String, String>> drill_list_tags=new ArrayList<>();
        final ArrayList<HashMap<String, String>> technique_list_tags=new ArrayList<>();
        Log.e( "get_Sub_Drills: ",id );
        final String URL = GlobalConstants.SESSIONVIDEOLIST+sp.getString(Constants.id,"")+"&drill_id="+id;
        StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {

                Log.e("response order", s);
                try {
                    JSONObject jsob = new JSONObject(s);
                    String response = jsob.getString("status");
                    if (response.equals("1"))
                    {
                        progressDialog.dismiss();
                        Log.e("status", response);
                        //  totalPages = Integer.parseInt(jsob.getString("total_pages"));
                        JSONArray jsonarr = jsob.getJSONArray("data");
                        if(drill_sub_List.size()==0) {
                            String main_drill_name="";
                            for(int i=0;i<jsonarr.length();i++)
                            {
                                JSONObject jsonobj=jsonarr.getJSONObject(i);
                                String id = jsonobj.getString("id");
                                String level_id = jsonobj.getString("level_id");
                                String drill_id = jsonobj.getString("drill_id");
                                String drill_name = jsonobj.getString("drill_name");
                                String video_id = jsonobj.getString("video_id");
                                String level_no = jsonobj.getString("level_no");
                                String video_name = jsonobj.getString("video_name");
                                String video_url = jsonobj.getString("video_url");
                                String video_thumbnail = jsonobj.getString("video_thumbnail");
                                String video_description = jsonobj.getString("video_description");
                                String video_time = jsonobj.getString("video_time");
                                String level_name = jsonobj.getString("level_name");
                                String is_locked = jsonobj.getString("is_locked");
                                String video_status = jsonobj.getString("video_status");
                                String is_favourite = jsonobj.getString("is_favourite");
                                JSONObject jsontag=jsonobj.getJSONObject("tags");
                                if(jsontag.has("techniques"))
                                {
                                    JSONArray jstech=jsontag.getJSONArray("techniques");
                                    for(int j=0;j<jstech.length();j++)
                                    {
                                        JSONObject jsontech=jstech.getJSONObject(j);
                                        String technique_id_tag = jsontech.getString("id");
                                        String technique_name_tag = jsontech.getString("name");
                                        String technique_description = jsontech.getString("description");
                                        String technique_image = jsontech.getString("image");
                                        String session_count = jsontech.getString("session_count");
                                        String is_lockedd = jsontech.getString("is_locked");
                                        String complete_session = jsontech.getString("complete_session");
                                        technique_hash_tags.put("tech_id", technique_id_tag);
                                        technique_hash_tags.put("tech_name", technique_name_tag);
                                        technique_hash_tags.put("tech_desc", technique_description);
                                        technique_hash_tags.put("tech_image", technique_image);
                                        technique_hash_tags.put("tech_session", session_count);
                                        technique_hash_tags.put("tech_locked", is_lockedd);
                                        technique_hash_tags.put("tech_complete", complete_session);
                                        technique_list_tags.add(technique_hash_tags);
                                        technique_hash_tags = new HashMap<String, String>();
                                    }
                                }
                                else
                                {
                                }
                                if(jsontag.has("drills"))
                                {
                                    JSONArray jstech=jsontag.getJSONArray("drills");
                                    for(int j=0;j<jstech.length();j++)
                                    {
                                        JSONObject jsontech=jstech.getJSONObject(j);
                                        String drill_id_tag = jsontech.getString("id");
                                        String drill_name_tag = jsontech.getString("name");
                                        String drill_description = jsontech.getString("description");
                                        String drill_image = jsontech.getString("image");
                                        String session_count = jsontech.getString("session_count");
                                        String is_lockedd = jsontech.getString("is_locked");
                                        String complete_session = jsontech.getString("complete_session");
                                        drill_hash_tags.put("drill_id", drill_id_tag);
                                        drill_hash_tags.put("drill_name", drill_name_tag);
                                        drill_hash_tags.put("drill_desc", drill_description);
                                        drill_hash_tags.put("drill_image", drill_image);
                                        drill_hash_tags.put("drill_session", session_count);
                                        technique_hash_tags.put("drill_locked", is_lockedd);
                                        technique_hash_tags.put("drill_complete", complete_session);
                                        drill_list_tags.add(drill_hash_tags);
                                        drill_hash_tags = new HashMap<String, String>();
                                    }
                                }
                                else
                                {
                                }
                                //  String url="http://worksdelight.com/kickoff/Uploads/Users/"+sp.getString(Constants.id,"")+"/"+category_image;
                                drill_sub_List.add(new DrillThirdModel(id,level_id,drill_id,drill_name,video_id,level_no,video_name,video_url,
                                        video_thumbnail,video_time,level_name,is_locked,video_status,video_description,pgettle,is_favourite
                                        ,drill_list_tags,technique_list_tags));
                            }
                        }
                        Log.e("listsize: ", String.valueOf(drill_sub_List.size()));
                        mAdapter = new DrillThirdAdapter(DrillThirdActivity.this, drill_sub_List);
                        mAdapter.notifyDataSetChanged();
                        recyclerView.setAdapter(mAdapter);

                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
                Toast.makeText(DrillThirdActivity.this, "Some error occurred -> " + volleyError, Toast.LENGTH_LONG).show();

            }
        }) ;
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(DrillThirdActivity.this);
        rQueue.add(request);
    }

    public void progressBarDialog()
    {
        progressDialog= new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }
}
