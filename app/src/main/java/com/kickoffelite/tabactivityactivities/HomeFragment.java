package com.kickoffelite.tabactivityactivities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kickoffelite.R;
import com.kickoffelite.activity.ProfileActivity;
import com.kickoffelite.activity.SearchActivity;
import com.kickoffelite.adapter.HomeLevelAdapter;
import com.kickoffelite.adapter.HomeTechniqueAdapter;
import com.kickoffelite.adapter.HorizontalAdapter;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.model.HomeDrillmodel;
import com.kickoffelite.model.HomeLevelModel;
import com.kickoffelite.model.HomeTechModel;
import com.kickoffelite.volley.VolleyMultipartRequest;
import com.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeFragment extends AppCompatActivity implements ViewPager.PageTransformer
{


    private static ViewPager mPager,mPager1;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    RelativeLayout mainlayout;

    //recyler
    RecyclerView horizontal_recycler_view;
    HorizontalAdapter horizontalAdapter;
    //pager
    public static final float MAX_SCALE = 1.0f;
    public static final float MIN_SCALE = 0.8f;
    ScrollView scroll;
    AlertDialog builder;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    TextView txt_skill;
    Dialog progressDialog;
    ImageView img_search;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.appcolor));
        }
        init();
        setListener();

        if(Constants.isNetworkAvailable(getApplicationContext()))
        {
            progressBarDialog();
            gethomedata();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        if(Constants.isNetworkAvailable(getApplicationContext()))
        {
            gethomedata();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(sp.getString(Constants.level_name,"").equalsIgnoreCase(""))
        {
            txt_skill.setText("Beginner");
        }
        else
        {
            txt_skill.setText(sp.getString(Constants.level_name,""));
        }
        scroll.smoothScrollBy(0,0);
    }

    private void init()
    {
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        img_search=(ImageView)findViewById(R.id.img_search);
        scroll=(ScrollView)findViewById(R.id.scroll);
        txt_skill=(TextView)findViewById(R.id.txt_skill);
        horizontal_recycler_view= (RecyclerView) findViewById(R.id.rvHorizontal);
        initpager();
        setListeners();
        scroll.smoothScrollBy(0,0);
    }


    public void initpager()
    {
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager1 = (ViewPager) findViewById(R.id.pager2);
    }

    public void setListener()
    {
        txt_skill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(getApplicationContext(), ProfileActivity.class);
                startActivity(intent);
            }
        });

        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(), SearchActivity.class);
                startActivity(intent);
            }
        });
    }

    public void setListeners()
    {
        mPager.setPageTransformer(true,this);
        mPager1.setPageTransformer(true,this);
    }

    //recyclerview
    public void initrecycler()
    {

    }

    //PagerOne
    public void pagerOne()
    {
        CirclePageIndicator indicator =(CirclePageIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);
        final float density = getResources().getDisplayMetrics().density;
        //Set circle indicator radius
        indicator.setRadius(4 * density);
       // NUM_PAGES =levellist.size();

        // Auto start of viewpager

//        final Handler handler = new Handler();
//        final Runnable Update = new Runnable() {
//            public void run() {
//                if (currentPage == NUM_PAGES) {
//                    currentPage = 0;
//                }
//                mPager.setCurrentItem(currentPage++, true);
//            }
//        };
//        Timer swipeTimer = new Timer();
//        swipeTimer.schedule(new TimerTask() {
//            @Override
//            public void run() {
//                handler.post(Update);
//            }
//        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPage = position;
                    if(currentPage==2)
                    {
                       // pagerTwo2();
                    }
                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {

                }

                    @Override
                    public void onPageScrollStateChanged(int pos) {

                }
                });
    }

    //PagerTwo
    public void pagerTwo()
    {

    }


    //for showing next image from right in viewPager
    @Override
    public void transformPage(View page, float position)
    {
        switch (page.getId())
        {
            case R.id.pager:

                break;

            case R.id.pager2:
                break;
        }
    }



    @Override
    public void onBackPressed() {
        alertDialogExit();
    }

    public void gethomedata() {
        final ArrayList<HomeLevelModel> levellist = new ArrayList<>();
        final ArrayList<HomeTechModel> techlist = new ArrayList<HomeTechModel>();
        final ArrayList<HomeDrillmodel> drilllist = new ArrayList<HomeDrillmodel>();
        String URL = GlobalConstants.GETHOMEDATA;
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL,
                 new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        String resultResponse = new String(response.data);
                        try {
                            Log.e("onResponse: ",resultResponse );
                            JSONObject result = new JSONObject(resultResponse);
                            String status = result.getString("status");
                            if(status.equalsIgnoreCase("1"))
                            {
                                progressDialog.dismiss();
                                JSONObject jsonobj=result.getJSONObject("data");

                                JSONObject jsonObj1=jsonobj.getJSONObject("level");
                                    String levelname=jsonObj1.getString("level_name");
                                    ed.putString(Constants.level_name,levelname);
                                    ed.commit();
                                    JSONArray jsonArray1=jsonObj1.getJSONArray("level_details");
                                    for(int j=0;j<jsonArray1.length();j++)
                                    {
                                        JSONObject jsonObject1=jsonArray1.getJSONObject(j);
                                        String id=jsonObject1.getString("id");
                                        String sublevel_id=jsonObject1.getString("sublevel_id");
                                        String name=jsonObject1.getString("name");
                                        String image=jsonObject1.getString("image");
                                        String description=jsonObject1.getString("description");
                                        String is_locked=jsonObject1.getString("is_locked");
                                        String session_count=jsonObject1.getString("session_count");
                                        String complete_session=jsonObject1.getString("complete_session");
                                        levellist.add(new HomeLevelModel(id,sublevel_id,name,image,description,is_locked,session_count,complete_session,levelname));
                                    }
                                    Log.e( "levelist: ",levellist.toString() );

                                   //Level
                                    mPager.setAdapter(new HomeLevelAdapter(HomeFragment.this,levellist));
                                    mPager.setClipToPadding(false);
                                    mPager.setPadding(20,0,50,0);
                                    pagerOne();

                                JSONArray jsonArraytech=jsonobj.getJSONArray("technique");
                                for(int i=0;i<jsonArraytech.length();i++)
                                {
                                    JSONObject jsonObject1=jsonArraytech.getJSONObject(i);
                                    String id=jsonObject1.getString("id");
                                    String name=jsonObject1.getString("name");
                                    String image=jsonObject1.getString("image");
                                    String description=jsonObject1.getString("description");
                                    String level_id=jsonObject1.getString("level_id");
                                    String sublevel_id=jsonObject1.getString("sublevel_id");
                                    String session_count=jsonObject1.getString("session_count");
                                    String is_locked=jsonObject1.getString("is_locked");
                                    String complete_session=jsonObject1.getString("complete_session");
                                    techlist.add(new HomeTechModel(id,name,image,description,level_id,sublevel_id,session_count,is_locked,complete_session));
                                }
                                Log.e( "techlist: ",techlist.toString() );
                                //Technique
                                mPager1.setAdapter(new HomeTechniqueAdapter(HomeFragment.this,techlist));
                                mPager1.setClipToPadding(false);
                                mPager1.setPadding(20,0,50,0);

                                JSONArray jsonArraydrill=jsonobj.getJSONArray("drill");
                                for(int i=0;i<jsonArraydrill.length();i++)
                                {
                                    JSONObject jsonObject1=jsonArraydrill.getJSONObject(i);
                                    String id=jsonObject1.getString("id");
                                    String name=jsonObject1.getString("name");
                                    String image=jsonObject1.getString("image");
                                    String description=jsonObject1.getString("description");
                                    String level_id=jsonObject1.getString("level_id");
                                    String sublevel_id=jsonObject1.getString("sublevel_id");
                                    String session_count=jsonObject1.getString("session_count");
                                    String is_locked=jsonObject1.getString("is_locked");
                                    String complete_session=jsonObject1.getString("complete_session");
                                    drilllist.add(new HomeDrillmodel(id,name,image,description,level_id,sublevel_id,session_count,is_locked,complete_session,levelname));
                                }
                                Log.e( "drilllist: ",drilllist.toString() );
                                //Drill
                                horizontalAdapter=new HorizontalAdapter(drilllist, getApplication());
                                GridLayoutManager horizontalLayoutManager = new GridLayoutManager(HomeFragment.this,1, GridLayoutManager.HORIZONTAL, false);
                                horizontal_recycler_view.setLayoutManager(horizontalLayoutManager);
                                horizontal_recycler_view.setAdapter(horizontalAdapter);
                                //

                            }else{
                                progressDialog.dismiss();

                            }
                        }
                        catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("onErrorResponse: ", error.toString());
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("user_id",sp.getString(Constants.id,""));
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    public void progressBarDialog()
    {
        progressDialog= new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    @SuppressLint("ResourceAsColor")
    public void alertDialogExit()
    {
        final Dialog openDialog = new Dialog(HomeFragment.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.background));
        //openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.mark_dilaog);
        RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        TextView txt_cancel=(TextView)openDialog.findViewById(R.id.txt_cancel);
        TextView txt_remove=(TextView)openDialog.findViewById(R.id.txt_remove);
        TextView txt_alertmsg=(TextView)openDialog.findViewById(R.id.txt_alertmsg);
        txt_alertmsg.setText("Do you really want to exit ?");
        txt_remove.setText("Exit");
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDialog.cancel();
            }
        });
        txt_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                openDialog.cancel();
            }
        });
        openDialog.show();
    }


}
