package com.kickoffelite.activity;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kickoffelite.R;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.GlobalConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener
{
    EditText edt_old,edt_new,edt_renew;
    Button btn_submit;
    String oldpswd,newpswd,renewpswd;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    Dialog progressDialog;
    RelativeLayout mainlayout;
    ImageView img_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        init();
        setListener();
    }

    public void init()
    {
        mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        edt_old=(EditText)findViewById(R.id.edt_old);
        edt_new=(EditText)findViewById(R.id.edt_new);
        edt_renew=(EditText)findViewById(R.id.edt_renew);
        btn_submit=(Button)findViewById(R.id.btn_submit);
        img_icon=(ImageView)findViewById(R.id.icon);
    }

    public void setListener()
    {
        btn_submit.setOnClickListener(this);
        img_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
             finish();
            }
        });
    }

    @Override
    public void onClick(View v)
    {
        oldpswd=edt_old.getText().toString();
        newpswd=edt_new.getText().toString();
        renewpswd=edt_renew.getText().toString();
        validation(oldpswd,newpswd,renewpswd);
    }

    public void validation(String oldpswd,String newpswd,String renewpswd)
    {
       if(oldpswd.equals(""))
       {
           edt_old.setError("old password required");
       }
       else if(oldpswd.length()<6)
       {
           edt_old.setError("wrong password");
       }
       else if(newpswd.equals(""))
       {
           edt_new.setError("new password required");
       }
       else if(newpswd.length()<6)
       {
           edt_new.setError("password must be of minimum 6 letters");
       }
       else if(renewpswd.equals(""))
       {
           edt_renew.setError("new password required");
       }
       else if(!renewpswd.equalsIgnoreCase(newpswd))
       {
           edt_renew.setError("new password and re enter password should be the same");
           edt_new.setError("");
       }
       else
       {
           //API
           JSONObject js=new JSONObject();
           try
           {
               js.put(Constants.id,sp.getString(Constants.id,""));
               js.put(Constants.old_password,oldpswd);
               js.put(Constants.new_password,newpswd);
               js.put(Constants.confirm_password,renewpswd);
               progressBarDialog();
               chngepswd(js);
           }
           catch (JSONException e) {
               e.printStackTrace();
           }
       }
    }

    ///CHangePAsswordAPI
    public void chngepswd(final JSONObject json1)
    {
        String URL= GlobalConstants.CHANGEPSWD;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    String message=s.getString("message");
                    if (status.equalsIgnoreCase("1"))
                    {
                         progressDialog.dismiss();
                        Log.e("onResponse: ", s.toString());
                        Toast.makeText(ResetPasswordActivity.this,message, Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else
                    {
                       progressDialog.dismiss();
                        Toast.makeText(ResetPasswordActivity.this,message, Toast.LENGTH_SHORT).show();
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                      progressDialog.dismiss();
                        Toast.makeText(ResetPasswordActivity.this,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }


                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(ResetPasswordActivity.this);
        rQueue.add(request);
    }
    public void progressBarDialog()
    {
        progressDialog= new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }
}
