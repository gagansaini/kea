package com.kickoffelite.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.StrictMode;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.kickoffelite.R;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.model.DailogLevelModel;
import com.kickoffelite.helper.GlobalConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends Activity implements View.OnClickListener
{
    ArrayList<DailogLevelModel> dialoglevel = new ArrayList<>();
    HashMap<String,String> levels=new HashMap<>();
    ArrayList<HashMap<String,String>> levellist = new ArrayList<>();
    TextView txt_forgot,txt_signin;
    EditText edt_email,edt_password;
    Button btn_signin,btn_fb2;
    String email,pswrd;
    Dialog progressDialog;
    HashMap<String,String> map=new HashMap<>();
    SharedPreferences sp;
    SharedPreferences.Editor ed;

    //fb
    CallbackManager callbackManager;
    private static final String EMAIL = "email";
    private static final String ProPic = "propic";
    LoginButton loginButton;
    AccessToken accessToken;
    static String encoded = null;
    static URL imageURL = null;
    String id;
    RelativeLayout mainlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Log.e("onError: ", "error");
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        init();
        setListener();
    }

    public void init()
    {
        loginButton = (LoginButton) findViewById(R.id.btn_fb);
        txt_signin=(TextView)findViewById(R.id.txt_signin);
        txt_forgot=(TextView)findViewById(R.id.txt_forgot);
        edt_email=(EditText)findViewById(R.id.edt_email);
        edt_password=(EditText)findViewById(R.id.edt_password);
        btn_signin=(Button)findViewById(R.id.btn_signin);
        btn_fb2=(Button)findViewById(R.id.btn_fb2);
        edt_email.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
    }

    public void setListener()
    {
        txt_forgot.setOnClickListener(this);
        btn_signin.setOnClickListener(this);
        btn_fb2.setOnClickListener(this);
        txt_signin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.txt_forgot:
                Intent i = new Intent(LoginActivity.this, ForgotActivity.class);
                startActivity(i);
                break;
            case R.id.btn_signin:
                email=edt_email.getText().toString();
                pswrd=edt_password.getText().toString();
                validation();
                break;
            case R.id.txt_signin:
                Intent inte=new Intent(getApplicationContext(),RegisterActivity.class);
                startActivity(inte);
                finish();
                break;
            case R.id.btn_fb2:
                if(Constants.isNetworkAvailable(LoginActivity.this))
                {
                    facebook();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    public void validation()
    {
        if(edt_email.getText().toString().equals(""))
        {
            edt_email.setError("email required");
        }
        else if (!Constants.isValid(email))
        {
            edt_email.setError("email not valid");
        }
        else if(pswrd.length() < 6)
        {
            edt_password.setError("password must be atleast of 6 characters!");
        }
        else if(pswrd.equals(""))
        {
            edt_password.setError("password required");
        }
        else
        {
            //Api
//            map.put(Constants.email,email);
//            map.put(Constants.password,pswrd);
            JSONObject js=new JSONObject();
            try {
                js.put(Constants.email,email);
                js.put(Constants.password,pswrd);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(Constants.isNetworkAvailable(LoginActivity.this))
            {
                progressBarDialog();
                login(js);
            }
            else
            {
                Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
            }


        }
    }

    ///FB
    public void facebook() {
        FacebookSdk.addLoggingBehavior(LoggingBehavior.REQUESTS);
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile","email","user_birthday"));
        loginButton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday"));
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e("onSuccess: ", loginResult.toString());
                accessToken = loginResult.getAccessToken();
                Profile profile = Profile.getCurrentProfile();
                progressBarDialog();
                fbgetProfile();
//                Intent ii = new Intent(getApplicationContext(), HomeActivity.class);
//                startActivity(ii);
            }

            @Override
            public void onCancel() {
                LoginManager.getInstance().logOut();
            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    //simple login
    public void login(final JSONObject json1)
    {
        String URL= GlobalConstants.LOGIN;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    String message=s.getString("message");
                    if (status.equalsIgnoreCase("1"))
                    {
                        progressDialog.dismiss();
                        JSONObject jsonobj=s.getJSONObject("data");
                        String id=jsonobj.getString("id");
                        String level_id=jsonobj.getString("level_id");
                        String level_name=jsonobj.getString("level_name");
                        String name=jsonobj.getString("name");
                        String image=jsonobj.getString("image");
                        String date_of_birth=jsonobj.getString("date_of_birth");
                        String email=jsonobj.getString("email");
                        String gender=jsonobj.getString("gender");
                        String address=jsonobj.getString("address");
                        String push_notification=jsonobj.getString("push_notification");
                        String user_level_for_api=jsonobj.getString("user_level_for_api");
                        JSONArray jsarr=jsonobj.getJSONArray("available_level");
                        for(int i=0;i<jsarr.length();i++)
                        {
                            JSONObject jsobj=jsarr.getJSONObject(i);
                            String idd=jsobj.getString("id");
                            String levelname=jsobj.getString("level_name");
                            String description=jsobj.getString("description");
                            String is_locked=jsobj.getString("is_locked");
                            String total_videos=jsobj.getString("total_videos");
                            String seen_videos=jsobj.getString("seen_videos");
                            String percentage_seen=jsobj.getString("percentage_seen");
                            levels.put("id",idd);
                            levels.put("level_name",levelname);
                            levels.put("description",description);
                            levels.put("is_locked",is_locked);
                            levels.put("total_videos",total_videos);
                            levels.put("seen_videos",seen_videos);
                            levels.put("percentage_seen",percentage_seen);
                            levellist.add(levels);
                            dialoglevel.add(new DailogLevelModel(idd, levelname,description,is_locked,total_videos,seen_videos,percentage_seen));
                            levels=new HashMap<>();
                        }
                        Gson gson = new Gson();
                        String json = gson.toJson(dialoglevel);
                        ed.putString(Constants.pushnotification,push_notification);
                        ed.putString("levels", json);
                        ed.putString(Constants.gender,gender);
                        ed.putString(Constants.address,address);
                        ed.putString(Constants.date_of_birth,date_of_birth);
                        ed.putString(Constants.email,email);
                        ed.putString(Constants.name, name);
                        ed.putString(Constants.level_id,level_id);
                        ed.putString(Constants.level_name,level_name);
                        ed.putString(Constants.image,image);
                        ed.putString(Constants.id,id);
                        ed.putString(Constants.loginflag,"true");
                        ed.putString(Constants.userapilevel,user_level_for_api);
                        ed.commit();
                        Log.e("onResponse: ", s.toString());
                        Toast.makeText(LoginActivity.this,message, Toast.LENGTH_SHORT).show();
                        Intent ii=new Intent(getApplicationContext(),HomeActivity.class);
                        startActivity(ii);
                        finish();
                    }
                    else
                    {
                      progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this,message, Toast.LENGTH_SHORT).show();
                    }
                }
                catch (JSONException e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }


                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(LoginActivity.this);
        rQueue.add(request);
    }

    public void fbgetProfile()
    {
        GraphRequest request = GraphRequest.newMeRequest(accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        Log.e("getprofile: ", response.toString());
                        JSONObject js = new JSONObject();
                        try {
                            if (object.has("name")) {
                                js.put(Constants.name, object.getString("name"));
                            }
                            if (object.has("email")) {
                                js.put(Constants.email, object.getString("email"));
                            }
                            if (object.has("birthday")) {
                                js.put(Constants.date_of_birth, object.getString("birthday"));
                                Log.e("date_of_birth: ","" );
                            }
                            else
                            {
                                js.put(Constants.date_of_birth,"");
                                Log.e("date_of_birth22 ","" );
                            }
                            if (object.has("id")) {
                                js.put(Constants.token, object.getString("id"));
                                id=object.getString("id");
                                Log.e("id: ",object.getString("id") );
                            }
//                            if(object.has("picture"))
//                            {
//                                js.put(Constants.image, getFacebookProfilePicture(id).toString());
//                                Log.e("image: ", getFacebookProfilePicture(id).toString() );
//                            }
//                            else
//                            {
//                                js.put(Constants.image,"");
//                                Log.e("image22", "" );
//                            }
                            //Log.e("image: ", getFacebookProfilePicture(id).toString() );
                            getFacebookProfilePicture(id);
                            js.put(Constants.phone_no, "4567890876");
                            js.put(Constants.social_type, "facebook");
                            js.put(Constants.image, imageURL);
                            Log.e("setProfileToView: ", js.toString());
                            loginfb(js);

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,birthday");//,email,gender,birthday"
        request.setParameters(parameters);
        request.executeAsync();
    }

    public static String getFacebookProfilePicture(String userID)
    {
        Bitmap bitmap = null;
        try
        {
            imageURL = new URL("https://graph.facebook.com/" + userID + "/picture?type=large");
            bitmap = BitmapFactory.decodeStream(imageURL.openConnection().getInputStream());
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return encoded;
    }

    public void loginfb(final JSONObject json1)
    {
        String URL= GlobalConstants.SOCIALLOGIN;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    String message = s.getString("message");
                    if (status.equalsIgnoreCase("1"))
                    {
                        progressDialog.dismiss();
                        JSONObject jsonobj=s.getJSONObject("data");
                        String id=jsonobj.getString("id");
                        String level_id=jsonobj.getString("level_id");
                        String level_name=jsonobj.getString("level_name");
                        String name=jsonobj.getString("name");
                        String image=jsonobj.getString("image");
                        String date_of_birth=jsonobj.getString("date_of_birth");
                        String email=jsonobj.getString("email");
                        String gender=jsonobj.getString("gender");
                        String address=jsonobj.getString("address");
                        String push_notification=jsonobj.getString("push_notification");
                        String user_level_for_api=jsonobj.getString("user_level_for_api");

                        JSONArray jsarr=jsonobj.getJSONArray("available_level");
                        for(int i=0;i<jsarr.length();i++)
                        {
                            JSONObject jsobj=jsarr.getJSONObject(i);
                            String idd=jsobj.getString("id");
                            String levelname=jsobj.getString("level_name");
                            String description=jsobj.getString("description");
                            String is_locked=jsobj.getString("is_locked");
                            String total_videos=jsobj.getString("total_videos");
                            String seen_videos=jsobj.getString("seen_videos");
                            String percentage_seen=jsobj.getString("percentage_seen");
                            levels.put("id",idd);
                            levels.put("level_name",levelname);
                            levels.put("description",description);
                            levels.put("is_locked",is_locked);
                            levels.put("total_videos",total_videos);
                            levels.put("seen_videos",seen_videos);
                            levels.put("percentage_seen",percentage_seen);
                            levellist.add(levels);
                            dialoglevel.add(new DailogLevelModel(idd, levelname,description,is_locked,total_videos,seen_videos,percentage_seen));
                            levels=new HashMap<>();
                        }
                        Gson gson = new Gson();
                        String json = gson.toJson(dialoglevel);
                        ed.putString("levels", json);
                        ed.putString(Constants.pushnotification,push_notification);
                        ed.putString(Constants.gender,gender);
                        ed.putString(Constants.address,address);
                        ed.putString(Constants.date_of_birth,date_of_birth);
                        ed.putString(Constants.email,email);
                        ed.putString(Constants.name, name);
                        ed.putString(Constants.level_id,level_id);
                        ed.putString(Constants.level_name,level_name);
                        ed.putString(Constants.image,image);
                        ed.putString(Constants.id,id);
                        ed.putString(Constants.loginflag,"true");
                        ed.putString(Constants.userapilevel,user_level_for_api);
                        ed.commit();
                        Log.e("onResponse: ", s.toString());
                        Toast.makeText(LoginActivity.this,message, Toast.LENGTH_SHORT).show();
                        Intent ii=new Intent(getApplicationContext(),HomeActivity.class);
                        startActivity(ii);
                        finish();
                    }
                    else
                    {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this,message, Toast.LENGTH_SHORT).show();
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }


                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
               /// headers.put(Constants.image, imageURL+"");
                Log.e("getHeaders: ", headers.toString());
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(LoginActivity.this);
        rQueue.add(request);
    }

    public void progressBarDialog()
    {
        progressDialog= new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }
}
