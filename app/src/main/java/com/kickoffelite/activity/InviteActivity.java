package com.kickoffelite.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.kickoffelite.R;
import com.kickoffelite.helper.Constants;

public class InviteActivity extends AppCompatActivity
{
    Button btn_invite;
    ImageView img_icon;
    RelativeLayout mainlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);
        init();
        setListeners();
    }

    public void init()
    {
        btn_invite=(Button)findViewById(R.id.btn_invite);
        img_icon=(ImageView)findViewById(R.id.img_icon);
        RelativeLayout mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
    }

    public void setListeners()
    {
        img_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_invite.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is the KickOff App.");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
    }
}
