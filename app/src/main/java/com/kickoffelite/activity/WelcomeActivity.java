package com.kickoffelite.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.kickoffelite.R;

public class WelcomeActivity extends Activity implements View.OnClickListener
{
    Button btn_continue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_welcome);
        init();
        setListeners();
    }

    public void init()
    {
        btn_continue=(Button)findViewById(R.id.btn_continue);
    }

    public void setListeners()
    {
        btn_continue.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        Intent i = new Intent(WelcomeActivity.this, HomeActivity.class);
        startActivity(i);
        finish();
    }
}
