package com.kickoffelite.helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kickoffelite.GooglePay.util.IabHelper;
import com.kickoffelite.GooglePay.util.IabResult;
import com.kickoffelite.GooglePay.util.Inventory;
import com.kickoffelite.GooglePay.util.Purchase;
import com.kickoffelite.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class InAppPurchaseMethods extends Activity
{
    private static final String TAG = "com.example.billing";
    static final String ITEM_SKU = "android.test.purchased";
    TextView ads;
    static String finalId = "android.test.purchased";
    // String PRODUCT_ID[] ={"android.test.purchased", "android.test.purchased", "android.test.purchased", "android.test.purchased", "android.test.purchased"};
    boolean mIsPremium = false;
    private static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnSS+Ap37Y3aSxxQz21i4vhBjbYIXNas9W1qGtoOK+TTiF3uuUZyuoqtOqvsyo96L9EQx7DYvT4zrPXHmL0y6FV/SVFQNzba+l6V87zLQT283zSdn3+l0Rd/0DJzrXmnP5L5J5dNWjDY4Yruv3I06h7kPWbii2amA/RX2QSIAwjE21wEc0wzlq0aAuCnqtlEbScXlo856bNj/DBzfhFtaJtnKHUm60XUMLWM7ECeqzGloAfVDflc3AtksLgZT38m7dteUCDPV7XICBVBDwZLQN3OsE2FYqRgirRCcZtQ4HY6Utkvlmu7JHauKZHUjzuxgbhnCm4Sj3BvykdOXvJdaAQIDAQAB"; // PUT
    static IabHelper mHelper;
    static SharedPreferences sp;
    static SharedPreferences.Editor edd;
    static Context ctx;

    public static SharedPreferences pref;
    public static SharedPreferences.Editor edit;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

    }


    ///In App Purchase
    public static void InAppInit(Context context)
    {
        ctx=context;
        String base64EncodedPublicKey = LICENSE_KEY;

        mHelper = new IabHelper(context, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set
        // this to false).
        mHelper.enableDebugLogging(true);

        Log.e(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.e(TAG, "In-app Billing setup failed: " + result);

                } else {
                    Log.e(TAG, "In-app Billing is set up OK");


                    try {
                        mHelper.queryInventoryAsync(mReceivedInventoryListener);
                    } catch (IabHelper.IabAsyncInProgressException e) {
                        e.printStackTrace();
                    }


                }
            }
        });
    }

    static  IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {


            if (result.isFailure()) {
                // Handle failure
                Log.e("inventry failure", result.toString());

            }else {


//            if (inventory.hasPurchase(finalId)) {
//                Toast.makeText(ForAdvertisementActivity.this, "querying owned items)", Toast.LENGTH_SHORT).show();
//            } else {

                Purchase gasPurchase = inventory.getPurchase(String.valueOf(finalId));


                if (gasPurchase != null && verifyDeveloperPayload(gasPurchase)) {
                    Log.e(TAG, "We have product. Consuming it.");


                    try {
                        mHelper.consumeAsync(inventory.getPurchase(String.valueOf(finalId)), mConsumeFinishedListener);
                    } catch (IabHelper.IabAsyncInProgressException e) {
                        e.printStackTrace();
                    }

                    return;
                }
//            }
            }}
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (mHelper == null)
            return;

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }

    }


   static IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.e(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            if (result.isFailure()) {
                Log.e("result message", String.valueOf(result.getResponse()));
//                userSession.saveUAdsStatus("1");
//                Toast.makeText(ForAdvertisementActivity.this, "Done", Toast.LENGTH_SHORT).show();
                Toast.makeText(ctx,"Purchased",Toast.LENGTH_SHORT).show();
//                edd.putString(Constants.level_name,"Moderate");
//                edd.commit();
                return;
            }


            if (!verifyDeveloperPayload(purchase)) {
                //  showToast("Error purchasing. Authenticity verification failed.");
                return;
            }
            Toast.makeText(ctx,"Purchased Successfully",Toast.LENGTH_SHORT).show();
//            edd.putString(Constants.level_name,"Moderate");
//            edd.commit();


            //}

//            userSession.saveUAdsStatus("1");
//            Toast.makeText(ForAdvertisementActivity.this, "Successfully done", Toast.LENGTH_SHORT).show();
            if (purchase.getSku().equals(finalId)) {
                //mHelper.queryInventoryAsync(mReceivedInventoryListener);
                // userSession.saveUAdsStatus("1");


                try {
                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }


            }


        }
    };
    static IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {

            if (result.isSuccess()) {
                edd.putString(Constants.level_name,"Moderate");
                edd.commit();
//                userSession.saveUAdsStatus("1");
//                Toast.makeText(ForAdvertisementActivity.this, "Done", Toast.LENGTH_SHORT).show();



            } else {
                // handle error
                //  userSession.saveUAdsStatus("0");

            }
        }
    };

    static  boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

        return true;
    }


    public static void upgradedialog(final Context context, final String userid,final String levelid)
    {
        final Dialog openDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.upgrade_dialog);
        RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(context,mainlayout);
        ImageView img_cross=(ImageView)openDialog.findViewById(R.id.img_cross);
        Button btn_subscribeall=(Button)openDialog.findViewById(R.id.btn_subscribeall);
        Button subscribe = (Button) openDialog.findViewById(R.id.btn_subscribe);
        subscribe.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                try {
                    mHelper.launchPurchaseFlow((Activity) ctx, String.valueOf(finalId), 10001, mPurchaseFinishedListener,
                            "mypurchasetoken");
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
                JSONObject js=new JSONObject();
                try
                {
                    js.put("user_id",userid);
                    js.put("level_id",levelid);
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                Constants.progressBarDialog(context);
                Upgradelevel(context,js);
            }
        });

        btn_subscribeall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mHelper.launchPurchaseFlow((Activity) ctx, String.valueOf(finalId), 10001, mPurchaseFinishedListener,
                            "mypurchasetoken");
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
                JSONObject js=new JSONObject();
                try
                {
                    js.put("user_id",userid);
                    //  js.put("level_id",pref.getString(Constants.level_id,""));
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                Constants.progressBarDialog(context);
                UpgradeAll(context,js);
            }
        });

        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog.dismiss();
            }
        });
        openDialog.show();
    }


    public static void Upgradelevel(final Context context,final JSONObject json1)
    {
        String URL= GlobalConstants.UpgradeLevel;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    String message=s.getString("message");
                    if (status.equalsIgnoreCase("1"))
                    {
                        Constants.hideLoader();
                    }
                    else
                    {
                        Constants.hideLoader();
                    }
                }
                catch (JSONException e)
                {
                    Constants.hideLoader();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Constants.hideLoader();
                        Toast.makeText(context,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(context);
        rQueue.add(request);
    }


    public static void UpgradeAll(final Context context, final JSONObject json1)
    {
        String URL= GlobalConstants.UpgradeAll;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try
                {
                    String status = s.getString("status");
                    String message=s.getString("message");
                    if (status.equalsIgnoreCase("1"))
                    {
                        Constants.hideLoader();
                    }
                    else
                    {
                        Constants.hideLoader();
                    }
                }
                catch (JSONException e)
                {
                    Constants.hideLoader();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Constants.hideLoader();
                        Toast.makeText(context,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(context);
        rQueue.add(request);
    }


}
