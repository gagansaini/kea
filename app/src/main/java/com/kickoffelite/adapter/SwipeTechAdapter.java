package com.kickoffelite.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.chauthai.swipereveallayout.ViewBinderHelper;
import com.daimajia.swipe.SwipeLayout;
import com.joooonho.SelectableRoundedImageView;
import com.kickoffelite.R;
import com.kickoffelite.activity.DetailsActivity;
import com.kickoffelite.activity.PlayVideoActivity;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.InAppPurchaseMethods;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.model.TechVideoModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by HP on 6/4/2018.
 */

public class SwipeTechAdapter extends RecyclerView.Adapter<SwipeTechAdapter.ViewHolder>
{
    private List<TechVideoModel> moviesList;
    private LayoutInflater mInflater;
    private Context mContext;
    View itemView;
    private final ViewBinderHelper binderHelper = new ViewBinderHelper();
    private SwipeLayout preswipes=null;
    Dialog progressDialog;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    String ss="",videoid,text,vidid,userid,levelid,message;
    AlertDialog.Builder builder;
   // RelativeLayout mainlayout;

        public SwipeTechAdapter(Context context, List<TechVideoModel> moviesList)
        {
            this.moviesList = moviesList;
            mContext = context;
            mInflater = LayoutInflater.from(context);
            sp=mContext.getSharedPreferences(Constants.appname,MODE_PRIVATE);
            ed=sp.edit();

            userid=sp.getString(Constants.id,"");
            levelid=sp.getString(Constants.level_id,"");
        // uncomment if you want to open only one row at a time
        // binderHelper.setOpenOnlyOne(true);
        }

     class ViewHolder extends RecyclerView.ViewHolder
     {
        SwipeLayout swipeLayout;
        RelativeLayout frontLayout,mainlayout,rel_detail;
        RelativeLayout backLayout;
        ImageView img_check,img_tick,img_lock,img_detail,img_fav;
        SelectableRoundedImageView icon;
        TextView txt_name,txt_time,txt_mark,txt_lname,txt_level;

        public ViewHolder(View itemView)
        {
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            frontLayout =(RelativeLayout) itemView.findViewById(R.id.layout_foreground);
            backLayout = (RelativeLayout) itemView.findViewById(R.id.back_layout);
            mainlayout =(RelativeLayout) itemView.findViewById(R.id.mainlayout);
            rel_detail =(RelativeLayout) itemView.findViewById(R.id.rel_detail);
            Constants.overrideFonts(mContext,frontLayout);
            Constants.overrideFonts(mContext,backLayout);
            img_check=(ImageView)itemView.findViewById(R.id.img_check);
            img_tick=(ImageView)itemView.findViewById(R.id.img_tick);
            img_lock=(ImageView)itemView.findViewById(R.id.img_lock);
            img_detail=(ImageView)itemView.findViewById(R.id.img_detail);
            img_fav=(ImageView)itemView.findViewById(R.id.img_fav);
            icon=(SelectableRoundedImageView)itemView.findViewById(R.id.img_video);
            txt_name=(TextView)itemView.findViewById(R.id.txt_name);
            txt_time=(TextView)itemView.findViewById(R.id.txt_time);
            txt_mark=(TextView)itemView.findViewById(R.id.txt_mark);
          //  txt_lname=(TextView)itemView.findViewById(R.id.txt_lname);
            txt_level=(TextView)itemView.findViewById(R.id.txt_level);
        }

        public void bind(final String data)
        {

        }
    }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            itemView= mInflater.inflate(R.layout.tech_swipe_items, parent, false);
            return new ViewHolder(itemView);
        }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final TechVideoModel movie = moviesList.get(position);
        holder.txt_name.setText(movie.getVideo_name());
        holder.txt_time.setText(movie.getVideo_time()+" min");
        holder.txt_level.setText(movie.getLevel_name());

        if(movie.getIs_favourite().equalsIgnoreCase("1"))
        {
            holder.img_fav.setImageResource(0);
            Picasso.with(mContext).load(R.drawable.favoritesclicked).into(holder.img_fav);
            holder.img_fav.setTag(R.drawable.favoritesclicked);
        }

        else
        {
            holder.img_fav.setImageResource(0);
            Picasso.with(mContext).load(R.drawable.favortiesicon).into(holder.img_fav);
            holder.img_fav.setTag(R.drawable.favortiesicon);
        }

        String image=movie.getVideo_thumbnail();
        String imageurl="";

        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                imageurl=image;
            }
            else
            {
                imageurl= GlobalConstants.ImageUrl+"/"+image;
            }
            Picasso.with(mContext).load(imageurl).into(holder.icon);
        }
        else
        {
            Picasso.with(mContext).load(R.color.grayback).into(holder.icon);
        }
       // Picasso.with(mContext).load(movie.getVideo_thumbnail()).into(holder.icon);
        holder.rel_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, DetailsActivity.class);
                intent.putExtra(Constants.is_locked,movie.getIs_locked());
                intent.putExtra(Constants.name,movie.getVideo_name());
                intent.putExtra(Constants.totaltime,movie.getVideo_time());
                intent.putExtra(Constants.level_name,movie.getLevel_name());
                intent.putExtra(Constants.level_no,movie.getLevel_no());
                intent.putExtra("drilltagslist",movie.getDrill_id_tags());
                intent.putExtra("techniquetagslist",movie.getTechnique_id_tags());
                intent.putExtra(Constants.desc,movie.getVideo_description());
                intent.putExtra(Constants.pgetitle,movie.getTechnique_name());
                intent.putExtra(Constants.url,movie.getVideo_url());
                intent.putExtra(Constants.image,movie.getVideo_thumbnail());
                mContext.startActivity(intent);
            }
        });

        if(movie.getIs_locked().equalsIgnoreCase("1"))
        {
            holder.img_lock.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.img_lock.setVisibility(View.GONE);
        }
        if(movie.getVideo_status().equalsIgnoreCase("1"))
        {
            holder.img_check.setVisibility(View.VISIBLE);
            holder.img_check.setBackgroundResource( 0 );
            Picasso.with(mContext).load(R.drawable.marked).into( holder.img_check);
            holder.img_tick.setBackgroundResource( 0 );
            Picasso.with(mContext).load(R.drawable.delete).into( holder.img_tick);
            holder.txt_mark.setText("Remove");
            holder.img_tick.setColorFilter(mContext.getResources().getColor(R.color.white));
            holder.backLayout.setBackgroundColor(mContext.getResources().getColor(R.color.red));
            text=holder.txt_mark.getText().toString();
        }
        else
            {
                holder.img_check.setBackgroundResource( 0 );
                Picasso.with(mContext).load(R.drawable.unmarked).into( holder.img_check);
                holder.txt_mark.setText("Complete");
                holder.backLayout.setBackgroundColor(mContext.getResources().getColor(R.color.lghtgreen));
                text=holder.txt_mark.getText().toString();
            }

            holder.img_tick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    videoid=movie.getVideo_id();
                    if(holder.txt_mark.getText().toString().equalsIgnoreCase("Complete"))
                    {
                        alertDialogMark(holder);
                        holder.swipeLayout.close();
                    }

                    else
                    {
                        Toast.makeText(getApplicationContext(),"Already marked",Toast.LENGTH_SHORT).show();
                        holder.swipeLayout.close();
                    }
                }
            });

        holder.img_fav.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if ((Integer)holder.img_fav.getTag()==(R.drawable.favortiesicon))
                {
                    Log.e( "imgfave: ","light"+movie.getVideo_id() );
                    JSONObject js=new JSONObject();
                    try
                    {
                        js.put("user_id",sp.getString(Constants.id,""));
                        js.put("video_id",movie.getVideo_id());
                        js.put("status","1");
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                    progressBarDialog();
                    Addfavorite(js,holder,movie.getVideo_id());
                }

                else
                {
                    Log.e( "imgfave: ","dark"+movie.getVideo_id());
                    JSONObject js=new JSONObject();
                    try
                    {
                        js.put("user_id",sp.getString(Constants.id,""));
                        js.put("video_id",movie.getVideo_id());
                        js.put("status","0");
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                    progressBarDialog();
                    Addfavorite(js,holder,movie.getVideo_id());
                }
            }
        });

        holder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(movie.getIs_locked().equalsIgnoreCase("1"))
                {
                    if(sp.getString(Constants.checked,"0").equalsIgnoreCase("1"))
                    {
                        InAppPurchaseMethods.InAppInit(mContext);
                        InAppPurchaseMethods.upgradedialog(mContext, userid, levelid);
                    }
                    {
                        alertDialogUnlock(mContext,userid,levelid);
                    }
                }
                else {
                    Intent ii = new Intent(mContext, PlayVideoActivity.class);
                    ii.putExtra(Constants.name, movie.getVideo_name());
                    ii.putExtra(Constants.url, movie.getVideo_url());
                    ii.putExtra(Constants.image, movie.getVideo_thumbnail());
                    ii.putExtra(Constants.video_id, movie.getVideo_id());
                    ii.putExtra(Constants.is_locked, movie.getIs_locked());
                    ii.putExtra(Constants.VideoType, "technique");
                    ii.putExtra(Constants.video_status, movie.getVideo_status());
                    ii.putExtra(Constants.totaltime, movie.getVideo_time());
                    ii.putExtra(Constants.level_name, movie.getLevel_name());
                    ii.putExtra("drilltagslist", movie.getDrill_id_tags());
                    ii.putExtra("techniquetagslist", movie.getTechnique_id_tags());
                    ii.putExtra(Constants.desc, movie.getVideo_description());
                    ii.putExtra(Constants.pgetitle, movie.getTechnique_name());
                    ii.putExtra(Constants.level_no, movie.getLevel_no());
                    mContext.startActivity(ii);
                }
            }
        });
        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {

            @Override
            public void onStartOpen(SwipeLayout swipeLayout)
            {
                if(preswipes==null)
                {
                    preswipes=swipeLayout;
                }
                else
                {
                    preswipes.close(true);
                    preswipes=swipeLayout;
                }
            }

            @Override
            public void onOpen(SwipeLayout swipeLayout)
            {

            }

            @Override
            public void onStartClose(SwipeLayout swipeLayout)
            {

            }

            @Override
            public void onClose(SwipeLayout swipeLayout)
            {

            }

            @Override
            public void onUpdate(SwipeLayout swipeLayout, int i, int i1)
            {

            }

            @Override
            public void onHandRelease(SwipeLayout swipeLayout, float v, float v1)
            {
                if (swipeLayout != null) {
                    swipeLayout.close();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


    public void markComplete(final JSONObject json1, final ViewHolder holder1, final String videeid)
    {
        String URL= GlobalConstants.MARKVIDEO;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    String message=s.getString("message");
                    if (status.equalsIgnoreCase("1"))
                    {
                        progressDialog.dismiss();
                        Toast.makeText(mContext,message, Toast.LENGTH_SHORT).show();
//
//                    if(message.equalsIgnoreCase("Video unmarked successfully"))
//                    {
                        Log.e( "unmark: ", message);
                        holder1.img_check.setBackgroundResource(0);
                        Picasso.with(mContext).load(R.drawable.marked).into( holder1.img_check);
                        holder1.txt_mark.setText("Completed");
                        holder1.img_tick.setBackgroundResource( 0 );
                        holder1.backLayout.setBackgroundColor(mContext.getResources().getColor(R.color.lghtgreen));
                        Picasso.with(mContext).load(R.drawable.checkedwhite).into( holder1.img_tick);
//                    }
//
//                    else if(message.equalsIgnoreCase("Video marked succesfully"))
//                    {
//                        Log.e( "mark: ", message);
//                        holder1.img_check.setVisibility(View.VISIBLE);
//                        holder1.img_check.setBackgroundResource( 0 );
//                        Picasso.with(mContext).load(R.drawable.marked).into( holder1.img_check);
//                        holder1.img_tick.setBackgroundResource( 0 );
//                        Picasso.with(mContext).load(R.drawable.delete).into( holder1.img_tick);
//                        holder1.img_tick.setColorFilter(mContext.getResources().getColor(R.color.white));
//                        holder1.txt_mark.setText("Remove");
//                        holder1.backLayout.setBackgroundColor(mContext.getResources().getColor(R.color.red));
//                        text=message;
//                        vidid=videeid;
//                    }

//                    else
//                    {
//                        holder1.img_check.setBackgroundResource( 0 );
//                        Picasso.with(mContext).load(R.drawable.unmarked).into( holder1.img_check);
//                    }
                    }
                    else
                    {
                        ss="0";
                        progressDialog.dismiss();
                        Toast.makeText(mContext,message, Toast.LENGTH_SHORT).show();

                    }
                    holder1.swipeLayout.close();

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(mContext,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }

                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(mContext);
        rQueue.add(request);
    }

    public void Addfavorite(final JSONObject json1, final ViewHolder holder1, final String videeid)
    {
        String URL= GlobalConstants.AddFavorite;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    String message=s.getString("message");
                    if (status.equalsIgnoreCase("1"))
                    {
                        progressDialog.dismiss();
                        Toast.makeText(mContext,message, Toast.LENGTH_SHORT).show();
                        if(message.equalsIgnoreCase("Video successfully added to favourite"))
                        {
                            holder1.img_fav.setImageResource(0);
                            Picasso.with(mContext).load(R.drawable.favoritesclicked).into(holder1.img_fav);
                            holder1.img_fav.setTag(R.drawable.favoritesclicked);
                        }
                        else
                        {
                            holder1.img_fav.setImageResource(0);
                            Picasso.with(mContext).load(R.drawable.favortiesicon).into(holder1.img_fav);
                            holder1.img_fav.setTag(R.drawable.favortiesicon);
                        }

                    }

                    else
                    {
                        progressDialog.dismiss();
                        Toast.makeText(mContext,message, Toast.LENGTH_SHORT).show();
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(mContext,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }

                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(mContext);
        rQueue.add(request);
    }

    public void progressBarDialog()
    {
        progressDialog= new Dialog(mContext);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    @SuppressLint("ResourceAsColor")
    public void alertDialogUnMark(final ViewHolder holder)
    {
        final Dialog openDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.background));
       // openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.mark_dilaog);
        RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        ImageView img_alert=(ImageView)openDialog.findViewById(R.id.img_alert);
        Picasso.with(getApplicationContext()).load(R.drawable.attention).into(img_alert);
        TextView txt_cancel=(TextView)openDialog.findViewById(R.id.txt_cancel);
        TextView txt_remove=(TextView)openDialog.findViewById(R.id.txt_remove);
        TextView txt_alertmsg=(TextView)openDialog.findViewById(R.id.txt_alertmsg);
        txt_alertmsg.setText(mContext.getResources().getString(R.string.alertmsgremove));
        txt_remove.setText("Remove");
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDialog.cancel();
            }
        });
        txt_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject js=new JSONObject();
                try {
                    js.put(Constants.user_id,sp.getString(Constants.id,""));
                    js.put(Constants.video_id,videoid);
                    js.put(Constants.complete_status,"0");
                } catch (JSONException e)
                {
                    e.printStackTrace();
                }
                if(Constants.isNetworkAvailable(mContext))
                {
                    progressBarDialog();
                    markComplete(js,holder,videoid);
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                }
                openDialog.cancel();
            }
        });
        openDialog.show();
    }

    @SuppressLint("ResourceAsColor")
    public void alertDialogMark(final ViewHolder holder)
    {
        final Dialog openDialog = new Dialog(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.background));
        //openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.mark_dilaog);
        RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        ImageView img_alert=(ImageView)openDialog.findViewById(R.id.img_alert);
        Picasso.with(getApplicationContext()).load(R.drawable.complte).into(img_alert);
        TextView txt_cancel=(TextView)openDialog.findViewById(R.id.txt_cancel);
        TextView txt_remove=(TextView)openDialog.findViewById(R.id.txt_remove);
        TextView txt_alertmsg=(TextView)openDialog.findViewById(R.id.txt_alertmsg);
        txt_alertmsg.setText(mContext.getResources().getString(R.string.alertmsgcomplete));
        txt_remove.setText("Complete");
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDialog.cancel();
            }
        });
        txt_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Constants.isNetworkAvailable(mContext))
                {
                    JSONObject js=new JSONObject();
                    try {
                        js.put(Constants.user_id,sp.getString(Constants.id,""));
                        js.put(Constants.video_id,videoid);
                        js.put(Constants.complete_status,"1");
                        progressBarDialog();
                        markComplete(js,holder,videoid);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                }
                openDialog.cancel();
            }
        });
        openDialog.show();
    }


    @SuppressLint("ResourceAsColor")
    public void alertDialogUnlock(final Context context, final String userid, final String levelid)
    {
        final Dialog openDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.background));
        //openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.complete_level_dialog);
        RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(context,mainlayout);
        ImageView img_cross=(ImageView)openDialog.findViewById(R.id.img_cross);
        TextView txt_cancel=(TextView)openDialog.findViewById(R.id.txt_cancel);
        TextView txt_proceed=(TextView)openDialog.findViewById(R.id.txt_proceed);
        CheckBox checkbox=(CheckBox)openDialog.findViewById(R.id.checkbox);
        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog.cancel();
            }
        });
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                Toast.makeText(context,String.valueOf(isChecked),Toast.LENGTH_SHORT).show();
                if(isChecked==true)
                {
                    ed.putString(Constants.checked,"1");
                    ed.commit();
                }
                else
                {
                    ed.putString(Constants.checked,"0");
                    ed.commit();
                }

            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDialog.cancel();
            }
        });

        txt_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                InAppPurchaseMethods.upgradedialog(context, userid, levelid);
            }
        });

        openDialog.show();
    }

}
