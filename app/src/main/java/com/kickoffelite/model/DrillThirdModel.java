package com.kickoffelite.model;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by HP on 5/23/2018.
 */

public class DrillThirdModel
{
    String id,level_id,drill_id,drill_name,video_id,level_no,video_name,video_url,video_thumbnail,video_time,level_name,is_locked,video_status,video_description,pagetitle,is_favourite;
    ArrayList<HashMap<String, String>> drill_id_tags=new ArrayList<>();
    ArrayList<HashMap<String, String>> technique_id_tags=new ArrayList<>();

    public DrillThirdModel(String id, String level_id, String drill_id, String drill_name, String video_id, String level_no, String video_name, String video_url, String video_thumbnail, String video_time, String level_name, String is_locked, String video_status,String video_description,String pagetitle,String is_favourite,ArrayList<HashMap<String, String>> drill_id_tags,ArrayList<HashMap<String, String>> technique_id_tags) {
        this.id = id;
        this.level_id = level_id;
        this.drill_id = drill_id;
        this.drill_name = drill_name;
        this.video_id = video_id;
        this.level_no = level_no;
        this.video_name = video_name;
        this.video_url = video_url;
        this.video_thumbnail = video_thumbnail;
        this.video_time = video_time;
        this.level_name = level_name;
        this.is_locked = is_locked;
        this.video_status = video_status;
        this.video_description=video_description;
        this.pagetitle=pagetitle;
        this.is_favourite=is_favourite;
        this.drill_id_tags=drill_id_tags;
        this.technique_id_tags=technique_id_tags;
    }

    public String getIs_favourite() {
        return is_favourite;
    }

    public void setIs_favourite(String is_favourite) {
        this.is_favourite = is_favourite;
    }

    public String getPagetitle() {
        return pagetitle;
    }

    public void setPagetitle(String pagetitle) {
        this.pagetitle = pagetitle;
    }

    public ArrayList<HashMap<String, String>> getDrill_id_tags() {
        return drill_id_tags;
    }

    public ArrayList<HashMap<String, String>> getTechnique_id_tags() {
        return technique_id_tags;
    }

    public String getVideo_description() {
        return video_description;
    }

    public void setVideo_description(String video_description) {
        this.video_description = video_description;
    }


    public void setDrill_id_tags(ArrayList<HashMap<String, String>> drill_id_tags) {
        this.drill_id_tags = drill_id_tags;
    }

    public void setTechnique_id_tags(ArrayList<HashMap<String, String>> technique_id_tags) {
        this.technique_id_tags = technique_id_tags;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLevel_id() {
        return level_id;
    }

    public void setLevel_id(String level_id) {
        this.level_id = level_id;
    }

    public String getDrill_id() {
        return drill_id;
    }

    public void setDrill_id(String drill_id) {
        this.drill_id = drill_id;
    }

    public String getDrill_name() {
        return drill_name;
    }

    public void setDrill_name(String drill_name) {
        this.drill_name = drill_name;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getLevel_no() {
        return level_no;
    }

    public void setLevel_no(String level_no) {
        this.level_no = level_no;
    }

    public String getVideo_name() {
        return video_name;
    }

    public void setVideo_name(String video_name) {
        this.video_name = video_name;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getVideo_thumbnail() {
        return video_thumbnail;
    }

    public void setVideo_thumbnail(String video_thumbnail) {
        this.video_thumbnail = video_thumbnail;
    }

    public String getVideo_time() {
        return video_time;
    }

    public void setVideo_time(String video_time) {
        this.video_time = video_time;
    }

    public String getLevel_name() {
        return level_name;
    }

    public void setLevel_name(String level_name) {
        this.level_name = level_name;
    }

    public String getIs_locked() {
        return is_locked;
    }

    public void setIs_locked(String is_locked) {
        this.is_locked = is_locked;
    }

    public String getVideo_status() {
        return video_status;
    }

    public void setVideo_status(String video_status) {
        this.video_status = video_status;
    }
}
