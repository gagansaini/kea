package com.kickoffelite.model;

import java.util.ArrayList;
import java.util.HashMap;

public class LevelVideosModel
{
    String id,video_id,title,description,video_url,thumbnail_url,level_id,level_no,video_time,is_locked,video_status,levelname,video_description,pgetitle,is_favourite;
    ArrayList<HashMap<String, String>> drill_id_tags=new ArrayList<>();
    ArrayList<HashMap<String, String>> technique_id_tags=new ArrayList<>();

    public LevelVideosModel(String id, String video_id,String title, String description, String video_url, String thumbnail_url, String level_id, String level_no, String video_time, String is_locked, String video_status,String levelname,String pgetitle,String is_favourite,ArrayList<HashMap<String, String>> drill_id_tags,ArrayList<HashMap<String, String>> technique_id_tags) {
        this.id = id;
        this.title = title;
        this.video_id=video_id;
        this.description = description;
        this.video_url = video_url;
        this.thumbnail_url = thumbnail_url;
        this.level_id = level_id;
        this.level_no = level_no;
        this.video_time = video_time;
        this.is_locked = is_locked;
        this.video_status = video_status;
        this.levelname=levelname;
        this.video_description=video_description;
        this.pgetitle=pgetitle;
        this.is_favourite=is_favourite;
        this.drill_id_tags=drill_id_tags;
        this.technique_id_tags=technique_id_tags;
    }

    public String getIs_favourite() {
        return is_favourite;
    }

    public void setIs_favourite(String is_favourite) {
        this.is_favourite = is_favourite;
    }

    public String getVideo_description() {
        return video_description;
    }

    public void setVideo_description(String video_description) {
        this.video_description = video_description;
    }

    public String getPgetitle() {
        return pgetitle;
    }

    public void setPgetitle(String pgetitle) {
        this.pgetitle = pgetitle;
    }

    public ArrayList<HashMap<String, String>> getDrill_id_tags() {
        return drill_id_tags;
    }

    public ArrayList<HashMap<String, String>> getTechnique_id_tags() {
        return technique_id_tags;
    }


    public void setDrill_id_tags(ArrayList<HashMap<String, String>> drill_id_tags) {
        this.drill_id_tags = drill_id_tags;
    }

    public void setTechnique_id_tags(ArrayList<HashMap<String, String>> technique_id_tags) {
        this.technique_id_tags = technique_id_tags;
    }

    public String getLevelname() {
        return levelname;
    }

    public void setLevelname(String levelname) {
        this.levelname = levelname;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public String getLevel_id() {
        return level_id;
    }

    public void setLevel_id(String level_id) {
        this.level_id = level_id;
    }

    public String getLevel_no() {
        return level_no;
    }

    public void setLevel_no(String level_no) {
        this.level_no = level_no;
    }

    public String getVideo_time() {
        return video_time;
    }

    public void setVideo_time(String video_time) {
        this.video_time = video_time;
    }

    public String getIs_locked() {
        return is_locked;
    }

    public void setIs_locked(String is_locked) {
        this.is_locked = is_locked;
    }

    public String getVideo_status() {
        return video_status;
    }

    public void setVideo_status(String video_status) {
        this.video_status = video_status;
    }
}
