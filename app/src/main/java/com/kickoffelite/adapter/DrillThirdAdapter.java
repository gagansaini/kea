package com.kickoffelite.adapter;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kickoffelite.R;
import com.kickoffelite.activity.DetailsActivity;
import com.kickoffelite.activity.PlayVideoActivity;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.InAppPurchaseMethods;
import com.kickoffelite.model.DrillThirdModel;
import com.kickoffelite.helper.GlobalConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class DrillThirdAdapter extends RecyclerView.Adapter<DrillThirdAdapter.MyViewHolder>
{
    private List<DrillThirdModel> moviesList;
    Context context;
    Dialog progressDialog;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    String userid,levelid;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView name, time,txt_lname;
        ImageView img_video,img_mark,img_lock,img_detail,img_fav;
        RelativeLayout mainlayout,rel_detail;

        public MyViewHolder(View view)
        {
            super(view);
            mainlayout=(RelativeLayout)view.findViewById(R.id.mainlayout);
            Constants.overrideFonts(context,mainlayout);
            name = (TextView) view.findViewById(R.id.txt_name);
            time = (TextView) view.findViewById(R.id.txt_time);
            txt_lname = (TextView) view.findViewById(R.id.txt_lname);
            img_video = (ImageView) view.findViewById(R.id.img_video);
            img_mark=(ImageView)view.findViewById(R.id.img_mark);
            img_lock=(ImageView)view.findViewById(R.id.img_lock);
            img_detail=(ImageView)view.findViewById(R.id.img_detail);
            img_fav=(ImageView)view.findViewById(R.id.img_fav);
            rel_detail=(RelativeLayout)view.findViewById(R.id.rel_detail);
        }
    }

    public DrillThirdAdapter(Context context,List<DrillThirdModel> moviesList)
    {
        this.moviesList = moviesList;
        this.context = context;
        sp=context.getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        userid=sp.getString(Constants.id,"");
        levelid=sp.getString(Constants.level_id,"");
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drill_third_recycler, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position)
    {
        final DrillThirdModel movie = moviesList.get(position);
        holder.name.setText(movie.getVideo_name());
        holder.time.setText(movie.getVideo_time()+" min");
        holder.txt_lname.setText(movie.getLevel_name());

        if(movie.getIs_favourite().equalsIgnoreCase("1"))
        {
            holder.img_fav.setImageResource(0);
            Picasso.with(context).load(R.drawable.favoritesclicked).into(holder.img_fav);
            holder.img_fav.setTag(R.drawable.favoritesclicked);
        }

        else
        {
            holder.img_fav.setImageResource(0);
            Picasso.with(context).load(R.drawable.favortiesicon).into(holder.img_fav);
            holder.img_fav.setTag(R.drawable.favortiesicon);
        }

        holder.img_fav.setOnClickListener(new View.OnClickListener()
        {
            @SuppressLint("ResourceType")
            @Override
            public void onClick(View v)
            {
                if ((Integer)holder.img_fav.getTag()==(R.drawable.favortiesicon))
                {
                    Log.e( "imgfave: ","light"+movie.getVideo_id() );
                    JSONObject js=new JSONObject();
                    try
                    {
                        js.put("user_id",sp.getString(Constants.id,""));
                        js.put("video_id",movie.getVideo_id());
                        js.put("status","1");
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                    progressBarDialog();
                    Addfavorite(js,holder,movie.getVideo_id());
                }

                else
                {
                    Log.e( "imgfave: ","dark"+movie.getVideo_id());
                    JSONObject js=new JSONObject();
                    try
                    {
                        js.put("user_id",sp.getString(Constants.id,""));
                        js.put("video_id",movie.getVideo_id());
                        js.put("status","0");
                    }
                    catch (JSONException e)
                    {
                        e.printStackTrace();
                    }
                    progressBarDialog();
                    Addfavorite(js,holder,movie.getVideo_id());
                }

            }
        });

        holder.rel_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, DetailsActivity.class);
                intent.putExtra(Constants.name,movie.getVideo_name());
                intent.putExtra(Constants.totaltime,movie.getVideo_time());
                intent.putExtra(Constants.level_name,movie.getLevel_name());
                intent.putExtra(Constants.level_no,movie.getLevel_no());
                intent.putExtra(Constants.is_locked,movie.getIs_locked());
                intent.putExtra("drilltagslist",movie.getDrill_id_tags());
                intent.putExtra("techniquetagslist",movie.getTechnique_id_tags());
                intent.putExtra(Constants.desc,movie.getVideo_description());
                intent.putExtra(Constants.pgetitle,movie.getPagetitle());
                intent.putExtra(Constants.url,movie.getVideo_url());
                intent.putExtra(Constants.image,movie.getVideo_thumbnail());
                context.startActivity(intent);
            }
        });

        if(movie.getVideo_status().equalsIgnoreCase("1"))
        {
            holder.img_mark.setVisibility(View.VISIBLE);
            holder.img_mark.setBackgroundResource( 0 );
            Picasso.with(context).load(R.drawable.marked).into(holder.img_mark);
        }

        else
        {
           // holder.img_mark.setVisibility(View.INVISIBLE);
            holder.img_mark.setBackgroundResource( 0 );
            Picasso.with(context).load(R.drawable.unmarked).into(holder.img_mark);
        }

        if(movie.getIs_locked().equalsIgnoreCase("1"))
        {
            holder.img_lock.setVisibility(View.VISIBLE);
        }

        else
        {
            holder.img_lock.setVisibility(View.GONE);
        }

        holder.img_video.setVisibility(View.VISIBLE);
        holder.img_video.setScaleType(ImageView.ScaleType.CENTER_CROP);
        // holder.img_video.setImageResource(movie.getImg_video());
        String image=movie.getVideo_thumbnail();
        String imageurl="";

                if(!image.equalsIgnoreCase(""))
                {
                    if(image.contains("http"))
                    {
                        imageurl=image;
                    }
                    else
                    {
                        imageurl= GlobalConstants.ImageUrl+"/"+image;
                    }
                    Picasso.with(context).load(imageurl).into(holder.img_video);
                }
                else
                {
                    Picasso.with(context).load(R.color.grayback).into(holder.img_video);
                }

       // Picasso.with(context).load(movie.getVideo_thumbnail()).into(holder.img_video);
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(movie.getIs_locked().equalsIgnoreCase("1"))
                {
                    if(sp.getString(Constants.checked,"0").equalsIgnoreCase("1"))
                    {
                        InAppPurchaseMethods.InAppInit(context);
                        InAppPurchaseMethods.upgradedialog(context, userid, levelid);
                    }
                    {
                        alertDialogUnlock(context,userid,levelid);
                    }
                }
                else {
                    Intent ii = new Intent(context, PlayVideoActivity.class);
                    ii.putExtra(Constants.name, movie.getVideo_name());
                    ii.putExtra(Constants.url, movie.getVideo_url());
                    ii.putExtra(Constants.image, movie.getVideo_thumbnail());
                    ii.putExtra(Constants.video_id, movie.getVideo_id());
                    ii.putExtra(Constants.is_locked, movie.getIs_locked());
                    ii.putExtra(Constants.video_status, movie.getVideo_status());
                    //intent.putExtra(Constants.name,movie.getVideo_name());
                    ii.putExtra(Constants.totaltime, movie.getVideo_time());
                    ii.putExtra(Constants.level_name, movie.getLevel_name());
                    ii.putExtra("drilltagslist", movie.getDrill_id_tags());
                    ii.putExtra("techniquetagslist", movie.getTechnique_id_tags());
                    ii.putExtra(Constants.desc, movie.getVideo_description());
                    ii.putExtra(Constants.VideoType, "drill");
                    ii.putExtra(Constants.pgetitle, movie.getPagetitle());
                    ii.putExtra(Constants.level_no, movie.getLevel_no());
                    context.startActivity(ii);
                }
            }
        });
        ///
    }

    public void Addfavorite(final JSONObject json1, final MyViewHolder holder1, final String videeid)
    {
        String URL= GlobalConstants.AddFavorite;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    String message=s.getString("message");
                    if (status.equalsIgnoreCase("1"))
                    {
                        progressDialog.dismiss();
                        Toast.makeText(context,message, Toast.LENGTH_SHORT).show();
                        if(message.equalsIgnoreCase("Video successfully added to favourite"))
                        {
                            holder1.img_fav.setImageResource(0);
                            Picasso.with(context).load(R.drawable.favoritesclicked).into(holder1.img_fav);
                            holder1.img_fav.setTag(R.drawable.favoritesclicked);
                        }
                        else
                        {
                            holder1.img_fav.setImageResource(0);
                            Picasso.with(context).load(R.drawable.favortiesicon).into(holder1.img_fav);
                            holder1.img_fav.setTag(R.drawable.favortiesicon);
                        }

                    }

                    else
                    {
                        progressDialog.dismiss();
                        Toast.makeText(context,message, Toast.LENGTH_SHORT).show();
                    }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(context,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }

                }) {

            @Override
            public Map<String, String> getHeaders()
            {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(context);
        rQueue.add(request);
    }

    @SuppressLint("ResourceAsColor")
    public void alertDialogUnlock(final Context context, final String userid, final String levelid)
    {
        final Dialog openDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.background));
        //openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.complete_level_dialog);
        RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(context,mainlayout);
        ImageView img_cross=(ImageView)openDialog.findViewById(R.id.img_cross);
        TextView txt_cancel=(TextView)openDialog.findViewById(R.id.txt_cancel);
        TextView txt_proceed=(TextView)openDialog.findViewById(R.id.txt_proceed);
        CheckBox checkbox=(CheckBox)openDialog.findViewById(R.id.checkbox);
        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog.cancel();
            }
        });
        checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                Toast.makeText(context,String.valueOf(isChecked),Toast.LENGTH_SHORT).show();
                if(isChecked==true)
                {
                    ed.putString(Constants.checked,"1");
                    ed.commit();
                }
                else
                {
                    ed.putString(Constants.checked,"0");
                    ed.commit();
                }

            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDialog.cancel();
            }
        });

        txt_proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                InAppPurchaseMethods.upgradedialog(context, userid, levelid);
            }
        });

        openDialog.show();
    }



    public void progressBarDialog()
    {
        progressDialog= new Dialog(context);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }



    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}