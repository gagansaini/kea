package com.kickoffelite.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kickoffelite.R;
import com.kickoffelite.activity.DetailsActivity;
import com.kickoffelite.activity.PlayVideoActivity;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.model.FavoriteModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.MyViewHolder> {

    private List<FavoriteModel> moviesList;
    Context context;
    Dialog progressDialog;
    SharedPreferences sp;
    SharedPreferences.Editor ed;



    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, time,txt_level,txt_levelno;
        ImageView img_video,img_mark,img_lock,img_detail;
        RelativeLayout mainlayout,rel_detail;


        public MyViewHolder(View view) {
            super(view);
            mainlayout=(RelativeLayout)view.findViewById(R.id.mainlayout);
            Constants.overrideFonts(context,mainlayout);
            rel_detail=(RelativeLayout)view.findViewById(R.id.rel_detail);
            txt_levelno= (TextView) view.findViewById(R.id.txt_levelno);
            name = (TextView) view.findViewById(R.id.txt_name);
            time = (TextView) view.findViewById(R.id.txt_time);
            txt_level = (TextView) view.findViewById(R.id.txt_level);
            img_video = (ImageView) view.findViewById(R.id.img_video);
            //img_mark=(ImageView)view.findViewById(R.id.img_mark);
            img_lock=(ImageView)view.findViewById(R.id.img_lock);
            img_detail=(ImageView)view.findViewById(R.id.img_detail);
        }
    }

    public FavoriteAdapter(Context context,List<FavoriteModel> moviesList) {
        this.moviesList = moviesList;
        this.context = context;
    }

    @Override
    public FavoriteAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favorite_item, parent, false);

        return new FavoriteAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FavoriteAdapter.MyViewHolder holder, int position) {
        final FavoriteModel movie = moviesList.get(position);
        holder.name.setText(movie.getVideo_name());
        holder.time.setText(movie.getVideo_time()+" min");
        holder.txt_level.setText(movie.getLevel_name());
        String image=movie.getVideo_thumbnail();

        holder.rel_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, DetailsActivity.class);
                intent.putExtra(Constants.name,movie.getVideo_name());
                intent.putExtra(Constants.totaltime,movie.getVideo_time());
                intent.putExtra(Constants.level_name,movie.getLevel_name());
                intent.putExtra(Constants.desc,movie.getDescription());
                intent.putExtra(Constants.level_no,movie.getLevel_no());
                intent.putExtra(Constants.is_locked,movie.getIs_locked());
                //intent.putExtra(Constants.pgetitle,movie.());
                intent.putExtra(Constants.url,movie.getVideo_url());
                intent.putExtra("drilltagslist",movie.getDrill_id_tags());
                intent.putExtra("techniquetagslist",movie.getTechnique_id_tags());
                intent.putExtra(Constants.pgetitle, "");
                intent.putExtra(Constants.image,movie.getVideo_thumbnail());
                context.startActivity(intent);
            }
        });

//        if(movie.getVideo_status().equalsIgnoreCase("1"))
//        {
//            holder.img_mark.setVisibility(View.VISIBLE);
//            holder.img_mark.setBackgroundResource( 0 );
//            Picasso.with(context).load(R.drawable.marked).into(holder.img_mark);
//        }
//
//        else
//        {
//            // holder.img_mark.setVisibility(View.INVISIBLE);
//            holder.img_mark.setBackgroundResource( 0 );
//            Picasso.with(context).load(R.drawable.unmarked).into(holder.img_mark);
//        }

        if(movie.getIs_locked().equalsIgnoreCase("1"))
        {
            holder.img_lock.setVisibility(View.VISIBLE);
        }

        else
        {
            holder.img_lock.setVisibility(View.GONE);
        }

        holder.img_video.setVisibility(View.VISIBLE);
        holder.img_video.setScaleType(ImageView.ScaleType.CENTER_CROP);
        // holder.img_video.setImageResource(movie.getImg_video());

        String imageurl="";

        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                imageurl=image;
            }
            else
            {
                imageurl= GlobalConstants.ImageUrl+"/"+image;
            }
            Picasso.with(context).load(imageurl).into(holder.img_video);
        }
        else
        {
            Picasso.with(context).load(R.color.grayback).into(holder.img_video);
        }

        //Picasso.with(context).load(movie.getVideo_thumbnail()).into(holder.img_video);
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent ii=new Intent(context,PlayVideoActivity.class);
                ii.putExtra(Constants.name,movie.getVideo_name());
                ii.putExtra(Constants.url,movie.getVideo_url());
                ii.putExtra(Constants.image,movie.getVideo_thumbnail());
                ii.putExtra(Constants.video_id,movie.getVideo_id());
                ii.putExtra(Constants.is_locked,movie.getIs_locked());
                ii.putExtra(Constants.video_status,movie.getVideo_status());
                //intent.putExtra(Constants.name,movie.getVideo_name());
                ii.putExtra(Constants.totaltime,movie.getVideo_time());
                ii.putExtra(Constants.level_name,movie.getLevel_name());
                ii.putExtra("drilltagslist",movie.getDrill_id_tags());
                ii.putExtra("techniquetagslist",movie.getTechnique_id_tags());
                ii.putExtra(Constants.desc,movie.getDescription());
                ii.putExtra(Constants.VideoType,"drill");
                context.startActivity(ii);
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


}