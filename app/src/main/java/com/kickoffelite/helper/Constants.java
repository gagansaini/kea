package com.kickoffelite.helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kickoffelite.GooglePay.util.IabHelper;
import com.kickoffelite.R;
import com.kickoffelite.tabactivityactivities.SettingsFragment;
import com.wang.avi.AVLoadingIndicatorView;

import org.apache.http.util.ByteArrayBuffer;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by HP on 5/18/2018.
 */

public class Constants
{
   // private static final String TAG ="log" ;
    public static Context context1;
    private static Dialog progressDialog;

    public static SharedPreferences pref;
    public static SharedPreferences.Editor edit;

    public static String appname="kickoff";

    public static String chatURL="";
    public static String switchbutton="switchbutton";

    public static String level_id="level_id";
    public static String level_name="level_name";

    public static String videoid="videoid";
    public static String id="id";
    public static String subid="subid";
    public static String thirdid="thirdid";
    public static String url="url";
    public static String mainname="mainname";
    public static String subname="subname";
    public static String desc="desc";
    //Api
    public static String BaseUrl="http://worksdelight.com/kickoff/index.php/api/";
    //Register
    public static String name="name";
    public static String email="email";
    public static String password="password";
    public static String date_of_birth="date_of_birth";
    public static String phone_no="phone_no";
    public static String image="image";


    public static String gender="gender";
    public static String address="address";

    //fb
    public static String social_type="social_type";
    public static String token="token";

    //Constants

    public static String regflag="regflag";
    public static String loginflag="loginflag";
    public static String old_password="old_password";
    public static String new_password="new_password";
    public static String confirm_password="confirm_password";

    //markcomplete
    public static String user_id="user_id";
    public static String category_id="category_id";
    public static String subcategory_id="subcategory_id";
    public static String video_id="video_id";
    public static String video_status="video_status";
    public static String complete_status="complete_status";
    public static String video_type_id="video_type_id";

    public static String total_session="total_session";
    public static String completed_session="completed_session";


    //Techniques
    public static String TechId="TechId";
    public static String TechName="TechName";
    public static String TechImage="TechImage";
    public static String TechDesc="TechDesc";
    public static String TechSession="TechSession";
    public static String TechVideoUrl="TechVideoUrl";
    public static String TechVideoThumbnail="TechVideoThumbnail";
    public static String VideoType="VideoType";

    public static String markstatus="markstatus";

    public static String totaltime="totaltime";

    public static String is_locked="is_locked";

    public static String pushnotification="pushnotification";

    public static String levelbeg="levelbeg";
    public static String levelmod="levelmod";
    public static String leveladv="leveladv";
    public static String levelpro="levelpro";
    public static String levelelite="levelelite";

    public static String pgetitle="pgetitle";

    public  static  String level_no="level_no";

    public  static  String checked="checked";

    public  static  String userapilevel="userapilevel";


    //InAppPurchase
    private static final String TAG = "com.example.billing";
    static final String ITEM_SKU = "android.test.purchased";
    TextView ads;
    String finalId = "android.test.purchased";
    // String PRODUCT_ID[] ={"android.test.purchased", "android.test.purchased", "android.test.purchased", "android.test.purchased", "android.test.purchased"};
    boolean mIsPremium = false;
    private static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnSS+Ap37Y3aSxxQz21i4vhBjbYIXNas9W1qGtoOK+TTiF3uuUZyuoqtOqvsyo96L9EQx7DYvT4zrPXHmL0y6FV/SVFQNzba+l6V87zLQT283zSdn3+l0Rd/0DJzrXmnP5L5J5dNWjDY4Yruv3I06h7kPWbii2amA/RX2QSIAwjE21wEc0wzlq0aAuCnqtlEbScXlo856bNj/DBzfhFtaJtnKHUm60XUMLWM7ECeqzGloAfVDflc3AtksLgZT38m7dteUCDPV7XICBVBDwZLQN3OsE2FYqRgirRCcZtQ4HY6Utkvlmu7JHauKZHUjzuxgbhnCm4Sj3BvykdOXvJdaAQIDAQAB"; // PUT
    IabHelper mHelper;

    public static void UserSession(Context context) {
        //context1 = context;
        pref = PreferenceManager.getDefaultSharedPreferences(context);
        edit = pref.edit();
    }

    public static void saveUserName(String name) {
        edit.putString(switchbutton, name);
        edit.commit();
    }

    public static String getUserName() {
        String value = pref.getString(switchbutton, null);
        return value;
    }

    public static boolean isValid(String email)
    {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void progressBarDialog(Context context) {

        if (progressDialog == null) {
            progressDialog = new Dialog(context);
            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.setContentView(R.layout.progress_dialog);
            AVLoadingIndicatorView loaderView = (AVLoadingIndicatorView) progressDialog.findViewById(R.id.loader_view);
            loaderView.show();
        }
        progressDialog.show();

    }

    public static void hideLoader() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }


    public static boolean isNetworkAvailable(Context context)
    {
        ConnectivityManager connectivityManager
                = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


//font
    public static void overrideFonts(final Context context, final View v)
    {
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    overrideFonts(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/din-bold.ttf"));
            }
            else if (v instanceof EditText) {
                ((EditText) v).setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/din-bold.ttf"));
            }
        } catch (Exception e) {
        }
    }



    public static void upgradedialog(final Context context, final String userid,final String levelid)
    {
        final Dialog openDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.upgrade_dialog);
        RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(context,mainlayout);
        ImageView img_cross=(ImageView)openDialog.findViewById(R.id.img_cross);
        Button btn_subscribeall=(Button)openDialog.findViewById(R.id.btn_subscribeall);
        Button subscribe = (Button) openDialog.findViewById(R.id.btn_subscribe);



        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog.dismiss();
            }
        });
        openDialog.show();
    }



}
