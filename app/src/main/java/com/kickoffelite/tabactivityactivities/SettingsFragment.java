package com.kickoffelite.tabactivityactivities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kickoffelite.GooglePay.util.IabHelper;
import com.kickoffelite.R;
import com.kickoffelite.activity.DownloadedActivity;
import com.kickoffelite.activity.InviteActivity;
import com.kickoffelite.activity.LoginActivity;
import com.kickoffelite.activity.ProfileActivity;
import com.kickoffelite.activity.ResetPasswordActivity;
import com.kickoffelite.adapter.DailogLevelAdapter;
import com.kickoffelite.adapter.LevelAdapter;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.InAppPurchaseMethods;
import com.kickoffelite.model.DailogLevelModel;
import com.kickoffelite.helper.GlobalConstants;
import com.squareup.picasso.Picasso;
import com.suke.widget.SwitchButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class SettingsFragment extends AppCompatActivity implements View.OnClickListener,DailogLevelAdapter.ItemClickInterface
{
    ArrayList<DailogLevelModel> dialoglevel = new ArrayList<>();
    RelativeLayout rel6pswd,rellogout,rel_down,rel7,rel2,rel8_about,rel9_privacy,mainlayout,relupgrade;
    com.suke.widget.SwitchButton switchButton;
    Dialog progressDialog;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    CircleImageView img_profile;
    AlertDialog builder;
    TextView txt_name,txt_skill,txt_level,txt_enable;
    SharedPreferences sharedPref;
    SharedPreferences.Editor editor;
    AlertDialog.Builder builder1;
    Dialog openDialog1;
    DailogLevelAdapter dialogdapter;

    //In App
    private static final String TAG = "com.example.billing";
    static final String ITEM_SKU = "android.test.purchased";
    TextView ads;
    String finalId = "android.test.purchased";
    // String PRODUCT_ID[] ={"android.test.purchased", "android.test.purchased", "android.test.purchased", "android.test.purchased", "android.test.purchased"};
    boolean mIsPremium = false;
    private static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnSS+Ap37Y3aSxxQz21i4vhBjbYIXNas9W1qGtoOK+TTiF3uuUZyuoqtOqvsyo96L9EQx7DYvT4zrPXHmL0y6FV/SVFQNzba+l6V87zLQT283zSdn3+l0Rd/0DJzrXmnP5L5J5dNWjDY4Yruv3I06h7kPWbii2amA/RX2QSIAwjE21wEc0wzlq0aAuCnqtlEbScXlo856bNj/DBzfhFtaJtnKHUm60XUMLWM7ECeqzGloAfVDflc3AtksLgZT38m7dteUCDPV7XICBVBDwZLQN3OsE2FYqRgirRCcZtQ4HY6Utkvlmu7JHauKZHUjzuxgbhnCm4Sj3BvykdOXvJdaAQIDAQAB"; // PUT
    IabHelper mHelper;
    String userid,levelid;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_settings);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.appcolor));
        }
        init();
        setListeners();
    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
        dialoglevel=Download();
        if(sp.getString(Constants.pushnotification,"0").equalsIgnoreCase("1"))
        {
            Log.e( "init: ",sp.getString(Constants.pushnotification,"0") );
            switchButton.setChecked(true);
           // switchButton.isChecked();
            txt_enable.setText("Enabled");
        }
        else
        {
            Log.e( "initii: ",sp.getString(Constants.pushnotification,"0") );
            switchButton.setChecked(false);
            txt_enable.setText("Disabled");
        }

        // switchmethod();
        txt_name.setText(sp.getString(Constants.name,""));
        if(sp.getString(Constants.level_name,"").equalsIgnoreCase(""))
        {
            txt_skill.setText("Beginner");
        }
        else
        {
            txt_skill.setText(sp.getString(Constants.level_name,""));
        }

        String image=sp.getString(Constants.image,"");
        String url="";

        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                url=image;
            }
            else
            {
                url=GlobalConstants.ImageUrl+"/Uploads/Users/"+sp.getString(Constants.id,"")+"/"+image;
            }
            Log.e( "onRestart: ",url );
            Picasso.with(SettingsFragment.this).load(url).placeholder(R.color.grayback).into(img_profile);
        }
        else
        {

        }
    }

    public void init()
    {
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();

        userid=sp.getString(Constants.id,"");
        levelid=sp.getString(Constants.level_id,"");

        Context context = getApplicationContext();
        InAppPurchaseMethods.InAppInit(SettingsFragment.this);

        dialoglevel=Download();
        mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        txt_enable=(TextView)findViewById(R.id.txt_enable);
        txt_skill=(TextView)findViewById(R.id.txt_level);
        txt_level=(TextView)findViewById(R.id.txt_level);
        txt_skill.setText(sp.getString(Constants.level_name,""));
        img_profile=(CircleImageView)findViewById(R.id.img_profile);
        txt_name=(TextView)findViewById(R.id.txt_name);

        switchButton = (com.suke.widget.SwitchButton) findViewById(R.id.switch_button);
        switchButton.toggle();     //switch state
        switchButton.toggle(true);//switch without animation
        switchButton.setShadowEffect(false);//disable shadow effect
        switchButton.setEnabled(true);//disable button
        switchButton.setEnableEffect(true);//disable the switch animation
        if(sp.getString(Constants.pushnotification,"0").equalsIgnoreCase("1"))
        {
            Log.e( "init: ",sp.getString(Constants.pushnotification,"0") );
            switchButton.setChecked(true);
            switchButton.isChecked();
            txt_enable.setText("Enabled");
        }
        else
        {
            Log.e( "initii: ",sp.getString(Constants.pushnotification,"0") );
            switchButton.toggle();
            switchButton.setChecked(false);
            //switchButton.setEnabled(false);
            txt_enable.setText("Disabled");
        }

        rel6pswd=(RelativeLayout)findViewById(R.id.rel6);
        rel7=(RelativeLayout)findViewById(R.id.rel7);
        rel2=(RelativeLayout)findViewById(R.id.rel2);
        rellogout=(RelativeLayout)findViewById(R.id.rel10);
        rel_down=(RelativeLayout)findViewById(R.id.reldown);
        rel8_about=(RelativeLayout)findViewById(R.id.rel8);
        rel9_privacy=(RelativeLayout)findViewById(R.id.rel9);
        relupgrade=(RelativeLayout)findViewById(R.id.relupgrade);
        txt_name.setText(sp.getString(Constants.name,""));
        if(sp.getString(Constants.level_name,"").equalsIgnoreCase(""))
        {
            txt_skill.setText("Beginner");
        }
        else
        {
            txt_skill.setText(sp.getString(Constants.level_name,""));
        }
        String image=sp.getString(Constants.image,"");
        String url="";

        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                url=image;
            }
            else
            {
                url=GlobalConstants.ImageUrl+"/Uploads/Users/"+sp.getString(Constants.id,"")+"/"+image;
            }
            Log.e( "onRestart: ",url );
            Picasso.with(SettingsFragment.this).load(url).placeholder(R.color.grayback).into(img_profile);
        }
        else
        {

        }
    }

    @Override
    public void onBackPressed() {
        alertDialogExit();
    }

    public void setListeners()
    {
        rel6pswd.setOnClickListener(this);
        rel_down.setOnClickListener(this);
        rellogout.setOnClickListener(this);
        rel7.setOnClickListener(this);
        rel2.setOnClickListener(this);
        rel8_about.setOnClickListener(this);
        rel9_privacy.setOnClickListener(this);
        relupgrade.setOnClickListener(this);
        switchButton.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean b) {

                JSONObject js=new JSONObject();
                if(b==true)
                {  Log.e( "onCheckedChanged: ",b+"" );
                    switchButton.setChecked(true);
                    txt_enable.setText("Enabled");

                    String status="1";
                    try {
                        js.put("user_id",sp.getString(Constants.id,""));
                        js.put("push_notification",status);
                        if(Constants.isNetworkAvailable(getApplicationContext()))
                        {
                            PushNotificationApi(js);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    Log.e( "onCheckedChanged: ",b+"" );
                    switchButton.setChecked(false);
                    txt_enable.setText("Disabled");
                    String status="0";
                    try {
                        js.put("user_id",sp.getString(Constants.id,""));
                        js.put("push_notification",status);
                        if(Constants.isNetworkAvailable(getApplicationContext()))
                        {
                            PushNotificationApi(js);
                        }
                        else
                        {
                            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    @Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
            case R.id.rel6:
                Intent ii=new Intent(SettingsFragment.this, ResetPasswordActivity.class);
                startActivity(ii);
                break;

            case R.id.rel10:
                alertDialogLogout();
                break;

            case R.id.reldown:
                Intent ini=new Intent(SettingsFragment.this, DownloadedActivity.class);
                startActivity(ini);
                break;

            case R.id.rel7:
                Intent intentt=new Intent(SettingsFragment.this, InviteActivity.class);
                startActivity(intentt);
                break;

            case R.id.rel2:
                Intent intent=new Intent(SettingsFragment.this, ProfileActivity.class);
                startActivity(intent);
                break;

            case R.id.rel8:
                Intent intent1=new Intent(Intent.ACTION_VIEW);
                intent1.setData(Uri.parse("https://www.google.com"));
                startActivity(intent1);
                break;

            case R.id.rel9:
                Intent intent2=new Intent(Intent.ACTION_VIEW);
                intent2.setData(Uri.parse("https://www.google.com"));
                startActivity(intent2);
                break;

            case R.id.relupgrade:
                alertDialogLevels();
                break;
        }
    }

    public void LogOut() {

        final String URL = GlobalConstants.LOGOUT;
        StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                try {
                    JSONObject jsob = new JSONObject(s);
                    String response = jsob.getString("status");
                    String message = jsob.getString("message");

                    if (response.equals("1"))
                    {
                       Constants.hideLoader();
                       // Toast.makeText(SettingsFragment.this,message , Toast.LENGTH_LONG).show();
                        ed.clear();
                        ed.commit();
                        LoginManager.getInstance().logOut();
                       // deleteCache(getApplicationContext());
                        Intent iii=new Intent(SettingsFragment.this, LoginActivity.class);
                        Toast.makeText(getApplicationContext(),message,Toast.LENGTH_SHORT).show();
                        startActivity(iii);
                        finish();
                    }
                    else
                    {
                        Constants.hideLoader();
                    }
                } catch (JSONException e) {
                    Constants.hideLoader();
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Constants.hideLoader();
                Toast.makeText(SettingsFragment.this, "Some error occurred -> " + volleyError, Toast.LENGTH_LONG).show();

            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(SettingsFragment.this);
        rQueue.add(request);
        //Toast.makeText(getApplicationContext(),"Message sent",Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("ResourceAsColor")
    public void alertDialogLogout()
    {
        final Dialog openDialog = new Dialog(SettingsFragment.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.background));
        //openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.mark_dilaog);
        RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        TextView txt_cancel=(TextView)openDialog.findViewById(R.id.txt_cancel);
        TextView txt_remove=(TextView)openDialog.findViewById(R.id.txt_remove);
        TextView txt_alertmsg=(TextView)openDialog.findViewById(R.id.txt_alertmsg);

        txt_alertmsg.setText("Are you sure you want to logout ? ");
        txt_remove.setText("Logout");
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDialog.cancel();
            }
        });
        txt_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject js=new JSONObject();
                try {
                    js.put(Constants.user_id,sp.getString(Constants.id,""));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(Constants.isNetworkAvailable(getApplicationContext()))
                {
                    Constants.progressBarDialog(SettingsFragment.this);
                    LogOut();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                }

                LoginManager.getInstance().logOut();
                openDialog.cancel();
            }
        });
        openDialog.show();
    }

    public void PushNotificationApi(final JSONObject json1)
    {
        String URL= GlobalConstants.PushNotification;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    String message=s.getString("message");
                    if (status.equalsIgnoreCase("1"))
                    {
                        JSONObject json=s.getJSONObject("data");
                        String push_notification=json.getString("push_notification");
                        ed.putString(Constants.pushnotification,push_notification);
                        ed.commit();
                    }
                    else
                    {

                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(SettingsFragment.this,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }

                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(SettingsFragment.this);
        rQueue.add(request);
    }


    @SuppressLint("ResourceAsColor")
    public void alertDialogExit()
    {
        final Dialog openDialog = new Dialog(SettingsFragment.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.background));
        //openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.mark_dilaog);
        RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        TextView txt_cancel=(TextView)openDialog.findViewById(R.id.txt_cancel);
        TextView txt_remove=(TextView)openDialog.findViewById(R.id.txt_remove);
        TextView txt_alertmsg=(TextView)openDialog.findViewById(R.id.txt_alertmsg);
        txt_alertmsg.setText("Do you really want to exit ?");
        txt_remove.setText("Exit");
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                openDialog.cancel();
            }
        });
        txt_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                openDialog.cancel();
            }
        });
        openDialog.show();
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {}
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if(dir!= null && dir.isFile())
        {
            return dir.delete();
        } else
            {
            return false;
        }
    }

//    public void upgradedialog()
//    {
//        final Dialog openDialog = new Dialog(SettingsFragment.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
//        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//        openDialog.setContentView(R.layout.upgrade_dialog);
//        RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
//        Constants.overrideFonts(getApplicationContext(),mainlayout);
//        ImageView img_cross=(ImageView)openDialog.findViewById(R.id.img_cross);
//        Button btn_subscribeall=(Button)openDialog.findViewById(R.id.btn_subscribeall);
//        Button subscribe = (Button) openDialog.findViewById(R.id.btn_subscribe);
//        subscribe.setOnClickListener(new View.OnClickListener()
//        {
//            @Override
//            public void onClick(View v)
//            {
//
//                try {
//                    mHelper.launchPurchaseFlow(SettingsFragment.this, String.valueOf(finalId), 10001, mPurchaseFinishedListener,
//                            "mypurchasetoken");
//                } catch (IabHelper.IabAsyncInProgressException e) {
//                    e.printStackTrace();
//                }
//                JSONObject js=new JSONObject();
//                try
//                {
//                    js.put("user_id",sp.getString(Constants.user_id,""));
//                    js.put("level_id",sp.getString(Constants.level_id,""));
//                }
//                catch (JSONException e)
//                {
//                    e.printStackTrace();
//                }
//                Constants.progressBarDialog(SettingsFragment.this);
//                Upgradelevel(js);
//            }
//        });
//
//        btn_subscribeall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                try {
//                    mHelper.launchPurchaseFlow(SettingsFragment.this, String.valueOf(finalId), 10001, mPurchaseFinishedListener,
//                            "mypurchasetoken");
//                } catch (IabHelper.IabAsyncInProgressException e) {
//                    e.printStackTrace();
//                }
//                JSONObject js=new JSONObject();
//                try
//                {
//                    js.put("user_id",sp.getString(Constants.user_id,""));
//                    //  js.put("level_id",sp.getString(Constants.level_id,""));
//                }
//                catch (JSONException e)
//                {
//                    e.printStackTrace();
//                }
//                Constants.progressBarDialog(SettingsFragment.this);
//                UpgradeAll(js);
//            }
//        });
//
//        img_cross.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openDialog.dismiss();
//            }
//        });
//        openDialog.show();
//    }

//    ///In App Purchase
//    public void InAppInit()
//    {
//        String base64EncodedPublicKey = LICENSE_KEY;
//
//
//        mHelper = new IabHelper(this, base64EncodedPublicKey);
//
//        // enable debug logging (for a production application, you should set
//        // this to false).
//        mHelper.enableDebugLogging(true);
//
//        Log.e(TAG, "Starting setup.");
//        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
//            public void onIabSetupFinished(IabResult result) {
//                if (!result.isSuccess()) {
//                    Log.e(TAG, "In-app Billing setup failed: " + result);
//
//                } else {
//                    Log.e(TAG, "In-app Billing is set up OK");
//
//
//                    try {
//                        mHelper.queryInventoryAsync(mReceivedInventoryListener);
//                    } catch (IabHelper.IabAsyncInProgressException e) {
//                        e.printStackTrace();
//                    }
//
//
//                }
//            }
//        });
//    }
//
//    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
//        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
//
//
//            if (result.isFailure()) {
//                // Handle failure
//                Log.e("inventry failure", result.toString());
//
//            }else {
//
//
////            if (inventory.hasPurchase(finalId)) {
////                Toast.makeText(ForAdvertisementActivity.this, "querying owned items)", Toast.LENGTH_SHORT).show();
////            } else {
//
//                Purchase gasPurchase = inventory.getPurchase(String.valueOf(finalId));
//
//
//                if (gasPurchase != null && verifyDeveloperPayload(gasPurchase)) {
//                    Log.e(TAG, "We have product. Consuming it.");
//
//
//                    try {
//                        mHelper.consumeAsync(inventory.getPurchase(String.valueOf(finalId)), mConsumeFinishedListener);
//                    } catch (IabHelper.IabAsyncInProgressException e) {
//                        e.printStackTrace();
//                    }
//
//                    return;
//                }
////            }
//            }}
//    };
//
//
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//
//        if (mHelper == null)
//            return;
//
//        // Pass on the activity result to the helper for handling
//        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
//            // not handled, so handle it ourselves (here's where you'd
//            // perform any handling of activity results not related to in-app
//            // billing...
//            super.onActivityResult(requestCode, resultCode, data);
//        } else {
//            Log.d(TAG, "onActivityResult handled by IABUtil.");
//        }
//
//    }
//
//
//    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
//        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
//            Log.e(TAG, "Purchase finished: " + result + ", purchase: " + purchase);
//
//            if (result.isFailure()) {
//                Log.e("result message", String.valueOf(result.getResponse()));
////                userSession.saveUAdsStatus("1");
////                Toast.makeText(ForAdvertisementActivity.this, "Done", Toast.LENGTH_SHORT).show();
//                Toast.makeText(getApplicationContext(),"Purchased",Toast.LENGTH_SHORT).show();
//                ed.putString(Constants.level_name,"Moderate");
//                ed.commit();
//                return;
//            }
//
//
//            if (!verifyDeveloperPayload(purchase)) {
//                //  showToast("Error purchasing. Authenticity verification failed.");
//                return;
//            }
//            Toast.makeText(getApplicationContext(),"Purchased Successfully",Toast.LENGTH_SHORT).show();
//            ed.putString(Constants.level_name,"Moderate");
//            ed.commit();
//
//
//            //}
//
////            userSession.saveUAdsStatus("1");
////            Toast.makeText(ForAdvertisementActivity.this, "Successfully done", Toast.LENGTH_SHORT).show();
//            if (purchase.getSku().equals(finalId)) {
//                //mHelper.queryInventoryAsync(mReceivedInventoryListener);
//                // userSession.saveUAdsStatus("1");
//
//
//                try {
//                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
//                } catch (IabHelper.IabAsyncInProgressException e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//
//
//        }
//    };
//    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
//        public void onConsumeFinished(Purchase purchase, IabResult result) {
//
//            if (result.isSuccess()) {
//                ed.putString(Constants.level_name,"Moderate");
//                ed.commit();
////                userSession.saveUAdsStatus("1");
////                Toast.makeText(ForAdvertisementActivity.this, "Done", Toast.LENGTH_SHORT).show();
//
//
//
//            } else {
//                // handle error
//                //  userSession.saveUAdsStatus("0");
//
//            }
//        }
//    };
//
//    boolean verifyDeveloperPayload(Purchase p) {
//        String payload = p.getDeveloperPayload();
//
//        return true;
//    }
//
//    public void Upgradelevel(final JSONObject json1)
//    {
//        String URL= GlobalConstants.UpgradeLevel;
//        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject s)
//            {
//                Log.e("send response", String.valueOf(s));
//                try {
//                    String status = s.getString("status");
//                    String message=s.getString("message");
//                    if (status.equalsIgnoreCase("1"))
//                    {
//                        progressDialog.dismiss();
//                    }
//                    else
//                    {
//                        progressDialog.dismiss();
//                    }
//                }
//                catch (JSONException e)
//                {
//                    progressDialog.dismiss();
//                    e.printStackTrace();
//                }
//            }
//        },
//                new Response.ErrorListener() {
//
//                    @Override
//                    public void onErrorResponse(VolleyError volleyError) {
//                        progressDialog.dismiss();
//                        Toast.makeText(SettingsFragment.this,volleyError.toString(), Toast.LENGTH_SHORT).show();
//                        Log.e("Error: ",volleyError.toString()+json1 );
//                    }
//                }) {
//
//            @Override
//            public Map<String, String> getHeaders() {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json; charset=utf-8");
//                return headers;
//            }
//        };
//        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        RequestQueue rQueue = Volley.newRequestQueue(SettingsFragment.this);
//        rQueue.add(request);
//    }
//
//
//    public void UpgradeAll(final JSONObject json1)
//    {
//        String URL= GlobalConstants.UpgradeAll;
//        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {
//
//            @Override
//            public void onResponse(JSONObject s)
//            {
//                Log.e("send response", String.valueOf(s));
//                try
//                {
//                    String status = s.getString("status");
//                    String message=s.getString("message");
//                    if (status.equalsIgnoreCase("1"))
//                    {
//                        Constants.hideLoader();
//                    }
//                    else
//                    {
//                        Constants.hideLoader();
//                    }
//                }
//                catch (JSONException e)
//                {
//                    Constants.hideLoader();
//                    e.printStackTrace();
//                }
//            }
//        },
//                new Response.ErrorListener() {
//
//                    @Override
//                    public void onErrorResponse(VolleyError volleyError) {
//                        Constants.hideLoader();
//                        Toast.makeText(SettingsFragment.this,volleyError.toString(), Toast.LENGTH_SHORT).show();
//                        Log.e("Error: ",volleyError.toString()+json1 );
//                    }
//                }) {
//
//            @Override
//            public Map<String, String> getHeaders() {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json; charset=utf-8");
//                return headers;
//            }
//        };
//        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        RequestQueue rQueue = Volley.newRequestQueue(SettingsFragment.this);
//        rQueue.add(request);
//    }

    @SuppressLint("ResourceAsColor")
    public void alertDialogLevels()
    {
        openDialog1 = new Dialog(SettingsFragment.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog1.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.background));
        //openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog1.setContentView(R.layout.levellistview);
        RelativeLayout mainlayout=(RelativeLayout)openDialog1.findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        RecyclerView recyclerView;
        TextView txt_cancel;
        ImageView img_down;

        txt_cancel=(TextView)openDialog1.findViewById(R.id.txt_cancel);
        img_down=(ImageView)openDialog1.findViewById(R.id.img_down);
        recyclerView=(RecyclerView)openDialog1.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        dialogdapter = new DailogLevelAdapter(SettingsFragment.this, dialoglevel,this);
        recyclerView.setAdapter(dialogdapter);
        dialogdapter.notifyDataSetChanged();

        img_down.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog1.dismiss();
            }
        });

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog1.dismiss();
            }
        });

        openDialog1.show();
    }

    public ArrayList<DailogLevelModel> Download()
    {
        ArrayList<DailogLevelModel> callLog = new ArrayList<DailogLevelModel>();

        Gson gson = new Gson();
        String json = sp.getString("levels", "");
        if (json.isEmpty()) {
            callLog = new ArrayList<DailogLevelModel>();
        } else
            {
            Type type = new TypeToken<ArrayList<DailogLevelModel>>() {
            }.getType();
            callLog = gson.fromJson(json, type);
        }
        return callLog;
    }

    @Override
    public void onItemClick(String lock,String levelid) {
        Log.e("check: ", sp.getString(Constants.checked,""));
        if (lock.equalsIgnoreCase("1"))
        {
            if(sp.getString(Constants.checked,"0").equalsIgnoreCase("1"))
            {
                Log.e("11userid", userid+","+levelid );
                openDialog1.dismiss();
                InAppPurchaseMethods.InAppInit(SettingsFragment.this);
                InAppPurchaseMethods.upgradedialog(SettingsFragment.this, userid, levelid);
            }
            else
            {
                Log.e("22userid", userid+","+levelid );
                alertDialogUnlock(SettingsFragment.this,userid,levelid);
                openDialog1.dismiss();
            }
        }
        else
        {
            JSONObject js=new JSONObject();
            try
            {
                js.put("user_id",sp.getString(Constants.id,""));
                js.put("level_id",levelid);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Constants.progressBarDialog(SettingsFragment.this);
            SetLevel(js,levelid);
        }
    }

        @SuppressLint("ResourceAsColor")
        public void alertDialogUnlock(final Context context, final String userid, final String levelid)
        {

            final Dialog openDialog = new Dialog(context, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
            openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.background));
            //openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            openDialog.setContentView(R.layout.proceed_dialog);
            RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
            Constants.overrideFonts(SettingsFragment.this,mainlayout);
            ImageView img_cross=(ImageView)openDialog.findViewById(R.id.img_cross);
            TextView txt_cancel=(TextView)openDialog.findViewById(R.id.txt_cancel);
            TextView txt_proceed=(TextView)openDialog.findViewById(R.id.txt_proceedanyway);
            CheckBox checkbox=(CheckBox)openDialog.findViewById(R.id.checkbox);

            if(sp.getString(Constants.checked,"0").equalsIgnoreCase("1"))
            {
                checkbox.setChecked(true);
            }
            else
            {
                checkbox.setChecked(false);
            }
            img_cross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openDialog.cancel();
                }
            });
            checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
            {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                {
                    //Toast.makeText(context,String.valueOf(isChecked),Toast.LENGTH_SHORT).show();
                    if(isChecked==true)
                    {
                        ed.putString(Constants.checked,"1");
                        ed.commit();
                    }
                    else
                    {
                        ed.putString(Constants.checked,"0");
                        ed.commit();
                    }

                }
            });

            txt_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    openDialog.cancel();
                }
            });

            txt_proceed.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v)
                {
                    InAppPurchaseMethods.upgradedialog(SettingsFragment.this, userid, levelid);
                }
            });

            openDialog.show();
        }

    public void SetLevel(final JSONObject json1, final String levelid)
    {
        String URL= GlobalConstants.SetHomeLevel;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    String message=s.getString("message");

                    if (status.equalsIgnoreCase("1"))
                    {
                       Constants.hideLoader();
                        ed.putString(Constants.level_id,levelid);
                        ed.putString(Constants.level_name,message);
                        ed.commit();
                        openDialog1.dismiss();
                        dialogdapter.notifyDataSetChanged();
                    }
                    else
                    {
                        Constants.hideLoader();
                    }
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError)
                    {
                        Constants.hideLoader();
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(getApplicationContext());
        rQueue.add(request);
    }

}
