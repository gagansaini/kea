package com.kickoffelite.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kickoffelite.R;
import com.kickoffelite.adapter.SwipeTechAdapter;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.model.DrillThirdModel;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.model.TechVideoModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SwipeTechActivity extends AppCompatActivity implements View.OnClickListener,View.OnTouchListener
{
    RecyclerView recyclerView;
    SwipeTechAdapter swipeTechAdapter;

    ImageView img_icon,img_video;
    Dialog progressDialog;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    TextView txt_title,txt_head,txt_session,txt_desc;
    String techid;
    RelativeLayout mainlayout;
    ArrayList<HashMap<String, String>> drill_list_tags=new ArrayList<>();
    HashMap<String, String> drill_hash_tags = new HashMap<String, String>();
    ArrayList<HashMap<String, String>> technique_list_tags=new ArrayList<>();
    HashMap<String, String> technique_hash_tags = new HashMap<String, String>();
    String title,techtitle,imageurl;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_tech);
        init();
        setListeners();
    }

    public void init()
    {
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        Intent intent = getIntent();
        txt_desc=(TextView)findViewById(R.id.txt_desc);
        txt_head=(TextView)findViewById(R.id.txt_head);
        txt_title=(TextView)findViewById(R.id.txt_title);
        txt_session=(TextView)findViewById(R.id.txt_session);
        img_icon=(ImageView)findViewById(R.id.img_icon) ;
        img_video=(ImageView)findViewById(R.id.img_video) ;
        mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);

        techtitle=intent.getStringExtra(Constants.TechName);
        String sessions=intent.getStringExtra(Constants.TechSession);
        String desc=intent.getStringExtra(Constants.TechDesc);
        String image=intent.getStringExtra(Constants.TechImage);
        techid=intent.getStringExtra(Constants.TechId);

        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                imageurl=image;
            }
            else
            {
                imageurl= GlobalConstants.ImageUrl+"/"+image;
            }
            Picasso.with(SwipeTechActivity.this).load(imageurl).placeholder(R.color.grayback).into(img_video);
        }
        else
        {
            Picasso.with(SwipeTechActivity.this).load(R.color.grayback).placeholder(R.color.grayback).into(img_video);
        }
        txt_session.setText(sessions+" Sessions");
        txt_desc.setText(desc);
        txt_head.setText(techtitle);
        txt_title.setText(techtitle);
        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        if(intent.getStringExtra("name").equalsIgnoreCase("tech"))
        {
            techid=intent.getStringExtra("id");
            techtitle=intent.getStringExtra("title");
            String techdesc=intent.getStringExtra("desc");
            String techsession=intent.getStringExtra(Constants.TechSession);
            String techimage=intent.getStringExtra("image");
            txt_desc.setText(techdesc);
            txt_session.setText(techsession+" Sessions");
            if(!techimage.equalsIgnoreCase(""))
            {
                if(image.contains("http"))
                {
                    imageurl=image;
                }
                else
                {
                    imageurl= GlobalConstants.ImageUrl+"/"+image;
                }
                Picasso.with(SwipeTechActivity.this).load(imageurl).placeholder(R.color.grayback).into(img_video);
            }
            else
            {
                Picasso.with(SwipeTechActivity.this).load(R.color.grayback).placeholder(R.color.grayback).into(img_video);
            }

            txt_title.setText(techtitle);
            txt_head.setText(techtitle);
            progressBarDialog();
            get_Techniques_Videos();
        }

        else
        {
            if(Constants.isNetworkAvailable(getApplicationContext()))
            {
                progressBarDialog();
                get_Techniques_Videos();
            }
            else
            {
                Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void setListeners()
    {
        img_icon.setOnTouchListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e( "onResume: ","resume" );
       // get_Techniques_Videos();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e( "onRestart: ","onRestart" );
        if(Constants.isNetworkAvailable(getApplicationContext()))
        {
            get_Techniques_Videos();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    public void get_Techniques_Videos()
    {
        final List<DrillThirdModel> drill_sub_List = new ArrayList<>();
        final ArrayList<HashMap<String, String>> drill_list_tags=new ArrayList<>();
        final ArrayList<HashMap<String, String>> technique_list_tags=new ArrayList<>();
        final List<TechVideoModel> arrayList = new ArrayList<>();
        final String URL = GlobalConstants.TECHVIDEOLIST+sp.getString(Constants.id,"")+"&technique_id="+techid;
        StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.e("response order", s);
                try {
                    JSONObject jsob = new JSONObject(s);
                    String response = jsob.getString("status");
                    if (response.equals("1"))
                    {
                        progressDialog.dismiss();
                        Log.e("statusvideo", response);
                        JSONArray jsonarr = jsob.getJSONArray("data");
                        if(arrayList.size()==0) {
                            for(int i=0;i<jsonarr.length();i++)
                            {
                                JSONObject jsonobj=jsonarr.getJSONObject(i);
                                String id= jsonobj.getString("id");
                                String level_id= jsonobj.getString("level_id");
                                String technique_id= jsonobj.getString("technique_id");
                                String technique_name = jsonobj.getString("technique_name");
                                String video_id = jsonobj.getString("video_id");
                                String level_no= jsonobj.getString("level_no");
                                String video_name = jsonobj.getString("video_name");
                                String video_url = jsonobj.getString("video_url");
                                String video_thumbnail = jsonobj.getString("video_thumbnail");
                                String video_description = jsonobj.getString("video_description");
                                String video_time = jsonobj.getString("video_time");
                                String level_name = jsonobj.getString("level_name");
                                String is_locked = jsonobj.getString("is_locked");
                                String video_status = jsonobj.getString("video_status");
                                String is_favourite = jsonobj.getString("is_favourite");
                                JSONObject jsontag=jsonobj.getJSONObject("tags");
                                if(jsontag.has("techniques"))
                                {
                                    JSONArray jstech=jsontag.getJSONArray("techniques");
                                    for(int j=0;j<jstech.length();j++)
                                    {
                                        JSONObject jsontech=jstech.getJSONObject(j);
                                        String technique_id_tag = jsontech.getString("id");
                                        String technique_name_tag = jsontech.getString("name");
                                        String technique_description = jsontech.getString("description");
                                        String technique_image = jsontech.getString("image");
                                        String session_count = jsontech.getString("session_count");
                                        String is_lockedd = jsontech.getString("is_locked");
                                        String complete_session = jsontech.getString("complete_session");
                                        technique_hash_tags.put("tech_id", technique_id_tag);
                                        technique_hash_tags.put("tech_name", technique_name_tag);
                                        technique_hash_tags.put("tech_desc", technique_description);
                                        technique_hash_tags.put("tech_image", technique_image);
                                        technique_hash_tags.put("tech_session", session_count);
                                        technique_hash_tags.put("tech_locked", is_lockedd);
                                        technique_hash_tags.put("tech_complete", complete_session);
                                        technique_list_tags.add(technique_hash_tags);
                                        technique_hash_tags = new HashMap<String, String>();
                                    }
                                }
                                else
                                {
                                }
                                if(jsontag.has("drills"))
                                {
                                    JSONArray jstech=jsontag.getJSONArray("drills");
                                    for(int j=0;j<jstech.length();j++)
                                    {
                                        JSONObject jsontech=jstech.getJSONObject(j);
                                        String drill_id_tag = jsontech.getString("id");
                                        String drill_name_tag = jsontech.getString("name");
                                        String drill_description = jsontech.getString("description");
                                        String drill_image = jsontech.getString("image");
                                        String session_count = jsontech.getString("session_count");
                                        String is_lockedd = jsontech.getString("is_locked");
                                        String complete_session = jsontech.getString("complete_session");
                                        drill_hash_tags.put("drill_id", drill_id_tag);
                                        drill_hash_tags.put("drill_name", drill_name_tag);
                                        drill_hash_tags.put("drill_desc", drill_description);
                                        drill_hash_tags.put("drill_image", drill_image);
                                        drill_hash_tags.put("drill_session", session_count);
                                        technique_hash_tags.put("drill_locked", is_lockedd);
                                        technique_hash_tags.put("drill_complete", complete_session);
                                        drill_list_tags.add(drill_hash_tags);
                                        drill_hash_tags = new HashMap<String, String>();
                                    }
                                }
                                else
                                {
                                }
                                // String video_type = jsonobj.getString("video_type");
                                //String url="http://worksdelight.com/kickoff/Uploads/Users/"+sp.getString(Constants.id,"")+"/"+technique_image;
                                  Log.e("urlswipeimage",video_thumbnail);
                                arrayList.add(new TechVideoModel(id,level_id,technique_id,technique_name,video_id,level_no,video_name,video_url,video_thumbnail,video_time,
                                        level_name,is_locked,video_status ,video_description,is_favourite
                                        ,drill_list_tags,technique_list_tags));
                            }
                        }
                        swipeTechAdapter = new SwipeTechAdapter(SwipeTechActivity.this, arrayList);
                        swipeTechAdapter.notifyDataSetChanged();
                        recyclerView.setAdapter(swipeTechAdapter);

                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
                Toast.makeText(SwipeTechActivity.this, "Some error occurred -> " + volleyError, Toast.LENGTH_LONG).show();

            }
        }) ;
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(SwipeTechActivity.this);
        rQueue.add(request);
    }

    public void progressBarDialog()
    {
        progressDialog= new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId())
        {
            case R.id.img_icon:
                finish();
                break;

        }
        return false;
    }
}
