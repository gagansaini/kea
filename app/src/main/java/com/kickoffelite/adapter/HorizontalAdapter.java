package com.kickoffelite.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.kickoffelite.R;
import com.kickoffelite.activity.DrillThirdActivity;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.model.HomeDrillmodel;
import com.kickoffelite.helper.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {

    List<HomeDrillmodel> drillist ;
    Context context;
    SharedPreferences sp;
    SharedPreferences.Editor ed;

    public HorizontalAdapter(List<HomeDrillmodel> drillist, Context context)
    {
        this.drillist = drillist;
        this.context = context;
        sp=context.getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        SelectableRoundedImageView imageView;
        TextView txtview;
        LinearLayout mainlayout;

        public MyViewHolder(View view)
        {
            super(view);
            imageView=(SelectableRoundedImageView) view.findViewById(R.id.imageview);
            txtview=(TextView) view.findViewById(R.id.txtview);
            mainlayout=(LinearLayout)view.findViewById(R.id.mainlayout);
            Constants.overrideFonts(context,mainlayout);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final HomeDrillmodel homedrill = drillist.get(position);
        holder.txtview.setText(homedrill.getName());
        Log.e( "imggggg: ",homedrill.getImage() );
        String image=homedrill.getImage();
        String imageurl="";

        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                imageurl=image;
            }
            else
            {
                imageurl= GlobalConstants.ImageUrl+"/"+image;
            }
            Picasso.with(context).load(imageurl).into(holder.imageView);
        }
        else
        {
            Picasso.with(context).load(R.color.grayback).into(holder.imageView);
        }
       // Picasso.with(context).load(homedrill.getImage()).into(holder.imageView);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v)
            {
                Intent intent=new Intent(context, DrillThirdActivity.class);
                intent.putExtra(Constants.name,homedrill.getName());
                intent.putExtra(Constants.is_locked,homedrill.getIs_locked());
                intent.putExtra(Constants.video_id,homedrill.getId());
                intent.putExtra(Constants.image,homedrill.getImage()) ;
                intent.putExtra(Constants.desc,homedrill.getDescription()) ;
                intent.putExtra(Constants.total_session,homedrill.getSession_count()) ;
                intent.putExtra(Constants.completed_session,homedrill.getComplete_session()) ;
                intent.putExtra(Constants.level_name,homedrill.getLevelname()) ;

                intent.putExtra("name","");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }

        });
    }

    @Override
    public int getItemCount()
    {
        return drillist.size();
    }
}

