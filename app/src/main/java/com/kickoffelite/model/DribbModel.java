package com.kickoffelite.model;

/**
 * Created by HP on 5/22/2018.
 */

public class DribbModel
{
    String name;
    String time;

    public DribbModel(String name, String time, int thumbnail) {
        this.name = name;
        this.time = time;
        this.thumbnail = thumbnail;
    }

    int thumbnail;

    public DribbModel()
    {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(int thumbnail) {
        this.thumbnail = thumbnail;
    }
}
