package com.kickoffelite.helper;

import com.github.mikephil.charting.charts.BarChart;

public class GlobalConstants {
    public static final String BASEURL ="http://chinamaja.com/kickoff/api/" ;
    public static final String DRILLSLIST =BASEURL+"drills/Drillslist?user_id=" ;
    public static final String SESSIONLIST =BASEURL+"drills/Sessionslist?user_id=" ;
    public static final String SESSIONVIDEOLIST =BASEURL+"drills/Drillsvideos?user_id=" ;
    public static final String MARKVIDEO =BASEURL+"drills/markcompletevideo";
    public static final String LOGIN =BASEURL+"users/login";
    public static final String SIGNUP =BASEURL+"users/signup";
    public static final String SOCIALLOGIN =BASEURL+"users/social_login";
    public static final String EDITPROFILE =BASEURL+"users/editprofile";
    public static final String GETPROFILE =BASEURL+"users/profile?id=";
    public static final String CHANGEPSWD =BASEURL+"users/changePassword";
    public static final String FORGOTPSWD =BASEURL+"users/forgetPassword";
    public static final String TECHNIQUESLIST =BASEURL+"Techniques/Techniquelists?user_id=";
    public static final String TECHVIDEOLIST =BASEURL+"Techniques/Techniquesvideos?user_id=";
    public static final String GETHOMEDATA =BASEURL+"home";
    public static final String GETLEVELVIDEOS=BASEURL+"home/sublevel_videos";
    public static final String LOGOUT=BASEURL+"users/logout";
    public static final String GetGraph=BASEURL+"users/graphvideo?user_id=";
    public static final String UpgradeLevel=BASEURL+"users/subscription";
    public static final String UpgradeAll=BASEURL+"users/subscribeall";
    public static final String Search=BASEURL+"Home/searchVideos";
    public static final String PushNotification=BASEURL+"users/notifications";
    public static final String AddFavorite=BASEURL+"users/mark_favourite_videos";
    public static final String GetFavorite=BASEURL+"users/favourite_videos?user_id=";
    public static final String ImageUrl="http://chinamaja.com/kickoff";
    public static final String SetHomeLevel=BASEURL+"users/change_level_id_for_home";
}
