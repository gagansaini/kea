package com.kickoffelite.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kickoffelite.R;
import com.kickoffelite.adapter.LevelAdapter;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.model.LevelVideosModel;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class LevelActivity extends AppCompatActivity implements View.OnClickListener,View.OnTouchListener{

    RecyclerView recyclerView;
    LevelAdapter levelAdapter;
    ImageView img_icon,img_video;
    Dialog progressDialog;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    TextView txt_title,txt_head,txt_session,txt_desc,txt_levelname,txt_skill;
    String techid,sublevelid,levelname;
    RelativeLayout mainlayout;
    ArrayList<HashMap<String, String>> drill_list_tags=new ArrayList<>();
    HashMap<String, String> drill_hash_tags = new HashMap<String, String>();
    ArrayList<HashMap<String, String>> technique_list_tags=new ArrayList<>();
    HashMap<String, String> technique_hash_tags = new HashMap<String, String>();

    String level_name,title_name;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);
        init();
        setListeners();
    }

    public void init()
    {
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        Intent intent = getIntent();
        txt_levelname=(TextView)findViewById(R.id.txt_levelname);
        txt_desc=(TextView)findViewById(R.id.txt_desc);
        txt_head=(TextView)findViewById(R.id.txt_head);
        txt_title=(TextView)findViewById(R.id.txt_title);
        txt_session=(TextView)findViewById(R.id.txt_session);
        txt_skill=(TextView)findViewById(R.id.txt_skill);
        img_icon=(ImageView)findViewById(R.id.img_icon);
        img_video=(ImageView)findViewById(R.id.img_video);
        mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        img_video.setScaleType(ImageView.ScaleType.CENTER_CROP);
        title_name=intent.getStringExtra(Constants.name);
        String totalsession = intent.getStringExtra(Constants.total_session);
        txt_session.setText(totalsession+" Sessions");
        levelname=intent.getStringExtra(Constants.level_name);
        String image=intent.getStringExtra(Constants.image);
        String desc=intent.getStringExtra(Constants.desc);
        txt_desc.setText(desc);
        String url="";
        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                url=image;
            }
            else
            {
                url=GlobalConstants.ImageUrl+"/"+image;
            }
            Picasso.with(LevelActivity.this).load(url).placeholder(R.color.grayback).into(img_video);
        }

        else
        {
            Picasso.with(LevelActivity.this).load(R.color.grayback).placeholder(R.color.grayback).into(img_video);
        }
        txt_skill.setText(levelname);
        txt_levelname.setText(levelname);
        txt_head.setText(title_name);
        txt_title.setText(title_name);
        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        sublevelid=intent.getStringExtra(Constants.subcategory_id);

       JSONObject js=new JSONObject();
        try
        {
            js.put("user_id",sp.getString(Constants.id,""));
            js.put("sublevel_id",sublevelid);
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
        if(Constants.isNetworkAvailable(getApplicationContext()))
        {
            progressBarDialog();
            getLevelVideos(js);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
        }


    }


    public void setListeners()
    {
        img_icon.setOnTouchListener(this);
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {

        }
    }



    public void progressBarDialog()
    {
        progressDialog= new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.e( "onRestart: ","onRestart" );
        JSONObject js=new JSONObject();
        try {
            js.put("user_id",sp.getString(Constants.id,""));
            js.put("sublevel_id",sublevelid);
        } catch (JSONException e) {
            e.printStackTrace();
        }
       // progressBarDialog();
        if(Constants.isNetworkAvailable(getApplicationContext()))
        {
            getLevelVideos(js);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId())
        {
            case R.id.img_icon:
                finish();
                break;

        }
        return false;
    }

    public void getLevelVideos(final JSONObject json1)
    {
         final List<LevelVideosModel> levellist = new ArrayList<>();
        final ArrayList<HashMap<String, String>> drill_list_tags=new ArrayList<>();
        final ArrayList<HashMap<String, String>> technique_list_tags=new ArrayList<>();
        String URL= GlobalConstants.GETLEVELVIDEOS;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    if (status.equalsIgnoreCase("1"))
                    {
                        JSONArray jsonarray=s.getJSONArray("data");
                        for(int i=0;i<jsonarray.length();i++)
                        {
                            progressDialog.dismiss();
                           JSONObject jsonObject=jsonarray.getJSONObject(i);
                            String id=jsonObject.getString("id");
                            String video_id=jsonObject.getString("video_id");
                            String video_name=jsonObject.getString("video_name");
                            String description=jsonObject.getString("video_description");
                            String video_url=jsonObject.getString("video_url");
                            String video_thumbnail=jsonObject.getString("video_thumbnail");
                            String level_id=jsonObject.getString("level_id");
                            String level_no=jsonObject.getString("level_no");
                            String video_time=jsonObject.getString("video_time");
                            String is_locked=jsonObject.getString("is_locked");
                            String video_status=jsonObject.getString("video_status");
                            String level_name=jsonObject.getString("level_name");
                            String is_favourite = jsonObject.getString("is_favourite");
                            JSONObject jsontag=jsonObject.getJSONObject("tags");
                            if(jsontag.has("techniques"))
                            {
                                JSONArray jstech=jsontag.getJSONArray("techniques");
                                for(int j=0;j<jstech.length();j++)
                                {
                                    JSONObject jsontech=jstech.getJSONObject(j);
                                    String technique_id_tag = jsontech.getString("id");
                                    String technique_name_tag = jsontech.getString("name");
                                    String technique_description = jsontech.getString("description");
                                    String technique_image = jsontech.getString("image");
                                    String session_count = jsontech.getString("session_count");
                                    String is_lockedd = jsontech.getString("is_locked");
                                    String complete_session = jsontech.getString("complete_session");
                                    technique_hash_tags.put("tech_id", technique_id_tag);
                                    technique_hash_tags.put("tech_name", technique_name_tag);
                                    technique_hash_tags.put("tech_desc", technique_description);
                                    technique_hash_tags.put("tech_image", technique_image);
                                    technique_hash_tags.put("tech_session", session_count);
                                    technique_hash_tags.put("tech_locked", is_lockedd);
                                    technique_hash_tags.put("tech_complete", complete_session);
                                    technique_list_tags.add(technique_hash_tags);
                                    technique_hash_tags = new HashMap<String, String>();
                                }
                            }
                            else
                            {
                            }
                            if(jsontag.has("drills"))
                            {
                                JSONArray jstech=jsontag.getJSONArray("drills");
                                for(int j=0;j<jstech.length();j++)
                                {
                                    JSONObject jsontech=jstech.getJSONObject(j);
                                    String drill_id_tag = jsontech.getString("id");
                                    String drill_name_tag = jsontech.getString("name");
                                    String drill_description = jsontech.getString("description");
                                    String drill_image = jsontech.getString("image");
                                    String session_count = jsontech.getString("session_count");
                                    String is_lockedd = jsontech.getString("is_locked");
                                    String complete_session = jsontech.getString("complete_session");
                                    drill_hash_tags.put("drill_id", drill_id_tag);
                                    drill_hash_tags.put("drill_name", drill_name_tag);
                                    drill_hash_tags.put("drill_desc", drill_description);
                                    drill_hash_tags.put("drill_image", drill_image);
                                    drill_hash_tags.put("drill_session", session_count);
                                    technique_hash_tags.put("drill_locked", is_lockedd);
                                    technique_hash_tags.put("drill_complete", complete_session);
                                    drill_list_tags.add(drill_hash_tags);
                                    drill_hash_tags = new HashMap<String, String>();
                                }
                            }
                            else
                            {
                            }
                            levellist.add(new LevelVideosModel(id,video_id,video_name,description,video_url,video_thumbnail,level_id,level_no,video_time,is_locked,video_status,levelname,
                                    title_name,is_favourite,drill_list_tags,technique_list_tags));
                        }
                        levelAdapter = new LevelAdapter(LevelActivity.this, levellist);
                        levelAdapter.notifyDataSetChanged();
                        recyclerView.setAdapter(levelAdapter);
                    }
                    else
                    {
                        progressDialog.dismiss();

                    }
                }
                catch (JSONException e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(LevelActivity.this,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(LevelActivity.this);
        rQueue.add(request);
    }


}
