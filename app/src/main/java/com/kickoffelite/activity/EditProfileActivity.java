package com.kickoffelite.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.kickoffelite.R;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.volley.VolleyMultipartRequest;
import com.squareup.picasso.Picasso;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener,DatePickerDialog.OnDateSetListener
{
    ImageView img_icon;
    CircleImageView img_camera,img_profile;
    TextView txt_birth,txt_gender,txt_save,txt_email;
    BottomSheetDialog bsd,bsdgender;
    private int GALLERY = 1, CAMERA = 2;
    AlertDialog builder;
    Bitmap bitmap;
    String selectedImagePath = "";
    public static final int RequestPermissionCode = 7;
    Dialog progressDialog;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    String name,email,dob,gender,address,image,date;
    EditText txt_name,txt_address;
    RelativeLayout rel_birth,mainlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        init();
        setListener();
    }

    public void init()
    {
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        Intent intent=getIntent();
        name=intent.getStringExtra(Constants.name);
        email=intent.getStringExtra(Constants.email);
        dob=intent.getStringExtra(Constants.date_of_birth);
        gender=intent.getStringExtra(Constants.gender);
        address=intent.getStringExtra(Constants.address);
        image=intent.getStringExtra(Constants.image);
        img_icon=(ImageView)findViewById(R.id.img_icon);
        img_camera=(CircleImageView)findViewById(R.id.img_camera);
        img_profile=(CircleImageView)findViewById(R.id.img_profile);
        txt_name=(EditText)findViewById(R.id.txt_name);
        txt_email=(TextView)findViewById(R.id.txt_email);
        txt_birth=(TextView)findViewById(R.id.txt_birth);
        txt_gender=(TextView)findViewById(R.id.txt_gender);
        txt_address=(EditText)findViewById(R.id.txt_address);
        txt_save=(TextView)findViewById(R.id.txt_save);
        rel_birth=(RelativeLayout)findViewById(R.id.rel_birth);
        mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        txt_name.setSelection(txt_name.getText().length());
        txt_name.setText(name);
        txt_email.setText(email);
        txt_birth.setText(dob);
        txt_name.setSelection(txt_name.getText().length());
        String url="";
        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                url=image;
            }
            else
            {
                url=GlobalConstants.ImageUrl+"/Uploads/Users/"+sp.getString(Constants.id,"")+"/"+image;
            }
            Picasso.with(EditProfileActivity.this).load(url).placeholder(R.color.grayback).into(img_profile);
        }

        else
        {
           Picasso.with(EditProfileActivity.this).load(R.color.grayback).placeholder(R.color.grayback).into(img_profile);
        }

        if(gender.equalsIgnoreCase("Add gender"))
        {
            txt_gender.setText(gender);
            txt_gender.setTextColor(getResources().getColor(R.color.txtblue));
        }

        else
        {
            txt_gender.setText(gender);
            txt_gender.setTextColor(getResources().getColor(R.color.black));
        }

        if(address.equalsIgnoreCase("Add address"))
        {
            txt_address.setHint(address);
            txt_address.setTextColor(getResources().getColor(R.color.txtblue));
        }

        else
        {
            txt_address.setText(address);
            txt_address.setTextColor(getResources().getColor(R.color.black));
        }

            txt_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            txt_address.setTextColor(getResources().getColor(R.color.black));
            }

            @Override
            public void afterTextChanged(Editable s)
            {

            }
        });

    }

    public void setListener()
    {
        img_icon.setOnClickListener(this);
        img_camera.setOnClickListener(this);
        txt_save.setOnClickListener(this);
        txt_gender.setOnClickListener(this);
        txt_address.setOnClickListener(this);
        rel_birth.setOnClickListener(this);
        txt_name.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.img_icon:
                finish();
                break;

            case R.id.img_camera:
                if (CheckingPermissionIsEnabledOrNot())
                {
                    dailog();
                }

                else
                {
                    RequestMultiplePermission();
                }
                break;

            case R.id.txt_save:
                if(Constants.isNetworkAvailable(EditProfileActivity.this))
                {
                    progressBarDialog();
                    Editprofile();
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.txt_gender:
                   // dailoggender();
                break;

            case R.id.txt_address:
                txt_address.setText("");
                break;

            case R.id.rel_birth:
                birthDialog();
                break;

            case R.id.txt_name:

                break;
        }
    }

    public void dailog()
    {
        bsd = new BottomSheetDialog(EditProfileActivity.this);
        bsd.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bsd.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        bsd.setContentView(R.layout.camera_dialog);
        bsd.show();
        onclick();
    }


    public void onclick()
    {
        LinearLayout camera, gallery,cancel_layout;

        camera = (LinearLayout) bsd.findViewById(R.id.camera_layout);
        gallery = (LinearLayout) bsd.findViewById(R.id.gallery_layout);
        cancel_layout= (LinearLayout) bsd.findViewById(R.id.cancel_layout);

        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePhotoFromCamera();
            }
        });

        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                choosePhotoFromGallary();
            }
        });

        cancel_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bsd.dismiss();
            }
        });
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {

        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == this.RESULT_CANCELED) {
            return;
        }

        if (requestCode == GALLERY)
        {
            if (data != null) {
                final Uri contentURI = data.getData();
                try {

                    //bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    Bitmap  bit = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    bitmap = Bitmap.createScaledBitmap(bit, 120, 120, false);
                    // String path = saveImage(bitmap);
                    //  Toast.makeText(Edit_Profile_Activity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    builder = new AlertDialog.Builder(EditProfileActivity.this).setMessage("Do You Want To Upload this Photo ?")
                            .setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    img_profile.setImageBitmap(bitmap);
                                    //   getStringFromBitmap(bitmap);
                                    bsd.dismiss();
                                    try {
                                        selectedImagePath=getFilePath(EditProfileActivity.this,contentURI);
                                        // Toast.makeText(getApplicationContext(),selectedImagePath,Toast.LENGTH_SHORT).show();
                                    } catch (URISyntaxException e) {
                                        e.printStackTrace();
                                    }


                                }

                            }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    // TODO Auto-generated method stub

                                    builder.dismiss();
                                }
                            }).setIcon(R.mipmap.ic_launcher).show();


                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(EditProfileActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }
        }

        else if (requestCode == CAMERA) {
            bitmap = (Bitmap) data.getExtras().get("data");

            ///////////
            builder = new AlertDialog.Builder(EditProfileActivity.this).setMessage("Do You Want To Upload this Photo ?")
                    .setCancelable(false).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            img_profile.setImageBitmap(bitmap);
                            //////////
                            // getStringImage(thumbnail);
                            //NOw storing String to SharedPreferences
                            // selectedImagePath=saveImage(thumbnail);
                            //   img_profile.setImageBitmap(bitmap);
                            //   getStringFromBitmap(bitmap);
                            bsd.dismiss();
                        }

                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub

                            builder.dismiss();
                        }
                    }).setIcon(R.mipmap.ic_launcher).show();
            /////////
        }
    }

    public String getFilePath(Context context, Uri uri) throws URISyntaxException {
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        if (Build.VERSION.SDK_INT >= 19 && DocumentsContract.isDocumentUri(context.getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[]{
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }


    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    public boolean CheckingPermissionIsEnabledOrNot()
    {
        int FirstPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA);
        int ThirdPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_EXTERNAL_STORAGE);
        int ForthPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int FifthPermissionResult = ContextCompat.checkSelfPermission(getApplicationContext(), READ_PHONE_STATE);


        return FirstPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ThirdPermissionResult == PackageManager.PERMISSION_GRANTED &&
                ForthPermissionResult == PackageManager.PERMISSION_GRANTED &&
                FifthPermissionResult == PackageManager.PERMISSION_GRANTED ;
    }

    private void RequestMultiplePermission()

    {
        // Creating String Array with Permissions.
        ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]
                {
                        Manifest.permission.CAMERA,

                        READ_EXTERNAL_STORAGE,
                        WRITE_EXTERNAL_STORAGE,
                        READ_PHONE_STATE

                }, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        switch (requestCode)
        {
            case RequestPermissionCode:

                if (grantResults.length > 0)
                {
                    boolean CameraPermission = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean ReadExternalStorage = grantResults[1] == PackageManager.PERMISSION_GRANTED;
                    boolean WriteExternalStorage = grantResults[2] == PackageManager.PERMISSION_GRANTED;
                    boolean ReadPhoneState = grantResults[3] == PackageManager.PERMISSION_GRANTED;

                    if (CameraPermission  && ReadExternalStorage && WriteExternalStorage && ReadPhoneState)
                    {
                        dailog();
                    }

                    else
                    {
                        // Toast.makeText(RegisterActivity.this,"Permission Denied,You may not be able to use some of the features of this App",Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }


    ///Gender
    public void dailoggender() {
        bsdgender = new BottomSheetDialog(EditProfileActivity.this);
        bsdgender.requestWindowFeature(Window.FEATURE_NO_TITLE);
        bsdgender.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        bsdgender.setContentView(R.layout.select_gender_dialog);

        bsdgender.show();

        onclickgender();

    }

    public void onclickgender()
    {
        TextView txt_ok,txt_cancel;
        final RadioGroup rg ;
        RadioButton rd_male,rd_female;

        txt_ok = (TextView) bsdgender.findViewById(R.id.txt_ok);
        txt_cancel = (TextView) bsdgender.findViewById(R.id.txt_cancel);
        rg=(RadioGroup)bsdgender.findViewById(R.id.rg);
        rd_female=(RadioButton)bsdgender.findViewById(R.id.rd_female);
        rd_male=(RadioButton)bsdgender.findViewById(R.id.rd_male);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                RadioButton radioButton = (RadioButton) group.findViewById(checkedId);
                //Toast.makeText(getBaseContext(), radioButton.getText(), Toast.LENGTH_SHORT).show();
                gender= String.valueOf(radioButton.getText());

            }
        });
        txt_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                txt_gender.setText(gender);
                txt_gender.setTextColor(getResources().getColor(R.color.black));
                bsdgender.dismiss();
            }
        });
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                bsdgender.dismiss();
            }
        });
    }

    public void Editprofile() {
        String URL = GlobalConstants.EDITPROFILE;
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, URL,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        String resultResponse = new String(response.data);
                        try
                        {
                            Log.e("onResponse: ",resultResponse );
                            JSONObject result = new JSONObject(resultResponse);
                            String status = result.getString("status");
                            String message = result.getString("message");
                            if(status.equalsIgnoreCase("1"))
                            {
                                progressDialog.dismiss();
                                JSONObject jsonobj=result.getJSONObject("data");
                                String id=jsonobj.getString("id");
                                String level_id=jsonobj.getString("level_id");
                                String name=jsonobj.getString("name");
                                String email=jsonobj.getString("email");
                                String dob=jsonobj.getString("date_of_birth");
                                String gender=jsonobj.getString("gender");
                                String address=jsonobj.getString("address");
                                String level_name=jsonobj.getString("level_name");
                                String image=jsonobj.getString("image");
                                String push_notification=jsonobj.getString("push_notification");
                                txt_name.setText(name);
                                txt_email.setText(email);
                                txt_birth.setText(dob);

                                if(address.equalsIgnoreCase(""))
                                {
                                    txt_address.setText("Add address");
                                    txt_address.setTextColor(getResources().getColor(R.color.txtblue));
                                }
                                else
                                {
                                    txt_address.setText(address);
                                    txt_address.setTextColor(getResources().getColor(R.color.black));
                                }

                                ed.putString(Constants.level_id,level_id);
                                ed.putString(Constants.level_name,level_name);
                                ed.putString(Constants.id,id);
                                ed.putString(Constants.name,name);
                                ed.putString(Constants.email,email);
                                ed.putString(Constants.date_of_birth,dob);
                                ed.putString(Constants.image,image);
                                ed.putString(Constants.gender,gender);
                                ed.putString(Constants.address,address);
                                ed.putString(Constants.pushnotification,push_notification);
                                ed.commit();
                                Intent ii = new Intent(getApplicationContext(), ProfileActivity.class);
                                startActivity(ii);
                                finish();
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

                            }

                        }
                        catch (JSONException e)
                        {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("onErrorResponse: ", error.toString());
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(Constants.name, txt_name.getText().toString());
                params.put("date_of_birth",txt_birth.getText().toString());
                params.put("gender",gender);
                params.put("address",txt_address.getText().toString());
                params.put("user_id",sp.getString(Constants.id,""));
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                if(bitmap != null)
                {
                    long imagename = System.currentTimeMillis();
                    params.put(Constants.image, new DataPart(imagename + ".png",getFileDataFromDrawable(bitmap)) );
                }
                else
                {

                }

                return params;
            }
        };
        Volley.newRequestQueue(this).add(volleyMultipartRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


    public void progressBarDialog()
    {
        progressDialog= new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void birthDialog()
    {
        Calendar now = Calendar.getInstance();
        DatePickerDialog datepickerdialog = DatePickerDialog.newInstance(
                EditProfileActivity.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        datepickerdialog.setThemeDark(true); //set dark them for dialog?
        datepickerdialog.vibrate(true); //vibrate on choosing date?
        datepickerdialog.dismissOnPause(true); //dismiss dialog when onPause() called?
        datepickerdialog.showYearPickerFirst(false); //choose year first?
        datepickerdialog.setAccentColor(Color.parseColor("#FEDC18")); // custom accent color
        datepickerdialog.setTitle("Please select a date"); //dialog title
        datepickerdialog.show(getFragmentManager(), "Datepickerdialog"); //show dialog
    }


    @Override
    public void onDateSet(DatePickerDialog datePickerDialog, int year, int month, int dayOfMonth) {
        date =dayOfMonth + "-" + (++month) + "-" + year;
        txt_birth.setText(date);
        Log.e("onDateSet: ",date );
    }

}
