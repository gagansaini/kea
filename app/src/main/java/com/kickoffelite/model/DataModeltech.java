package com.kickoffelite.model;

/**
 * Created by HP on 5/21/2018.
 */

public class DataModeltech
{
    public String id,name,description,image,level_id,sublevel_id,session_count,is_locked,complete_session;


    public DataModeltech(String id, String name, String image,String description,  String level_id, String sublevel_id, String session_count, String is_locked, String complete_session) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.image = image;
        this.level_id = level_id;
        this.sublevel_id = sublevel_id;
        this.session_count = session_count;
        this.is_locked = is_locked;
        this.complete_session = complete_session;
    }

    public String getLevel_id() {
        return level_id;
    }

    public void setLevel_id(String level_id) {
        this.level_id = level_id;
    }

    public String getSublevel_id() {
        return sublevel_id;
    }

    public void setSublevel_id(String sublevel_id) {
        this.sublevel_id = sublevel_id;
    }

    public String getIs_locked() {
        return is_locked;
    }

    public void setIs_locked(String is_locked) {
        this.is_locked = is_locked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getSession_count() {
        return session_count;
    }

    public void setSession_count(String session_count) {
        this.session_count = session_count;
    }

    public String getComplete_session() {
        return complete_session;
    }

    public void setComplete_session(String complete_session) {
        this.complete_session = complete_session;
    }
}
