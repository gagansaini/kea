package com.kickoffelite.activity;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kickoffelite.R;
import com.kickoffelite.adapter.DownloadedAdapter;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.model.DownloadModel;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DownloadedActivity extends AppCompatActivity {

    private List<DownloadModel> downmodel = new ArrayList<>();
    private List<String> names = new ArrayList<>();
    private RecyclerView recyclerView;
    private DownloadedAdapter downAdapter;
    String temp,fileurl,name,time,level,desc;
    ImageView img_icon;
    RelativeLayout mainlayout;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    SharedPreferences spdown;
    SharedPreferences.Editor eddown;
    ArrayList<HashMap<String, String>> downloadlist=new ArrayList<>();
    HashMap<String, String> downhash = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloaded);
        init();
        setListener();
    }

        public void init()
        {
            sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
            ed=sp.edit();
            spdown=getSharedPreferences("video",MODE_PRIVATE);
            eddown=spdown.edit();
            downloadlist=Download();
            img_icon=(ImageView)findViewById(R.id.img_icon);
            recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
            mainlayout=(RelativeLayout)findViewById(R.id.mainlayout) ;
            Constants.overrideFonts(getApplicationContext(),mainlayout);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            FetchVideos();
        }

        public void setListener()
        {
            img_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }

    private ArrayList<File> FetchVideos() {

        ArrayList<File> filenames = new ArrayList<File>();
        ArrayList<File> images = new ArrayList<File>();

        String path = (getApplicationContext().getCacheDir() + "/kickoff/");
        File apkStorage = new File(
                getApplicationContext().getCacheDir() + "/kickoff/");
        //If File is not present create directory
        if (!apkStorage.exists()) {

            //Log.e(TAG, "Directory Created.");
        } else {
            File image_name = null;
            File directory = new File(path);
            File[] files = directory.listFiles();

            Log.e("files: ", files.length + "");
            downmodel.clear();
            for (int i = 0; i < files.length; i++) {

                String filename = files[i].getName();
//                Log.e( "namefull: ",filename );
//                String[] separated = filename.split("=");
//                filename=separated[1];
                File file_name = files[i];
//                String[] file_url=String.valueOf(files[i]).split("=");
//                fileurl=file_url[0];


                Log.e("fileurl: ", "" + fileurl);
                // you can store name to arraylist and use it later
                filenames.add(file_name);

                /////
                String pathimage = (getApplicationContext().getCacheDir() + "/kickoffimages/");
                File apkStorageimage = new File(
                        getApplicationContext().getCacheDir() + "/kickoffimages/");
                //If File is not present create directory
                if (!apkStorageimage.exists()) {

                    //Log.e(TAG, "Directory Created.");
                } else {
                    File _name = null;
                    File directoryimage = new File(pathimage);
                    File[] fileimage = directoryimage.listFiles();

                    Log.e("files: ", fileimage.length + "");

//                    String imagename = fileimage[i].getName();
                    File images_name = fileimage[i];

                    Log.e("sizevideo: ", "" + files[i].length());
                    // you can store name to arraylist and use it later
                    images.add(images_name);
                    //Toast.makeText(getApplicationContext(),images_name.toString(),Toast.LENGTH_SHORT).show();
                    // downmodel.add(new DownloadModel(filename,file_name.toString()));}
                    //////
                    String name="";
                    if(downloadlist.size()>0)
                    {
                       // name=downloadlist.get(i);
                        name=downloadlist.get(i).get("name");
                        time=downloadlist.get(i).get("time");
                        level=downloadlist.get(i).get("level");
                        desc=downloadlist.get(i).get("desc");
                    }
                    Log.e( "imagename: ", images_name.toString() );
                    downmodel.add(new DownloadModel(name, images_name.toString(), file_name.toString(),level,time,desc));
                }
            }

            downAdapter = new DownloadedAdapter(DownloadedActivity.this, downmodel);
            recyclerView.setAdapter(downAdapter);
            downAdapter.notifyDataSetChanged();

    }
        return filenames;
    }


    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream baos=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, baos);
        byte [] b=baos.toByteArray();
        temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }

    public ArrayList<String> EnterregionList() {
        ArrayList< String> callLog = new ArrayList< String>();

        Gson gson = new Gson();
        String json = spdown.getString("namelist", "");
        if (json.isEmpty()) {
            callLog = new ArrayList<String>();
        } else {
            Type type = new TypeToken<ArrayList<String>>() {
            }.getType();
            callLog = gson.fromJson(json, type);
        }
        return callLog;
    }

    public ArrayList<HashMap<String, String>> Download()
    {
        ArrayList<HashMap<String, String>> callLog = new ArrayList<HashMap<String, String>>();

        Gson gson = new Gson();
        String json = spdown.getString("Download", "");
        if (json.isEmpty()) {
            callLog = new ArrayList<HashMap<String, String>>();
        } else {
            Type type = new TypeToken<ArrayList<HashMap<String, String>>>() {
            }.getType();
            callLog = gson.fromJson(json, type);
        }
        return callLog;
    }

    }
