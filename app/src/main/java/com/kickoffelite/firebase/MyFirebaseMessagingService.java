package com.kickoffelite.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kickoffelite.R;
import com.kickoffelite.helper.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

//AIzaSyCxauxV_ztpEdQQXbIAL5LQGcucw9GXv7A
//Server key2===AAAAN3Z-jvY:APA91bEiYhPJGU5zftYJHuR55VU8i6YciyzfBa1kxf_hmaqUa-IqzFglZrCAKyrd6tFyLyUE5r_5LsNK8p_cO2LABrvVrfdGKy0WOMSdfQtAKyD_hP-4W5pX-HgRRkM0x1bSwntOXpor
//Server key===AAAAN3Z-jvY:APA91bFPvazf3LW4DEx0dSRzIpsoecP9vz0yTUSSWnKRL0oow4elgfM_32IFqhJ4VG5Sf_rqtubGditf_DweAw_Z1u3LZoPS1hZ5OflHXJusBMR55_E-riDp9TByo54nvInKmlslM45e
/**
 * Created by HP on 5/17/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService
{
    private Context mCtx;
    private static final String TAG = "MyFirebaseMsgService";
    public static final int ID_SMALL_NOTIFICATION = 235;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
       // Constants.UserSession(getApplicationContext());
       // String status=Constants.getUserName();
        //Context context = MyFirebaseMessagingService.this;
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences( "firebase", Context.MODE_PRIVATE);
        String status=sharedPref.getString(Constants.switchbutton,"true");
        Log.e("onMessageReceived: ",status);
        if(status.equalsIgnoreCase("true"))
        {
            if (remoteMessage.getData().size() > 0) {
                Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
                try {
                    JSONObject json = new JSONObject(remoteMessage.getData().toString());

                    sendPushNotification(json);

                } catch (Exception e) {
                    Log.e(TAG, "Exception: " + e.getMessage());
                }
            }
        }
        else
        {
            Log.e( "elsepartservice " , "else");
        }
    }

    private void sendPushNotification(JSONObject json) {
        //optionally we can display the json into log
        Log.e(TAG, "Notification JSON " + json.toString());
        try {
            //getting the json data
            JSONObject data = json.getJSONObject("data");

            //parsing json data
            String title = data.getString("title");
            String message = data.getString("message");
            String imageUrl = data.getString("image");

            Intent intent = new Intent(String.valueOf(MyFirebaseMessagingService.this));

            //if there is no image
            if(imageUrl.equals("null")){
                //displaying small notification
              showSmallNotification(title, message, intent);
            }else{
                //if there is an image
                //displaying a big notification
            }
        } catch (JSONException e) {
            Log.e(TAG, "Json Exception: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }


    public void showSmallNotification(String title, String message, Intent intent) {
        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        mCtx,
                        ID_SMALL_NOTIFICATION,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );


        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(mCtx);
        Notification notification;
        notification = mBuilder.setSmallIcon(R.mipmap.ic_launcher).setTicker(title).setWhen(0)
                .setAutoCancel(true)
                .setContentIntent(resultPendingIntent)
                .setContentTitle(title)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(mCtx.getResources(), R.mipmap.ic_launcher))
                .setContentText(message)
                .build();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        NotificationManager notificationManager = (NotificationManager) mCtx.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(ID_SMALL_NOTIFICATION, notification);
    }

    //The method will return Bitmap from an image URL
    private Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
