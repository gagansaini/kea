package com.kickoffelite.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kickoffelite.R;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.GlobalConstants;
import com.ms.square.android.expandabletextview.ExpandableTextView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import co.lujun.androidtagview.TagContainerLayout;
import co.lujun.androidtagview.TagView;


public class DetailsActivity extends AppCompatActivity implements View.OnClickListener
{
    TextView txt_name,txt_date,txt_desc,txt_less,txt_more,txt_level,txt_levelname,txt_download;
    ExpandableTextView expand_text_view;
    ImageView img_image,img_back,img_download;
    String name,time,levelname,desc,levelno,islocked,Url,image,imageurl;
    RelativeLayout mainlayout;
    TagContainerLayout tag_technique,tag_drill;

    ArrayList<HashMap<String, String>> drilltags=new ArrayList<>();
    ArrayList<HashMap<String, String>> techniquetags=new ArrayList<>();
    List<String > drilltaglist = new ArrayList<String>();
    List<String > techtaglist = new ArrayList<String>();

    SharedPreferences sp;
    SharedPreferences.Editor ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        init();
        setListener();
    }

    public void init()
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.appcolor));
        }
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        Intent intent = getIntent();

        txt_levelname= (TextView) findViewById(R.id.txt_levelname);
        tag_technique=(TagContainerLayout)findViewById(R.id.tag_technique);
        tag_drill=(TagContainerLayout)findViewById(R.id.tag_drill);
        txt_name = (TextView) findViewById(R.id.txt_name);
        txt_date = (TextView) findViewById(R.id.txt_date);
        txt_level= (TextView) findViewById(R.id.txt_level);
        img_image=(ImageView)findViewById(R.id.img_image);
        img_back=(ImageView)findViewById(R.id.img_back);
        img_download=(ImageView)findViewById(R.id.img_download);
        txt_desc = (TextView) findViewById(R.id.txt_desc);
        txt_download= (TextView) findViewById(R.id.txt_download);
//        txt_less = (TextView) findViewById(R.id.txt_less);
//        txt_more = (TextView) findViewById(R.id.txt_more);
        mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);

        name=intent.getStringExtra(Constants.name);
        time=intent.getStringExtra(Constants.totaltime);
        levelname=intent.getStringExtra(Constants.level_name);
        desc=intent.getStringExtra(Constants.desc);
        levelno=intent.getStringExtra(Constants.level_no);
        islocked=intent.getStringExtra(Constants.is_locked);
        Url=intent.getStringExtra(Constants.url);
        image=intent.getStringExtra(Constants.image);

        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                imageurl=image;
            }
            else
            {
                imageurl= GlobalConstants.ImageUrl+"/"+image;
            }
            Picasso.with(DetailsActivity.this).load(imageurl).placeholder(R.color.grayback).into(img_image);
        }
        else
        {
            Picasso.with(DetailsActivity.this).load(R.color.grayback).placeholder(R.color.grayback).into(img_image);
        }

        String splitPath[] = Url.split("/");
        String targetFileName = splitPath[splitPath.length - 1];

        //  Log.e("user_id", user_id+","+main_category_id+","+sub_category_id+","+category_id );
        File videofile = new File(getApplicationContext().getCacheDir() + "/kickoff/" + targetFileName);
        Log.e("init: ", videofile.toString());
        if (videofile.exists())
        {
            img_download.setBackgroundResource(0);
            Glide.with(getApplicationContext()).load(R.drawable.downloaded).into(img_download);
            txt_download.setTextColor(getResources().getColor(R.color.green));
        }

        drilltags=new ArrayList<>();
        techniquetags=new ArrayList<>();
        drilltaglist = new ArrayList<String>();
        techtaglist = new ArrayList<String>();

        drilltags = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("drilltagslist");
        techniquetags = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("techniquetagslist");

        if(drilltags!=null )
        {
            Log.e("init: ", String.valueOf(drilltags.size()));
            for(int i=0;i<drilltags.size();i++)
            {
                Log.e("drilltags: ",drilltags.toString() );
                drilltaglist.add(drilltags.get(i).get("drill_name"));
            }
        }

        if(techniquetags!=null ) {
            Log.e("init: ", String.valueOf(techniquetags.size()));
            for (int i = 0; i < techniquetags.size(); i++)
            {

                Log.e("techniquetags: ",techniquetags.toString() );
                techtaglist.add(techniquetags.get(i).get("tech_name"));
            }
        }

        tag_technique.setBackgroundColor(getResources().getColor(R.color.white));
        tag_technique.setBorderColor(getResources().getColor(R.color.white));
        tag_technique.setTagTextColor(getResources().getColor(R.color.white));
        //  tag_technique.setBackground(getResources().getColor(R.color.white));
        tag_technique.setTags(techtaglist);
        tag_drill.setBackgroundColor(getResources().getColor(R.color.white));
        tag_drill.setBorderColor(getResources().getColor(R.color.white));
        tag_drill.setTagTextColor(getResources().getColor(R.color.white));
        tag_drill.setTags(drilltaglist);

        txt_desc.setText(desc);
        txt_desc.setMaxLines(2);
        txt_date.setText(time+ " min");
        txt_level.setText(levelno);
        txt_name.setText(name);
        txt_levelname.setText(levelname);
    }

    public void setListener()
    {
//        txt_less.setOnClickListener(this);
//        txt_more.setOnClickListener(this);
        img_back.setOnClickListener(this);
        setListeners();
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
//            case R.id.txt_less:
//                txt_less.setVisibility(View.GONE);
//                txt_more.setVisibility(View.VISIBLE);
//                txt_desc.setMaxLines(2);
//                break;
//
//            case R.id.txt_more:
//                txt_more.setVisibility(View.GONE);
//                txt_less.setVisibility(View.VISIBLE);
//                txt_desc.setMaxLines(10);
//                break;

            case R.id.img_back:
                finish();
                break;
        }
    }

    public void setListeners()
    {
        tag_technique.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int i, String s)
            {
                String id=techniquetags.get(i).get("tech_id");
                String desc=techniquetags.get(i).get("tech_desc");
                String image=techniquetags.get(i).get("tech_image");
                String session=techniquetags.get(i).get("tech_session");
                String title=techniquetags.get(i).get("tech_name");
                Log.e( "techtags: ", id.toString());
                Intent intent=new Intent(getApplicationContext(),SwipeTechActivity.class);
                intent.putExtra("name","tech");
                intent.putExtra("id",id);
                intent.putExtra("desc",desc);
                intent.putExtra("image",image);
                intent.putExtra(Constants.TechSession,session);
                intent.putExtra("title",title);
                intent.putExtra(Constants.TechImage,image);
                startActivity(intent);
                //finish();

            }

            @Override
            public void onTagLongClick(int i, String s)
            {
                String id=techniquetags.get(i).get("tech_id");
                String desc=techniquetags.get(i).get("tech_desc");
                String image=techniquetags.get(i).get("tech_image");
                String session=techniquetags.get(i).get("tech_session");
                String title=techniquetags.get(i).get("tech_name");
                Log.e( "techtags: ", id.toString());
                Intent intent=new Intent(getApplicationContext(),SwipeTechActivity.class);
                intent.putExtra("name","tech");
                intent.putExtra("id",id);
                intent.putExtra("desc",desc);
                intent.putExtra("image",image);
                intent.putExtra(Constants.TechSession,session);
                intent.putExtra("title",title);
                intent.putExtra(Constants.TechImage,image);
                startActivity(intent);
            }

            @Override
            public void onTagCrossClick(int i)
            {

            }
        });

        tag_drill.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int i, String s) {
                String id= drilltags.get(i).get("drill_id");
                String desc=drilltags.get(i).get("drill_desc");
                String image=drilltags.get(i).get("drill_image");
                String session=drilltags.get(i).get("drill_session");
                String title=drilltags.get(i).get("drill_name");
                Log.e( "drilltags: ", id.toString());
                Intent intent=new Intent(getApplicationContext(),DrillThirdActivity.class);
                intent.putExtra("name","drill");
                intent.putExtra("id",id);
                intent.putExtra("desc",desc);
                intent.putExtra("image",image);
                intent.putExtra(Constants.total_session,session);
                intent.putExtra("title",title);
                startActivity(intent);
               //finish();
            }

            @Override
            public void onTagLongClick(int i, String s)
            {
                String id= drilltags.get(i).get("drill_id");
                String desc=drilltags.get(i).get("drill_desc");
                String image=drilltags.get(i).get("drill_image");
                String session=drilltags.get(i).get("drill_session");
                String title=drilltags.get(i).get("drill_name");
                Log.e( "drilltags: ", id.toString());
                Intent intent=new Intent(getApplicationContext(),DrillThirdActivity.class);
                intent.putExtra("name","drill");
                intent.putExtra("id",id);
                intent.putExtra("desc",desc);
                intent.putExtra("image",image);
                intent.putExtra(Constants.total_session,session);
                intent.putExtra("title",title);
                startActivity(intent);
            }

            @Override
            public void onTagCrossClick(int i) {

            }
        });

    }

}
