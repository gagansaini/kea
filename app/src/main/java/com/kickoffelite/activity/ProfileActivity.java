package com.kickoffelite.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.kickoffelite.GooglePay.util.IabHelper;
import com.kickoffelite.GooglePay.util.IabResult;
import com.kickoffelite.GooglePay.util.Inventory;
import com.kickoffelite.GooglePay.util.Purchase;
import com.kickoffelite.R;
import com.kickoffelite.adapter.DailogLevelAdapter;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.model.DailogLevelModel;
import com.kickoffelite.helper.GlobalConstants;
import com.nightonke.jellytogglebutton.JellyToggleButton;
import com.nightonke.jellytogglebutton.State;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileActivity extends AppCompatActivity implements DailogLevelAdapter.ItemClickInterface
{
    //JellyToggleButton jtb;
    BarChart barchart;
    HashMap<String,String> levels=new HashMap<>();
    ArrayList<String> xaxis = new ArrayList<>();
    ArrayList<String> yaxis = new ArrayList<>();
    ArrayList<HashMap<String,String>> levellist = new ArrayList<>();
    ArrayList<DailogLevelModel> dialoglevel = new ArrayList<>();
    ImageView icon,downspin;
    CircleImageView img_profile;
    Dialog progressDialog;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    TextView txt_name,txt_emailaddress,txt_birthdate,txt_addgender,txt_addaddress,txt_skill,txt_edit,txt_upgrade;
    String imagepath,name,level_name,email,dob,gender,address,image;
    RelativeLayout mainlayout,rel_skill;
    int percentagebeg,percentagemod,percentageadv,percentagepro,percentagelegend,percentagejuggling;

    DailogLevelAdapter dialogdapter;
    //In App Purchase
    private static final String TAG = "com.example.billing";
    static final String ITEM_SKU = "android.test.purchased";
    TextView ads;

    String finalId = "android.test.purchased";
    // String PRODUCT_ID[] ={"android.test.purchased", "android.test.purchased", "android.test.purchased", "android.test.purchased", "android.test.purchased"};
    boolean mIsPremium = false;

    private static final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnSS+Ap37Y3aSxxQz21i4vhBjbYIXNas9W1qGtoOK+TTiF3uuUZyuoqtOqvsyo96L9EQx7DYvT4zrPXHmL0y6FV/SVFQNzba+l6V87zLQT283zSdn3+l0Rd/0DJzrXmnP5L5J5dNWjDY4Yruv3I06h7kPWbii2amA/RX2QSIAwjE21wEc0wzlq0aAuCnqtlEbScXlo856bNj/DBzfhFtaJtnKHUm60XUMLWM7ECeqzGloAfVDflc3AtksLgZT38m7dteUCDPV7XICBVBDwZLQN3OsE2FYqRgirRCcZtQ4HY6Utkvlmu7JHauKZHUjzuxgbhnCm4Sj3BvykdOXvJdaAQIDAQAB"; // PUT
    IabHelper mHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.appcolor));
        }
        init();
        setListener();
    }

    public void init()
    {
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        Log.e("init: ",percentagebeg+","+percentagemod+","+percentageadv);
        InAppInit();
        name=sp.getString(Constants.name,"");
        level_name=sp.getString(Constants.level_name,"");
        email=sp.getString(Constants.email,"");
        dob=sp.getString(Constants.date_of_birth,"");
        gender=sp.getString(Constants.gender,"");
        address=sp.getString(Constants.address,"");
        image=sp.getString(Constants.image,"");
        mainlayout=(RelativeLayout)findViewById(R.id.mainlayout);
       // rel_skill=(RelativeLayout)findViewById(R.id.rel_skill);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        icon=(ImageView)findViewById(R.id.icon);
        img_profile=(CircleImageView)findViewById(R.id.img_profile);
       // downspin=(ImageView) findViewById(R.id.downspin);
        txt_name=(TextView)findViewById(R.id.txt_name);
        txt_emailaddress=(TextView)findViewById(R.id.txt_emailaddress);
        txt_birthdate=(TextView)findViewById(R.id.txt_birthdate);
        txt_addgender=(TextView)findViewById(R.id.txt_addgender);
        txt_addaddress=(TextView)findViewById(R.id.txt_addaddress);
        txt_skill=(TextView)findViewById(R.id.txt_skill);
        txt_edit=(TextView)findViewById(R.id.txt_edit);

       // txt_upgrade=(TextView)findViewById(R.id.txt_upgrade);

        barchart = findViewById(R.id.barchart);
        barchart.setDrawBarShadow(false);
        barchart.setDrawValueAboveBar(false);
        barchart.setDrawMarkerViews(false);
        barchart.getDescription().setEnabled(false);
        barchart.setPinchZoom(false);
        barchart.setTouchEnabled(false);
        barchart.getAxisLeft().setDrawGridLines(true);
        barchart.getAxisLeft().setGridColor(getResources().getColor(R.color.graydark));
        setTouchBack();
        Log.e(TAG, sp.getString(Constants.id,"") );
        progressBarDialog();
        getProfile();

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        String url="";
        Log.e("url",url);
        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                url=image;
            }
            else
            {
                url=GlobalConstants.ImageUrl+"/Uploads/Users/"+sp.getString(Constants.id,"")+"/"+image;
            }
            imagepath=url;
            Picasso.with(ProfileActivity.this).load(url).placeholder(R.color.grayback).into(img_profile);
        }
        else
        {
            imagepath="";
            Picasso.with(ProfileActivity.this).load(R.color.grayback).placeholder(R.color.grayback).into(img_profile);
        }

        txt_name.setText(name);
        if(sp.getString(Constants.level_name,"").equalsIgnoreCase(""))
        {
            txt_skill.setText("Beginner");
        }
        else
        {
            txt_skill.setText(sp.getString(Constants.level_name,""));
        }

        txt_emailaddress.setText(email);
        txt_birthdate.setText(dob);

        if(gender.equalsIgnoreCase("Add Gender")||gender.equalsIgnoreCase(""))
        {
            txt_addgender.setTextColor(getResources().getColor(R.color.txtblue));
            txt_addgender.setText("");
        }

        else
        {
            txt_addgender.setTextColor(getResources().getColor(R.color.black));
            txt_addgender.setText(gender);
        }

        if(address.equalsIgnoreCase("Add Address")||address.equalsIgnoreCase(""))
        {
            txt_addaddress.setTextColor(getResources().getColor(R.color.txtblue));
            txt_addaddress.setText("");
        }

        else
        {
            txt_addaddress.setTextColor(getResources().getColor(R.color.black));
            txt_addaddress.setText(address);
        }
    }

    public void setListener()
    {
        txt_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1=new Intent(ProfileActivity.this, EditProfileActivity.class);
                intent1.putExtra(Constants.name,txt_name.getText().toString());
                intent1.putExtra(Constants.date_of_birth,txt_birthdate.getText().toString());
                intent1.putExtra(Constants.date_of_birth,txt_birthdate.getText().toString());
                intent1.putExtra(Constants.email,txt_emailaddress.getText().toString());
                intent1.putExtra(Constants.gender,txt_addgender.getText().toString());
                intent1.putExtra(Constants.address,txt_addaddress.getText().toString());
                intent1.putExtra(Constants.image,imagepath);
                startActivity(intent1);
                finish();
            }
        });


    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", "1");
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    public void setTouchBack()
    {
        icon.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                finish();
                return false;
            }
        });
    }

    private ArrayList<String> getXAxisValues()
    {
        for(int i=0;i<levellist.size();i++)
        {
            String levelname=levellist.get(i).get("level_name");
            xaxis.add(levelname);
        }
        return xaxis;
    }

    private ArrayList<String> getYAxisValues()
    {
        yaxis=new ArrayList<>();
        for(int i=0;i<6;i++)
        {
            i=i*2;
            String levelname="Level "+i;
            yaxis.add(levelname);
        }
        return yaxis;
    }

    public void xEntries()
    {
        percentagebeg= Integer.parseInt(levellist.get(0).get("percentage_seen"));
        percentagemod= Integer.parseInt(levellist.get(1).get("percentage_seen"));
        percentageadv= Integer.parseInt(levellist.get(2).get("percentage_seen"));
        percentagepro= Integer.parseInt(levellist.get(3).get("percentage_seen"));
        percentagelegend= Integer.parseInt(levellist.get(4).get("percentage_seen"));
        //percentagejuggling= Integer.parseInt(levellist.get(5).get("percentage_seen"));

        List<BarEntry> entries = new ArrayList<>();
        entries.add(new BarEntry(0, percentagebeg));
        entries.add(new BarEntry(1f, percentagemod));
        entries.add(new BarEntry(2f, percentageadv));
        entries.add(new BarEntry(3f, percentagepro));
        entries.add(new BarEntry(4f, percentagelegend));
        //entries.add(new BarEntry(5f, percentagejuggling));

        BarDataSet set = new BarDataSet(entries, "BarDataSet");
        set.setDrawValues(false);

        //for label
        barchart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(getXAxisValues()));
        barchart.getAxisLeft();
        XAxis xAxis = barchart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setGranularityEnabled(true);
        xAxis.setDrawGridLines(false);
        barchart.getAxisRight().setDrawAxisLine(false);


        YAxis y = barchart.getAxisLeft();
        y.setLabelCount(5);
        y.setAxisMaxValue(100);
        y.setAxisMinValue(0);
       // barchart.getAxisLeft().setValueFormatter(new IndexAxisValueFormatter(entries2));

        //For YaxisRight
        YAxis yr = barchart.getAxisRight();
        yr.setDrawTopYLabelEntry(false);
        yr.setDrawGridLines(false);
        yr.setEnabled(false);

        BarData data = new BarData(set);
        data.setBarWidth(0.5f); // set custom bar width
        barchart.setData(data);
        barchart.setFitBars(true); // make the x-axis fit exactly all bars
        barchart.getLegend().setEnabled(false);//to remove bottom color cubes and text
        barchart.invalidate(); // refresh
    }

    //Api To Get Profile
    public void getProfile() {
        levellist = new ArrayList<>();
        final String URL = GlobalConstants.GETPROFILE + sp.getString(Constants.id,"");
        StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                try {
                    JSONObject jsob = new JSONObject(s);
                    String response = jsob.getString("status");
                    Log.e(TAG, s.toString() );
                    //Toast.makeText(Edit_Profile_Activity.this, "response "+response, Toast.LENGTH_LONG).show();
                    if (response.equals("1"))
                    {
                        progressDialog.dismiss();
                        JSONObject json = jsob.getJSONObject("data");
                        String id = json.getString("id");
                        String name = json.getString("name");
                        // String phone = json.getString("phone_no");
                        String email = json.getString("email");
                        String dob = json.getString("date_of_birth");
                        String gender = json.getString("gender");
                        String address = json.getString("address");
                        String image=json.getString("image");
                        String level=json.getString("level_name");
                        String level_id=json.getString("level_id");
                        String sublevel_id=json.getString("sublevel_id");
                        String user_level_for_api=json.getString("user_level_for_api");
                        JSONArray jsarr=json.getJSONArray("available_level");

                        for(int i=0;i<jsarr.length();i++)
                        {
                            JSONObject jsobj=jsarr.getJSONObject(i);
                            String idd=jsobj.getString("id");
                            String levelname=jsobj.getString("level_name");
                            String description=jsobj.getString("description");
                            String is_locked=jsobj.getString("is_locked");
                            String total_videos=jsobj.getString("total_videos");
                            String seen_videos=jsobj.getString("seen_videos");
                            String percentage_seen=jsobj.getString("percentage_seen");
                            levels.put("id",idd);
                            levels.put("level_name",levelname);
                            levels.put("description",description);
                            levels.put("is_locked",is_locked);
                            levels.put("total_videos",total_videos);
                            levels.put("seen_videos",seen_videos);
                            levels.put("percentage_seen",percentage_seen);
                            levellist.add(levels);
                            dialoglevel.add(new DailogLevelModel(idd, levelname,description,is_locked,total_videos,seen_videos,percentage_seen));
                            levels=new HashMap<>();
                        }
                        xEntries();

                        ed.putString(Constants.name,name);
                        ed.putString(Constants.email,email);
                        ed.putString(Constants.gender,gender);
                        ed.putString(Constants.date_of_birth,dob);
                        ed.putString(Constants.image,image);
                        ed.putString(Constants.level_name,level);
                        ed.putString(Constants.userapilevel,user_level_for_api);
                        ed.putString(Constants.level_id,level_id);
                        ed.commit();

                        //TextDrawable drawable = TextDrawable.builder().buildRound(name.substring(0,1).toUpperCase(),Color.parseColor("#D3351E"));

                        String url=GlobalConstants.ImageUrl+"/Uploads/Users/"+sp.getString(Constants.id,"")+"/"+image;
                        Log.e("url",url);
                        if(!image.equalsIgnoreCase(""))
                        {
                            imagepath=url;
                            Picasso.with(ProfileActivity.this).load(url).placeholder(R.color.grayback).into(img_profile);
                        }
                        else
                        {
                            imagepath="";
                            Picasso.with(ProfileActivity.this).load(R.color.grayback).placeholder(R.color.grayback).into(img_profile);
                        }
                        txt_skill.setText(level);
                        txt_name.setText(name);
                        txt_birthdate.setText(dob);
                        txt_emailaddress.setText(email);
                        if(!gender.equalsIgnoreCase(""))
                        {
                            txt_addgender.setTextColor(getResources().getColor(R.color.black));
                            txt_addgender.setText(gender);
                        }
                        else
                        {

                        }
                        if(!address.equalsIgnoreCase(""))
                        {
                            txt_addaddress.setTextColor(getResources().getColor(R.color.black));
                            txt_addaddress.setText(address);
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
                Toast.makeText(ProfileActivity.this, "Some error occurred -> " + volleyError, Toast.LENGTH_LONG).show();

            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(ProfileActivity.this);
        rQueue.add(request);
        //Toast.makeText(getApplicationContext(),"Message sent",Toast.LENGTH_SHORT).show();
    }

    public void progressBarDialog()
    {
        progressDialog= new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    ///In App Purchase
    public void InAppInit()
    {
        String base64EncodedPublicKey = LICENSE_KEY;


        mHelper = new IabHelper(this, base64EncodedPublicKey);

        // enable debug logging (for a production application, you should set
        // this to false).
        mHelper.enableDebugLogging(true);

        Log.e(TAG, "Starting setup.");
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.e(TAG, "In-app Billing setup failed: " + result);

                } else {
                    Log.e(TAG, "In-app Billing is set up OK");


                    try {
                        mHelper.queryInventoryAsync(mReceivedInventoryListener);
                    } catch (IabHelper.IabAsyncInProgressException e) {
                        e.printStackTrace();
                    }


                }
            }
        });
    }


    IabHelper.QueryInventoryFinishedListener mReceivedInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {


            if (result.isFailure()) {
                // Handle failure
                Log.e("inventry failure", result.toString());

            }else {


//            if (inventory.hasPurchase(finalId)) {
//                Toast.makeText(ForAdvertisementActivity.this, "querying owned items)", Toast.LENGTH_SHORT).show();
//            } else {

                Purchase gasPurchase = inventory.getPurchase(String.valueOf(finalId));

                if (gasPurchase != null && verifyDeveloperPayload(gasPurchase)) {
                    Log.e(TAG, "We have product. Consuming it.");

                    try {
                        mHelper.consumeAsync(inventory.getPurchase(String.valueOf(finalId)), mConsumeFinishedListener);
                    } catch (IabHelper.IabAsyncInProgressException e) {
                        e.printStackTrace();
                    }

                    return;
                }
//            }
            }}
    };


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (mHelper == null)
            return;
        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
        }
    }


    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.e(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            if (result.isFailure()) {
                Log.e("result message", String.valueOf(result.getResponse()));
//                userSession.saveUAdsStatus("1");
//                Toast.makeText(ForAdvertisementActivity.this, "Done", Toast.LENGTH_SHORT).show();
                Toast.makeText(getApplicationContext(),"Purchased",Toast.LENGTH_SHORT).show();
                ed.putString(Constants.level_name,"Moderate");
                ed.commit();
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                //  showToast("Error purchasing. Authenticity verification failed.");
                return;
            }
            Toast.makeText(getApplicationContext(),"Purchased Successfully",Toast.LENGTH_SHORT).show();
            ed.putString(Constants.level_name,"Moderate");
            ed.commit();
            //}

//            userSession.saveUAdsStatus("1");
//            Toast.makeText(ForAdvertisementActivity.this, "Successfully done", Toast.LENGTH_SHORT).show();
            if (purchase.getSku().equals(finalId)) {
                //mHelper.queryInventoryAsync(mReceivedInventoryListener);
                // userSession.saveUAdsStatus("1");


                try {
                    mHelper.consumeAsync(purchase, mConsumeFinishedListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
            }
        }
    };
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {

            if (result.isSuccess())
            {
                    ed.putString(Constants.level_name,"Moderate");
                    ed.commit();
//                userSession.saveUAdsStatus("1");
//                Toast.makeText(ForAdvertisementActivity.this, "Done", Toast.LENGTH_SHORT).show();

            }
            else
            {
                // handle error
                //  userSession.saveUAdsStatus("0");
            }
        }
    };

    boolean verifyDeveloperPayload(Purchase p)
    {
        String payload = p.getDeveloperPayload();
        return true;
    }

    //Api To Get Graph Data
    public void getGraphData()
    {
        final String URL = GlobalConstants.GetGraph+ sp.getString(Constants.id,"");
        StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {

                try {
                    JSONObject jsob = new JSONObject(s);
                    String response = jsob.getString("status");
                    Log.e("onResponse: ",s );
                    if (response.equals("1"))
                    {
                        progressDialog.dismiss();
                        JSONObject json = jsob.getJSONObject("data");
                        JSONObject jsbeg=json.getJSONObject("Beginner");
                        String totalbeg=jsbeg.getString("total_videos");
                        String seenbeg=jsbeg.getString("seen_videos");
                        percentagebeg= Integer.parseInt(jsbeg.getString("percentage_seen"));
                        JSONObject jsmod=json.getJSONObject("Moderate");
                        String totalmod=jsmod.getString("total_videos");
                        String seenmod=jsmod.getString("seen_videos");
                        percentagemod= Integer.parseInt(jsmod.getString("percentage_seen"));
                        JSONObject jsadv=json.getJSONObject("Advanced");
                        String totaladv=jsadv.getString("total_videos");
                        String seenadv=jsadv.getString("seen_videos");
                        percentageadv= Integer.parseInt(jsadv.getString("percentage_seen"));
                        JSONObject jspro=json.getJSONObject("Pro");
                        String totalpro=jspro.getString("total_videos");
                        String seenpro=jspro.getString("seen_videos");
                        percentagepro= Integer.parseInt(jspro.getString("percentage_seen"));
                        JSONObject jselite=json.getJSONObject("Advanced");
                        String totalelite=jselite.getString("total_videos");
                        String seenelite=jselite.getString("seen_videos");
                        percentagelegend= Integer.parseInt(jselite.getString("percentage_seen"));
                        Log.e("initgraph: ",percentagebeg+","+percentagemod+","+percentageadv);

                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                progressDialog.dismiss();
                Toast.makeText(ProfileActivity.this, "Some error occurred -> " + volleyError, Toast.LENGTH_LONG).show();

            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(ProfileActivity.this);
        rQueue.add(request);
        //Toast.makeText(getApplicationContext(),"Message sent",Toast.LENGTH_SHORT).show();
    }

    public void Upgradelevel(final JSONObject json1)
    {
        String URL= GlobalConstants.UpgradeLevel;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    String message=s.getString("message");
                    if (status.equalsIgnoreCase("1"))
                    {
                        progressDialog.dismiss();
                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                }
                catch (JSONException e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(ProfileActivity.this,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(ProfileActivity.this);
        rQueue.add(request);
    }


    public void UpgradeAll(final JSONObject json1)
    {
        String URL= GlobalConstants.UpgradeAll;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL,json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s)
            {
                Log.e("send response", String.valueOf(s));
                try
                {
                    String status = s.getString("status");
                    String message=s.getString("message");
                    if (status.equalsIgnoreCase("1"))
                    {
                        progressDialog.dismiss();
                    }
                    else
                    {
                        progressDialog.dismiss();
                    }
                }
                catch (JSONException e)
                {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(ProfileActivity.this,volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ",volleyError.toString()+json1 );
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(ProfileActivity.this);
        rQueue.add(request);
    }

    @SuppressLint("ResourceAsColor")
    public void alertDialogLevels()
    {
        final Dialog openDialog = new Dialog(ProfileActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.background));
        //openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.levellistview);
        RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        RecyclerView recyclerView;
        TextView txt_cancel;

        txt_cancel=(TextView)openDialog.findViewById(R.id.txt_cancel);
        recyclerView=(RecyclerView)openDialog.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        dialogdapter = new DailogLevelAdapter(ProfileActivity.this, dialoglevel,this);
        recyclerView.setAdapter(dialogdapter);
        dialogdapter.notifyDataSetChanged();

        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog.dismiss();
            }
        });
        openDialog.show();
    }

    public void upgradedialog()
    {
        final Dialog openDialog = new Dialog(ProfileActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.upgrade_dialog);
        RelativeLayout mainlayout=(RelativeLayout)openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(),mainlayout);
        ImageView img_cross=(ImageView)openDialog.findViewById(R.id.img_cross);
        Button btn_subscribeall=(Button)openDialog.findViewById(R.id.btn_subscribeall);
        Button subscribe = (Button) openDialog.findViewById(R.id.btn_subscribe);
        subscribe.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                try {
                    mHelper.launchPurchaseFlow(ProfileActivity.this, String.valueOf(finalId), 10001, mPurchaseFinishedListener,
                            "mypurchasetoken");
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
                JSONObject js=new JSONObject();
                try
                {
                    js.put("user_id",sp.getString(Constants.user_id,""));
                    js.put("level_id",sp.getString(Constants.level_id,""));
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                progressBarDialog();
                Upgradelevel(js);
            }
        });

        btn_subscribeall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    mHelper.launchPurchaseFlow(ProfileActivity.this, String.valueOf(finalId), 10001, mPurchaseFinishedListener,
                            "mypurchasetoken");
                } catch (IabHelper.IabAsyncInProgressException e) {
                    e.printStackTrace();
                }
                JSONObject js=new JSONObject();
                try
                {
                    js.put("user_id",sp.getString(Constants.user_id,""));
                    //  js.put("level_id",sp.getString(Constants.level_id,""));
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
                progressBarDialog();
                UpgradeAll(js);
            }
        });

        img_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog.dismiss();
            }
        });
        openDialog.show();
    }


    @Override
    public void onItemClick(String lock, String levelname) {
        if(lock.equalsIgnoreCase("1"))
        {
            upgradedialog();
        }
    }
}

