package com.kickoffelite.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.kickoffelite.R;
import com.kickoffelite.helper.Constants;

public class SplashActivity extends Activity
{
    SharedPreferences sp;
    SharedPreferences.Editor ed;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        init();
    }

    public void init()
    {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(sp.getString(Constants.loginflag,"").equalsIgnoreCase("true"))
                {
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(i);
                }
                else
                {
                    Intent i = new Intent(SplashActivity.this, RegisterActivity.class);
                    startActivity(i);
                }
                finish();

            }
        }, 3000);
    }



}
