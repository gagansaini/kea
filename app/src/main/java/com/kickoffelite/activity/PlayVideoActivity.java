package com.kickoffelite.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kickoffelite.R;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.GlobalConstants;
import com.squareup.picasso.Picasso;
import com.yalantis.contextmenu.lib.ContextMenuDialogFragment;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hb.xvideoplayer.MxVideoPlayer;
import hb.xvideoplayer.MxVideoPlayerWidget;

public class PlayVideoActivity extends AppCompatActivity implements View.OnClickListener{
    String Url, name, image = "", main_category_id, sub_category_id, category_id, user_id, category, videoid, videostatus, video_type, totaltime, levelname, desc,pgetitle,levelno,videour,imageurl;
    ImageView img_download, img_thumb, img_tick, img_back, img_chat, img_i;
    Dialog progressDialog;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    SharedPreferences spdown;
    SharedPreferences.Editor eddown;
    RelativeLayout mainlayout;
    MxVideoPlayerWidget videoPlayerWidget;
    ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    ArrayList<String> videoname = new ArrayList<>();
    ArrayList<HashMap<String, String>> downloadlist = new ArrayList<>();
    HashMap<String, String> downhash = new HashMap<String, String>();
    AlertDialog.Builder builder;
    ArrayList<HashMap<String, String>> drilltags = new ArrayList<>();
    ArrayList<HashMap<String, String>> techniquetags = new ArrayList<>();
    List<String> drilltaglist = new ArrayList<String>();
    List<String> techtaglist = new ArrayList<String>();
    NotificationCompat.Builder mBuilder;
    NotificationManager mNotifyManager;
    int id = 1;

    ContextMenuDialogFragment mMenuDialogFragment;
    FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.black));
        }
        setContentView(R.layout.activity_play_video);
        init();
        setListener();
    }

    public void init() {
        sp = getSharedPreferences(Constants.appname, MODE_PRIVATE);
        ed = sp.edit();

        spdown = getSharedPreferences("video", MODE_PRIVATE);
        eddown = spdown.edit();
        downloadlist = Download();
        videoPlayerWidget = (MxVideoPlayerWidget) findViewById(R.id.videoplayer);
        mainlayout = (RelativeLayout) findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(), mainlayout);
        user_id = sp.getString(Constants.id, "");
        img_i = (ImageView) findViewById(R.id.img_i);
        img_chat = (ImageView) findViewById(R.id.img_chat);
        img_download = (ImageView) findViewById(R.id.img_download);
        img_thumb = (ImageView) findViewById(R.id.img_thumb);
        img_tick = (ImageView) findViewById(R.id.img_tick);
        img_back = (ImageView) findViewById(R.id.img_back);
        Intent intent = getIntent();

        if (intent.getStringExtra(Constants.VideoType).equalsIgnoreCase("drill")) {
            videostatus = intent.getStringExtra(Constants.video_status);
            desc = intent.getStringExtra(Constants.desc);
            drilltags = new ArrayList<>();
            techniquetags = new ArrayList<>();
            drilltaglist = new ArrayList<String>();
            techtaglist = new ArrayList<String>();

            drilltags = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("drilltagslist");
            techniquetags = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("techniquetagslist");
            setDrill_id_tags(drilltags);
            setTechnique_id_tags(techniquetags);

            if (videostatus.equalsIgnoreCase("0")) {
                img_tick.setBackground(getResources().getDrawable(R.drawable.tickvideo));
            } else {
                //img_tick.setBackground(getResources().getDrawable());
                img_tick.setBackground(getResources().getDrawable(R.drawable.greentick));
                img_tick.setColorFilter(getResources().getColor(R.color.green));
            }
            pgetitle = intent.getStringExtra(Constants.pgetitle);
            name = intent.getStringExtra(Constants.name);
            Url = intent.getStringExtra(Constants.url);
            image = intent.getStringExtra(Constants.image);
            Log.e("urlplaydrillimage", image);
            if(!image.equalsIgnoreCase(""))
            {
                if(image.contains("http"))
                {

                }
                else
                {
                    imageurl=GlobalConstants.ImageUrl+"/"+image;
                }
            }

            if(!Url.equalsIgnoreCase(""))
            {
                if(Url.contains("http"))
                {
                    videour=Url;
                }
                else
                {
                    videour= GlobalConstants.ImageUrl+"/"+Url;
                }

            }

            totaltime = intent.getStringExtra(Constants.totaltime);
            levelname = intent.getStringExtra(Constants.level_name);
            levelno= intent.getStringExtra(Constants.level_no);
            // main_category_id=intent.getStringExtra(Constants.id);
            // sub_category_id=intent.getStringExtra(Constants.subid);
            videoid = intent.getStringExtra(Constants.video_id);
            video_type = intent.getStringExtra(Constants.VideoType);
            String splitPath[] = videour.split("/");
            String targetFileName = splitPath[splitPath.length - 1];

            //  Log.e("user_id", user_id+","+main_category_id+","+sub_category_id+","+category_id );
            File videofile = new File(getApplicationContext().getCacheDir() + "/kickoff/" + targetFileName);
            Log.e("init: ", videofile.toString());
            if (videofile.exists()) {
                img_download.setBackgroundResource(0);
                Glide.with(getApplicationContext()).load(R.drawable.downledd).into(img_download);
                videoPlayerWidget.startPlay("", Uri.fromFile(videofile), MxVideoPlayer.SCREEN_LAYOUT_NORMAL, imageurl, name);
                Picasso.with(getApplicationContext()).load(imageurl).into(videoPlayerWidget.mThumbImageView);
            }

            else
                {
                img_download.setBackgroundResource(0);
                Picasso.with(getApplicationContext()).load(R.drawable.download).into(img_download);
                videoPlayerWidget.startPlay(videour, null, MxVideoPlayer.SCREEN_LAYOUT_NORMAL, imageurl, name);
                Picasso.with(getApplicationContext()).load(new File(imageurl)).into(videoPlayerWidget.mThumbImageView);
            }
        }

        else if (intent.getStringExtra(Constants.VideoType).equalsIgnoreCase("download")) {

            name = intent.getStringExtra(Constants.name);
            Url = intent.getStringExtra(Constants.url);
            image = intent.getStringExtra(Constants.image);
            totaltime = intent.getStringExtra(Constants.totaltime);
            levelname = intent.getStringExtra(Constants.level_name);
            levelno= intent.getStringExtra(Constants.level_no);
            img_download.setVisibility(View.INVISIBLE);
            img_chat.setVisibility(View.INVISIBLE);
            img_i.setVisibility(View.INVISIBLE);
            img_tick.setVisibility(View.INVISIBLE);
            Log.e("downloadedurl: ", Url);
            Log.e("downimage: ", image);
            if(!image.equalsIgnoreCase(""))
            {
                if(image.contains("http"))
                {

                }
                else
                {
                    imageurl=GlobalConstants.ImageUrl+"/"+image;
                }
            }
            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(image, MediaStore.Video.Thumbnails.MICRO_KIND);
            Glide
                    .with(getApplicationContext())
                    .load(new File(image))
                    .into(videoPlayerWidget.mThumbImageView);
            videoPlayerWidget.startPlay("", Uri.fromFile(new File(Url)), MxVideoPlayer.SCREEN_LAYOUT_NORMAL, imageurl, name);

        }


        else {
            desc = intent.getStringExtra(Constants.desc);
            drilltags = new ArrayList<>();
            techniquetags = new ArrayList<>();
            drilltaglist = new ArrayList<String>();
            techtaglist = new ArrayList<String>();

            drilltags = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("drilltagslist");
            techniquetags = (ArrayList<HashMap<String, String>>) getIntent().getSerializableExtra("techniquetagslist");

            pgetitle = intent.getStringExtra(Constants.pgetitle);
            videostatus = intent.getStringExtra(Constants.video_status);
            name = intent.getStringExtra(Constants.name);
            Url = intent.getStringExtra(Constants.url);
            if(!Url.equalsIgnoreCase(""))
            {
                if(Url.contains("http"))
                {
                    videour=Url;
                }
                else
                {
                    videour= GlobalConstants.ImageUrl+"/"+Url;
                }

            }
            image = intent.getStringExtra(Constants.image);
            if(!image.equalsIgnoreCase(""))
            {
                if(image.contains("http"))
                {

                }
                else
                {
                    imageurl=GlobalConstants.ImageUrl+"/"+image;
                }
            }

            Log.e("urlplayanotherimage", imageurl);
            videoid = intent.getStringExtra(Constants.video_id);
            totaltime = intent.getStringExtra(Constants.totaltime);
            levelname = intent.getStringExtra(Constants.level_name);
            levelno= intent.getStringExtra(Constants.level_no);
            if (videostatus.equalsIgnoreCase("0")) {
                img_tick.setBackground(getResources().getDrawable(R.drawable.tickvideo));
            } else {
                img_tick.setBackground(getResources().getDrawable(R.drawable.greentick));
                //  img_tick.setColorFilter(getResources().getColor(R.color.green));
            }
            // sub_category_id=intent.getStringExtra(Constants.subcategory_id);
            video_type = intent.getStringExtra(Constants.VideoType);
            String splitPath[] = videour.split("/");
            String targetFileName = splitPath[splitPath.length - 1];
            // videoPlayerWidget = (MxVideoPlayerWidget) findViewById(R.id.videoplayer);
            File videofile = new File(getApplicationContext().getCacheDir() + "/kickoff/" + targetFileName);
            if (videofile.exists()) {
                img_download.setBackgroundResource(0);
                Glide.with(getApplicationContext()).load(R.drawable.downledd).into(img_download);
                videoPlayerWidget.startPlay("", Uri.fromFile(videofile), MxVideoPlayer.SCREEN_LAYOUT_NORMAL, imageurl, name);
                Picasso.with(getApplicationContext()).load(imageurl).into(videoPlayerWidget.mThumbImageView);
            } else {
                videoPlayerWidget.startPlay(videour, null, MxVideoPlayer.SCREEN_LAYOUT_NORMAL, imageurl, name);
                Picasso.with(getApplicationContext()).load(new File(imageurl)).into(videoPlayerWidget.mThumbImageView);
            }
        }

//        String splitPath[] = videour.split("/");
//        String targetFileName = splitPath[splitPath.length - 1];
//
//
//        File videofile = new File(getApplicationContext().getCacheDir() + "/kickoff/" + targetFileName);
//        Log.e("loadProfileImage: ", videofile.toString());
//        if (videofile.exists()) {
//
//        }
    }

    public void setListener() {
        img_download.setOnClickListener(this);
        img_tick.setOnClickListener(this);
        img_back.setOnClickListener(this);
        img_chat.setOnClickListener(this);
        img_i.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        MxVideoPlayer.releaseAllVideos();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_download:
                if (Constants.isNetworkAvailable(getApplicationContext())) {
                    loadProfileImage(videour, PlayVideoActivity.this);
                    loadImage(imageurl, PlayVideoActivity.this);
                } else {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.img_tick:
                if (videostatus.equalsIgnoreCase("0")) {
                    alertDialogMark();
                } else {
                  Toast.makeText(getApplicationContext(),"Already markerd",Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.img_back:
                finish();
                break;

            case R.id.img_chat:
                Intent intent = new Intent(getApplicationContext(), ChatActivity.class);
                intent.putExtra(Constants.pgetitle,pgetitle);
                intent.putExtra(Constants.name,name);
                intent.putExtra(Constants.image,image);
                startActivity(intent);
                break;

            case R.id.img_i:
                Intent intent1 = new Intent(getApplicationContext(), DetailsActivity.class);
                intent1.putExtra(Constants.totaltime, totaltime);
                intent1.putExtra(Constants.level_name, levelname);
                intent1.putExtra("drilltagslist", getDrill_id_tags());
                intent1.putExtra("techniquetagslist", getTechnique_id_tags());
                intent1.putExtra(Constants.desc, desc);
                intent1.putExtra(Constants.name, name);
                intent1.putExtra(Constants.url, videour);
                intent1.putExtra(Constants.level_no, levelno);
                intent1.putExtra(Constants.image, image);
                startActivity(intent1);
                break;
        }
    }

    public ArrayList<HashMap<String, String>> getDrill_id_tags() {
        return drilltags;
    }

    public ArrayList<HashMap<String, String>> getTechnique_id_tags() {
        return techniquetags;
    }

    public void setDrill_id_tags(ArrayList<HashMap<String, String>> drill_id_tags) {
        this.drilltags = drill_id_tags;
    }

    public void setTechnique_id_tags(ArrayList<HashMap<String, String>> technique_id_tags) {
        this.techniquetags = technique_id_tags;
    }


    class DownloadFileAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        @Override
        protected String doInBackground(String... aurl) {
            try {
                URL url = new URL(aurl[0]);//Create Download URl

                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection
                int lenghtOfFile = c.getContentLength();
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection

                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e("Server returned HTTP ", +c.getResponseCode()
                            + " " + c.getResponseMessage());
                }

                //Get File if SD card is present

                String splitPath[] = aurl[0].split("/");
                String targetFileName = splitPath[splitPath.length - 1];
                File apkStorage = new File(
                        getApplicationContext().getCacheDir() + "/kickoff/");

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    //Log.e(TAG, "Directory Created.");
                }

                File outputFile = new File(apkStorage, targetFileName);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    // Log.e(TAG, "File Created");
                }


                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1;//init length
                long total = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    total += len1;

                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    fos.write(buffer, 0, len1);//Write new file
                    //  fosimage.write(buffer,0,len1);
                }

                //Close all connection after doing task
                fos.close();

                is.close();
                outputFile = null;
            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();
                //Log.e(TAG, "Download Error Exception " + e.getMessage());
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(String... progress) {

            Log.e("ANDRO_ASYNC", progress[0]);
            pDialog.setProgress(Integer.parseInt(progress[0]));

        }

        @Override
        protected void onPostExecute(String unused) {
            pDialog.dismiss();
            Log.e("doInBackground: ", "download");
        }
    }

    public void loadProfileImage(String file_url, Context c) {

        String splitPath[] = file_url.split("/");
        String targetFileName = splitPath[splitPath.length - 1];


        File videofile = new File(getApplicationContext().getCacheDir() + "/kickoff/" + targetFileName);
        Log.e("loadProfileImage: ", videofile.toString());
        if (videofile.exists()) {
            Toast.makeText(getApplicationContext(), "Video already downloaded", Toast.LENGTH_SHORT).show();
            Log.e("exist: ", videofile.toString());
        } else {
            Log.e("else: ", videofile.toString());
            //progressBarDialog();

// instantiate it within the onCreate method

            /////
            pDialog = new ProgressDialog(this);
            pDialog.setMessage("Downloading file. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.setCancelable(false);
            pDialog.show();
            new DownloadFileAsync().execute(file_url);
        }
    }

    //
    public void progressBarDialog() {
        progressDialog = new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    //Api
    public void markComplete(final JSONObject json1) {
        String URL = GlobalConstants.MARKVIDEO;
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, json1, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject s) {
                Log.e("send response", String.valueOf(s));
                try {
                    String status = s.getString("status");
                    String message = s.getString("message");
                    if (status.equalsIgnoreCase("1")) {
                        progressDialog.dismiss();
//                        ed.putString(Constants.markstatus,"true");
//                        ed.commit();
//                        if (message.equalsIgnoreCase("Video unmarked successfully")) {
//                            videostatus = "0";
//                            img_tick.setBackgroundResource(0);
//                            img_tick.setImageDrawable(getResources().getDrawable(R.drawable.tickvideo));
//                            //   img_tick.setColorFilter(getResources().getColor(R.color.white));
//                        }
//                        else {
                            videostatus = "1";
                            img_tick.setBackgroundResource(0);
                            img_tick.setImageDrawable(getResources().getDrawable(R.drawable.greentick));
                            //  img_tick.setColorFilter(getResources().getColor(R.color.green));
                       // }
                        Toast.makeText(PlayVideoActivity.this, message, Toast.LENGTH_SHORT).show();
                    } else {
                        progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    progressDialog.dismiss();
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        progressDialog.dismiss();
                        Toast.makeText(PlayVideoActivity.this, volleyError.toString(), Toast.LENGTH_SHORT).show();
                        Log.e("Error: ", volleyError.toString() + json1);
                    }

                }) {

            @Override
            public Map<String, String> getHeaders() {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(PlayVideoActivity.this);
        rQueue.add(request);
    }


    ////
    class DownloadImageAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showDialog(progress_bar_type);
        }

        @Override
        protected String doInBackground(String... aurl) {
            try {
                URL url = new URL(aurl[0]);//Create Download URl

                HttpURLConnection c = (HttpURLConnection) url.openConnection();//Open Url Connection

                int lenghtOfFile = c.getContentLength();
                c.setRequestMethod("GET");//Set Request Method to "GET" since we are grtting data
                c.connect();//connect the URL Connection

                //If Connection response is not OK then show Logs
                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Log.e("Server returned HTTP ", +c.getResponseCode()
                            + " " + c.getResponseMessage());
                }

                //Get File if SD card is present

                String splitPath[] = aurl[0].split("/");
                String targetFileName = splitPath[splitPath.length - 1];
                File apkStorage = new File(
                        getApplicationContext().getCacheDir() + "/kickoffimages/");

                //If File is not present create directory
                if (!apkStorage.exists()) {
                    apkStorage.mkdir();
                    //Log.e(TAG, "Directory Created.");
                }

                File outputFile = new File(apkStorage, targetFileName);//Create Output file in Main File

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                    // Log.e(TAG, "File Created");
                }


                FileOutputStream fos = new FileOutputStream(outputFile);//Get OutputStream for NewFile Location

                InputStream is = c.getInputStream();//Get InputStream for connection

                byte[] buffer = new byte[1024];//Set buffer type
                int len1;//init length
                long total = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    total += len1;

                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                    fos.write(buffer, 0, len1);//Write new file
                    //  fosimage.write(buffer,0,len1);
                }

                //Close all connection after doing task
                fos.close();

                is.close();
                outputFile = null;
            } catch (Exception e) {

                //Read exception if something went wrong
                e.printStackTrace();

                //Log.e(TAG, "Download Error Exception " + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... progress) {

            Log.e("ANDRO_ASYNC", progress[0]);
            // pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        @Override
        protected void onPostExecute(String unused) {
            Toast.makeText(getApplicationContext(), "File Downloaded", Toast.LENGTH_SHORT).show();
            img_download.setBackgroundResource(0);
            Picasso.with(getApplicationContext()).load(R.drawable.downledd).into(img_download);
            videoname.add(name);
            downhash.put("name", name);
            downhash.put("time", totaltime);
            downhash.put("level", levelname);
            downhash.put("desc", desc);
            downloadlist.add(downhash);
            Gson gson = new Gson();
            String json = gson.toJson(downloadlist);
            eddown.putString("Download", json);
            eddown.commit();
//            dismissDialog(progress_bar_type);
            // pDialog.dismiss();
            //Toast.makeText(getApplicationContext(),"File Downloaded",Toast.LENGTH_SHORT).show();
//            Log.e( "image: ", unused);

//            dialog2.dismiss();
////
//            Intent i = new Intent(getActivity(), ViewActivity.class);
//            i.putExtra("file_path", filePath);
//            startActivity(i);
        }
    }

    public void loadImage(String image, Context c) {

        String splitPath[] = image.split("/");
        String targetFileName = splitPath[splitPath.length - 1];


        File videofile = new File(getApplicationContext().getCacheDir() + "/kickoffimages/" + targetFileName);
        Log.e("loadProfileImage: ", videofile.toString());
//        if (videofile.exists())
//        {
//            Log.e("exist: ",videofile.toString() );
//        }
//        else {
//            Log.e("else: ",videofile.toString() );
        //progressBarDialog();

// instantiate it within the onCreate method
        new DownloadImageAsync().execute(image);
        //  }
    }



    public ArrayList<HashMap<String, String>> Download() {
        ArrayList<HashMap<String, String>> callLog = new ArrayList<HashMap<String, String>>();

        Gson gson = new Gson();
        String json = spdown.getString("Download", "");
        if (json.isEmpty()) {
            callLog = new ArrayList<HashMap<String, String>>();
        } else {
            Type type = new TypeToken<ArrayList<HashMap<String, String>>>() {
            }.getType();
            callLog = gson.fromJson(json, type);
        }
        return callLog;
    }

    @SuppressLint("ResourceAsColor")
    public void alertDialogUnMark() {
        final Dialog openDialog = new Dialog(PlayVideoActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.background));
        // openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.mark_dilaog);
        RelativeLayout mainlayout = (RelativeLayout) openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(), mainlayout);
        TextView txt_cancel = (TextView) openDialog.findViewById(R.id.txt_cancel);
        TextView txt_remove = (TextView) openDialog.findViewById(R.id.txt_remove);
        TextView txt_alertmsg = (TextView) openDialog.findViewById(R.id.txt_alertmsg);
        txt_alertmsg.setText(getResources().getString(R.string.alertmsgremove));
        txt_remove.setText("Remove");
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog.cancel();
            }
        });
        txt_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject js = new JSONObject();
                try {
                    js.put(Constants.user_id, user_id);
                    js.put(Constants.video_id, videoid);
                    js.put(Constants.complete_status, "0");
                    // js.put(Constants.video_status,video_type);
                    // js.put(Constants.video_type_id,sub_category_id);
                    Log.e("onClick: ", js.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (Constants.isNetworkAvailable(PlayVideoActivity.this)) {
                    progressBarDialog();
                    markComplete(js);
                } else {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                openDialog.cancel();
            }
        });
        openDialog.show();
    }

    @SuppressLint("ResourceAsColor")
    public void alertDialogMark() {
        final Dialog openDialog = new Dialog(PlayVideoActivity.this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(R.color.background));
        //openDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        openDialog.setContentView(R.layout.mark_dilaog);
        RelativeLayout mainlayout = (RelativeLayout) openDialog.findViewById(R.id.mainlayout);
        Constants.overrideFonts(getApplicationContext(), mainlayout);
        ImageView img_alert = (ImageView) openDialog.findViewById(R.id.img_alert);
        Picasso.with(getApplicationContext()).load(R.drawable.complte).into(img_alert);
        TextView txt_cancel = (TextView) openDialog.findViewById(R.id.txt_cancel);
        TextView txt_remove = (TextView) openDialog.findViewById(R.id.txt_remove);
        TextView txt_alertmsg = (TextView) openDialog.findViewById(R.id.txt_alertmsg);
        txt_alertmsg.setText(getResources().getString(R.string.alertmsgcomplete));
        txt_remove.setText("Complete");
        txt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog.cancel();
            }
        });
        txt_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject js = new JSONObject();
                try {
                    js.put(Constants.user_id, user_id);
                    js.put(Constants.video_id, videoid);
                    js.put(Constants.complete_status, "1");
                    // js.put(Constants.video_status,video_type);
                    // js.put(Constants.video_type_id,sub_category_id);
                    Log.e("onClick: ", js.toString());
                    if (video_type.equalsIgnoreCase("drill")) {
                        // js.put("drill_id",main_category_id);
                    } else {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (Constants.isNetworkAvailable(PlayVideoActivity.this)) {
                    progressBarDialog();
                    markComplete(js);
                } else {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                }
                openDialog.cancel();
            }
        });
        openDialog.show();
    }





}
//https://github.com/lipangit/JiaoZiVideoPlayer
//http://www.blueappsoftware.in/android/blog/exoplayer-android-example//
// /http://www.gadgetsaint.com/android/download-manager/#.WyeKpRIzagQ
