package com.kickoffelite.model;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.HashMap;

public class DownloadModel
{
    String name,videourl,image,levelname,time,desc;
    Bitmap bm;
    ArrayList<HashMap<String, String>> drill_id_tags=new ArrayList<>();
    ArrayList<HashMap<String, String>> technique_id_tags=new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getBm() {
        return bm;
    }

    public void setBm(Bitmap bm) {
        this.bm = bm;
    }

    public String getLevelname() {
        return levelname;
    }

    public void setLevelname(String levelname) {
        this.levelname = levelname;
    }

    public DownloadModel(String name, String image, String videourl, String levelname,String time,String desc) {
        this.name = name;
        this.videourl = videourl;
        this.image=image;
        this.time=time;
        this.levelname=levelname;
        this.desc = desc;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getVideourl() {
        return videourl;

    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setVideourl(String videourl) {
        this.videourl = videourl;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
