package com.kickoffelite.application;


import android.app.Application;

import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;


public class ApplicationChat extends Application
{

    @Override
    public void onCreate()
    {
        super.onCreate();

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();
        JSONObject tags = new JSONObject();
        try {
            tags.put("key1", "value1");
            tags.put("key2", "value2");
            OneSignal.sendTags(tags);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
