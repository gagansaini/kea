package com.kickoffelite.model;

public class HomeLevelModel
{
    String id,sublevel_id,name,image,description,is_locked,session_count,complete_session,levelname;

    public HomeLevelModel(String id, String sublevel_id, String name, String image, String description, String is_locked, String session_count, String complete_session,String levelname) {
        this.id = id;
        this.sublevel_id = sublevel_id;
        this.name = name;
        this.image = image;
        this.description = description;
        this.is_locked = is_locked;
        this.session_count = session_count;
        this.complete_session = complete_session;
        this.levelname=levelname;
    }

    public String getLevelname() {
        return levelname;
    }

    public void setLevelname(String levelname) {
        this.levelname = levelname;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSublevel_id() {
        return sublevel_id;
    }

    public void setSublevel_id(String sublevel_id) {
        this.sublevel_id = sublevel_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIs_locked() {
        return is_locked;
    }

    public void setIs_locked(String is_locked) {
        this.is_locked = is_locked;
    }

    public String getSession_count() {
        return session_count;
    }

    public void setSession_count(String session_count) {
        this.session_count = session_count;
    }

    public String getComplete_session() {
        return complete_session;
    }

    public void setComplete_session(String complete_session) {
        this.complete_session = complete_session;
    }
}
