package com.kickoffelite.model;

import java.util.ArrayList;
import java.util.HashMap;

public class SearchModel
{
    String name,sublevel,id,desc,image,count,searchtype,url,video_status;
    ArrayList<HashMap<String, String>> drill_id_tags=new ArrayList<>();
    ArrayList<HashMap<String, String>> technique_id_tags=new ArrayList<>();

    public SearchModel(String id,String name, String sublevel,String desc,String image,String count,String searchtype,String url,String video_status,ArrayList<HashMap<String, String>> drill_id_tags,ArrayList<HashMap<String, String>> technique_id_tags) {
        this.id=id;
        this.name = name;
        this.sublevel = sublevel;
        this.desc=desc;
        this.image=image;
        this.count=count;
        this.drill_id_tags=drill_id_tags;
        this.technique_id_tags=technique_id_tags;
        this.searchtype=searchtype;
        this.url=url;
        this.video_status=video_status;
    }

    public String getVideo_status() {
        return video_status;
    }

    public void setVideo_status(String video_status) {
        this.video_status = video_status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSearchtype() {
        return searchtype;
    }

    public void setSearchtype(String searchtype) {
        this.searchtype = searchtype;
    }

    public ArrayList<HashMap<String, String>> getDrill_id_tags() {
        return drill_id_tags;
    }

    public void setDrill_id_tags(ArrayList<HashMap<String, String>> drill_id_tags) {
        this.drill_id_tags = drill_id_tags;
    }

    public ArrayList<HashMap<String, String>> getTechnique_id_tags() {
        return technique_id_tags;
    }

    public void setTechnique_id_tags(ArrayList<HashMap<String, String>> technique_id_tags) {
        this.technique_id_tags = technique_id_tags;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSublevel() {
        return sublevel;
    }

    public void setSublevel(String sublevel) {
        this.sublevel = sublevel;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
