package com.kickoffelite.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.kickoffelite.R;
import com.kickoffelite.activity.LevelActivity;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.model.HomeLevelModel;

import java.util.ArrayList;


public class SlidingImage_Adapter extends PagerAdapter {


    private ArrayList<HomeLevelModel> levellist;
    private LayoutInflater inflater;
    private Context context;


    public SlidingImage_Adapter(Context context,ArrayList<HomeLevelModel> levellist) {
        this.context = context;
        this.levellist=levellist;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return levellist.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.sliding_images, view, false);
        assert imageLayout != null;
        RelativeLayout mainlayout=(RelativeLayout)imageLayout.findViewById(R.id.mainlayout);
        Constants.overrideFonts(context,mainlayout);

        final SelectableRoundedImageView imageView = (SelectableRoundedImageView) imageLayout
                .findViewById(R.id.image);
        final TextView txt_level=(TextView)imageLayout.findViewById(R.id.txt_level);
        txt_level.setText("Level-"+(position+1));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setClipToOutline(true);
        }
        //imageView.setImageResource(levellist.get(position));

        view.addView(imageLayout, 0);

        imageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, LevelActivity.class);
                intent.putExtra(Constants.level_name,txt_level.getText().toString());
                //intent.putExtra(Constants.image,IMAGES.get(position));
                context.startActivity(intent);
            }
        });
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}