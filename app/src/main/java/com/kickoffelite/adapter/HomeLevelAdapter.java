package com.kickoffelite.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.kickoffelite.R;
import com.kickoffelite.activity.LevelActivity;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.model.HomeLevelModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class HomeLevelAdapter extends PagerAdapter {

    private ArrayList<HomeLevelModel> levellist;
    private LayoutInflater inflater;
    private Context context;
    Dialog progressDialog;
    SharedPreferences sp;
    SharedPreferences.Editor ed;


    public HomeLevelAdapter(Context context,ArrayList<HomeLevelModel> levellist) {
        this.context = context;
        this.levellist=levellist;
        inflater = LayoutInflater.from(context);
        sp=context.getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return levellist.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.sliding_images, view, false);
        assert imageLayout != null;
        RelativeLayout mainlayout=(RelativeLayout)imageLayout.findViewById(R.id.mainlayout);
        Constants.overrideFonts(context,mainlayout);
        final HomeLevelModel homelevel = levellist.get(position);
        final SelectableRoundedImageView imageView = (SelectableRoundedImageView) imageLayout
                .findViewById(R.id.image);
        final TextView txt_level=(TextView)imageLayout.findViewById(R.id.txt_level);
        txt_level.setText(homelevel.getName());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setClipToOutline(true);
        }
        view.addView(imageLayout, 0);
        String image=homelevel.getImage();
        String imageurl="";
        Log.e( "instantiateItem: ",image );
        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                imageurl=image;
            }
            else
            {
                imageurl= GlobalConstants.ImageUrl+"/"+image;
            }
            Picasso.with(context).load(imageurl).into(imageView);
        }
        else
        {
            Picasso.with(context).load(R.color.grayback).into(imageView);
        }
       // Picasso.with(context).load(homelevel.getImage()).into(imageView);
        imageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, LevelActivity.class);
                intent.putExtra(Constants.name,homelevel.getName());
                intent.putExtra(Constants.image,homelevel.getImage());
                intent.putExtra(Constants.subcategory_id,homelevel.getSublevel_id());
                intent.putExtra(Constants.is_locked,homelevel.getIs_locked());
                intent.putExtra(Constants.desc,homelevel.getDescription());
                intent.putExtra(Constants.total_session,homelevel.getSession_count());
                intent.putExtra(Constants.completed_session,homelevel.getComplete_session());
                intent.putExtra(Constants.level_name,homelevel.getLevelname());
               // intent.putExtra(Constants.video_status,homelevel.get());
                context.startActivity(intent);
            }
        });
        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}