package com.kickoffelite.tabactivityactivities;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.kickoffelite.R;
import com.kickoffelite.adapter.FavoriteAdapter;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.model.FavoriteModel;
import com.kickoffelite.helper.GlobalConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FavoriteActivity extends AppCompatActivity
{
    Dialog progressDialog;
    SharedPreferences sp;
    SharedPreferences.Editor ed;
    ArrayList<HashMap<String,String>> levellist = new ArrayList<>();
    RecyclerView recyclerView;
    FavoriteAdapter favoriteAdapter;
    ArrayList<HashMap<String, String>> drill_list_tags=new ArrayList<>();
    HashMap<String, String> drill_hash_tags = new HashMap<String, String>();
    ArrayList<HashMap<String, String>> technique_list_tags=new ArrayList<>();
    HashMap<String, String> technique_hash_tags = new HashMap<String, String>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        init();
    }

    public void init()
    {
        sp=getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
        recyclerView=(RecyclerView)findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if(Constants.isNetworkAvailable(getApplicationContext()))
        {
            Constants.progressBarDialog(FavoriteActivity.this);
            getFavorites();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    public void setListeners()
    {

    }

    @Override
    protected void onRestart()
    {
        super.onRestart();
        if(Constants.isNetworkAvailable(getApplicationContext()))
        {
            Constants.progressBarDialog(FavoriteActivity.this);
            getFavorites();
        }
        else
        {
            Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_SHORT).show();
        }
    }

    public void getFavorites() {
        final ArrayList<HashMap<String, String>> drill_list_tags=new ArrayList<>();
        final ArrayList<HashMap<String, String>> technique_list_tags=new ArrayList<>();
        final List<FavoriteModel> arrayList = new ArrayList<>();
        final String URL = GlobalConstants.GetFavorite+sp.getString(Constants.id,"");
        StringRequest request = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String s) {
                Log.e("getFavorites", s);
                try {
                    JSONObject jsob = new JSONObject(s);
                    String response = jsob.getString("status");
                    if (response.equals("1"))
                    {
                        Constants.hideLoader();
                        Log.e("getFavorites", response);
                        JSONArray jsonarr = jsob.getJSONArray("data");
                        if(arrayList.size()==0) {
                            for(int i=0;i<jsonarr.length();i++)
                            {
                                JSONObject jsonobj=jsonarr.getJSONObject(i);
                                String id= jsonobj.getString("id");
                                String video_id= jsonobj.getString("video_id");
                                String video_name= jsonobj.getString("video_name");
                                String description = jsonobj.getString("description");
                                String video_url = jsonobj.getString("video_url");
                                String video_thumbnail= jsonobj.getString("video_thumbnail");
                                String level_id = jsonobj.getString("level_id");
                                String level_no = jsonobj.getString("level_no");
                                String video_time = jsonobj.getString("video_time");
                                String is_locked = jsonobj.getString("is_locked");
                                String video_status = jsonobj.getString("video_status");
                                String level_name = jsonobj.getString("level_name");
                                JSONObject jsontag=jsonobj.getJSONObject("tags");

                                if(jsontag.has("techniques"))
                                {
                                    JSONArray jstech=jsontag.getJSONArray("techniques");
                                    for(int j=0;j<jstech.length();j++)
                                    {
                                        JSONObject jsontech=jstech.getJSONObject(j);
                                        String technique_id_tag = jsontech.getString("id");
                                        String technique_name_tag = jsontech.getString("name");
                                        String technique_description = jsontech.getString("description");
                                        String technique_image = jsontech.getString("image");
                                        String session_count = jsontech.getString("session_count");
                                        String is_lockedd = jsontech.getString("is_locked");
                                        String complete_session = jsontech.getString("complete_session");
                                        String tech_level_id = jsontech.getString("level_id");
                                        String tech_sublevel_id = jsontech.getString("sublevel_id");
                                        technique_hash_tags.put("tech_id", technique_id_tag);
                                        technique_hash_tags.put("tech_name", technique_name_tag);
                                        technique_hash_tags.put("tech_desc", technique_description);
                                        technique_hash_tags.put("tech_image", technique_image);
                                        technique_hash_tags.put("tech_session", session_count);
                                        technique_hash_tags.put("tech_locked", is_lockedd);
                                        technique_hash_tags.put("tech_complete", complete_session);
                                        technique_hash_tags.put("tech_level_id", tech_level_id);
                                        technique_hash_tags.put("tech_sublevel_id", tech_sublevel_id);
                                        technique_list_tags.add(technique_hash_tags);
                                        technique_hash_tags = new HashMap<String, String>();
                                    }
                                }

                                else
                                {
                                }

                                if(jsontag.has("drills"))
                                {
                                    JSONArray jstech=jsontag.getJSONArray("drills");
                                    for(int j=0;j<jstech.length();j++)
                                    {
                                        JSONObject jsontech=jstech.getJSONObject(j);
                                        String drill_id_tag = jsontech.getString("id");
                                        String drill_name_tag = jsontech.getString("name");
                                        String drill_description = jsontech.getString("description");
                                        String drill_image = jsontech.getString("image");
                                        String session_count = jsontech.getString("session_count");
                                        String is_lockedd = jsontech.getString("is_locked");
                                        String complete_session = jsontech.getString("complete_session");
                                        String drill_level_id = jsontech.getString("level_id");
                                        String drill_sublevel_id = jsontech.getString("sublevel_id");

                                        drill_hash_tags.put("drill_id", drill_id_tag);
                                        drill_hash_tags.put("drill_name", drill_name_tag);
                                        drill_hash_tags.put("drill_desc", drill_description);
                                        drill_hash_tags.put("drill_image", drill_image);
                                        drill_hash_tags.put("drill_session", session_count);
                                        drill_hash_tags.put("drill_locked", is_lockedd);
                                        drill_hash_tags.put("drill_complete", complete_session);
                                        drill_hash_tags.put("drill_level_id", drill_level_id);
                                        drill_hash_tags.put("drill_sublevel_id", drill_sublevel_id);
                                        drill_list_tags.add(drill_hash_tags);
                                        drill_hash_tags = new HashMap<String, String>();
                                    }
                                }

                                else
                                {

                                }

                                Log.e("urlswipeimage",video_thumbnail);
                                arrayList.add(new FavoriteModel(drill_list_tags,technique_list_tags,id,video_id,video_name,description,video_url,
                                       video_thumbnail,level_id,level_no,video_time,is_locked,video_status,level_name));
                            }
                        }
                        favoriteAdapter = new FavoriteAdapter(FavoriteActivity.this, arrayList);
                        favoriteAdapter.notifyDataSetChanged();
                        recyclerView.setAdapter(favoriteAdapter);

                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"No Favorite Video",Toast.LENGTH_SHORT).show();
                        Constants.hideLoader();
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Constants.hideLoader();
                Toast.makeText(FavoriteActivity.this, "Some error occurred -> " + volleyError, Toast.LENGTH_LONG).show();

            }
        }) ;
        request.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue rQueue = Volley.newRequestQueue(FavoriteActivity.this);
        rQueue.add(request);
    }

    public void progressBarDialog()
    {
        progressDialog= new Dialog(this);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.findViewById(R.id.loader_view).setVisibility(View.VISIBLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }
}
