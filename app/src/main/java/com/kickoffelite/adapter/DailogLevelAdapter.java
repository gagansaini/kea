package com.kickoffelite.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kickoffelite.R;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.model.DailogLevelModel;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class DailogLevelAdapter extends RecyclerView.Adapter<DailogLevelAdapter.MyViewHolder>
{
    private List<DailogLevelModel> moviesList;
    ItemClickInterface onclick;
    Context context;
    SharedPreferences sp;
    SharedPreferences.Editor ed;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView txt_level, txt_current;
        ImageView img_level,img_current;
        RelativeLayout rel_level;
        View view1;

        public MyViewHolder(View view)
        {
            super(view);
            txt_level = (TextView) view.findViewById(R.id.txt_level);
            img_current = (ImageView) view.findViewById(R.id.img_current);
            img_level = (ImageView) view.findViewById(R.id.img_level);
            rel_level = (RelativeLayout) view.findViewById(R.id.rel_level);
            view1 = (View) view.findViewById(R.id.view);
        }
    }

    public DailogLevelAdapter(Context context, List<DailogLevelModel> moviesList,ItemClickInterface onclick)
    {
        this.onclick=onclick;
        this.moviesList = moviesList;
        this.context = context;
        sp=context.getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
    }

    @Override
    public DailogLevelAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.level_dialog, parent, false);
        return new DailogLevelAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DailogLevelAdapter.MyViewHolder holder, int position) {

        final DailogLevelModel movie = moviesList.get(position);
        holder.txt_level.setText(movie.getLevel_name());

        if(movie.getIs_locked().equalsIgnoreCase("1"))
        {
           holder.img_level.setVisibility(View.VISIBLE);
           holder.img_current.setVisibility(View.INVISIBLE);
        }

        else
        {
            holder.img_level.setVisibility(View.INVISIBLE);
            holder.txt_level.setTextColor(context.getResources().getColor(R.color.black));
            if(movie.getId().equalsIgnoreCase(sp.getString(Constants.level_id,"")))
            {
                holder.txt_level.setTextColor(context.getResources().getColor(R.color.currenttext));
                holder.img_current.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.txt_level.setTextColor(context.getResources().getColor(R.color.black));
                holder.img_current.setVisibility(View.INVISIBLE);
            }
        }

        if(position==moviesList.size()-1)
        {
            holder.view1.setVisibility(View.INVISIBLE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onclick.onItemClick(movie.getIs_locked(),movie.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


    public interface ItemClickInterface
    {
        void onItemClick(String lock,String levelid);
    }

}