package com.kickoffelite.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.kickoffelite.R;
import com.kickoffelite.activity.PlayVideoActivity;
import com.kickoffelite.activity.SwipeTechActivity;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.model.DownloadModel;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by HP on 5/30/2018.
 */

public class DownloadedAdapter extends RecyclerView.Adapter<DownloadedAdapter.MyViewHolder>
{
    private List<DownloadModel> moviesList;
    Context context;
    SharedPreferences sp;
    SharedPreferences.Editor ed;

    public class MyViewHolder extends RecyclerView.ViewHolder
    {
        public TextView name, time,level;
        SelectableRoundedImageView img_video;
       // ImageView img_detail;
        RelativeLayout mainlayout;

        public MyViewHolder(View view)
        {
            super(view);
            name = (TextView) view.findViewById(R.id.txt_name);
            time = (TextView) view.findViewById(R.id.txt_time);
            level=(TextView)view.findViewById(R.id.txt_level);
            //img_detail=(ImageView)view.findViewById(R.id.img_detail);
            img_video = (SelectableRoundedImageView) view.findViewById(R.id.img_video);
            mainlayout=(RelativeLayout)view.findViewById(R.id.mainlayout);
            Constants.overrideFonts(context,mainlayout);

        }
    }

    public DownloadedAdapter(Context context,List<DownloadModel> moviesList) {
        this.moviesList = moviesList;
        this.context = context;
        sp=context.getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
    }

    @Override
    public DownloadedAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.downloaded_recycler, parent, false);

        return new DownloadedAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final DownloadedAdapter.MyViewHolder holder, int position) {
        final DownloadModel movie = moviesList.get(position);
        holder.time.setText(movie.getTime()+" min");
        holder.level.setText(movie.getLevelname());
        String image=movie.getImage();
//        String imageurl="";
//        if(!image.equalsIgnoreCase(""))
//        {
//            if(image.contains("http"))
//            {
//                imageurl=image;
//            }
//            else
//            {
//                imageurl= GlobalConstants.ImageUrl+"/"+image;
//            }
//            Picasso.with(context).load(imageurl).into(holder.img_video);
//        }
//        else
//        {
           // Picasso.with(context).load(image).into(holder.img_video);
        //}
        Picasso.with(context).load(new File(movie.getImage())).into(holder.img_video);
        holder.name.setText(movie.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent=new Intent(context,PlayVideoActivity.class);
                intent.putExtra(Constants.name,movie.getName());
                intent.putExtra(Constants.url,movie.getVideourl());
                intent.putExtra(Constants.image,movie.getImage());
                intent.putExtra(Constants.desc,movie.getDesc());
                intent.putExtra(Constants.VideoType,"download");
                context.startActivity(intent);
            }
        });
//        holder.img_detail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(context, DetailsActivity.class);
//                intent.putExtra(Constants.name,movie.getName());
//                intent.putExtra(Constants.totaltime,movie.getTime());
//                intent.putExtra(Constants.level_name,movie.getLevelname());
//                intent.putExtra(Constants.desc,movie.getDesc());
////                intent.putStringArrayListExtra("drilltagslist",movie.getDrill_id_tags());
////                intent.putStringArrayListExtra("techniquetagslist",movie.getTechnique_id_tags());
//                context.startActivity(intent);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}