package com.kickoffelite.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.joooonho.SelectableRoundedImageView;
import com.kickoffelite.R;
import com.kickoffelite.activity.SwipeTechActivity;
import com.kickoffelite.helper.Constants;
import com.kickoffelite.helper.GlobalConstants;
import com.kickoffelite.model.HomeTechModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;

public class HomeTechniqueAdapter extends PagerAdapter {

    private ArrayList<HomeTechModel> techlist;
    private LayoutInflater inflater;
    private Context context;
    SharedPreferences sp;
    SharedPreferences.Editor ed;


    public HomeTechniqueAdapter(Context context, ArrayList<HomeTechModel> techlist) {
        this.context = context;
        this.techlist=techlist;
        inflater = LayoutInflater.from(context);
        sp=context.getSharedPreferences(Constants.appname,MODE_PRIVATE);
        ed=sp.edit();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return techlist.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.home_technique_item, view, false);
        assert imageLayout != null;
        RelativeLayout mainlayout=(RelativeLayout)imageLayout.findViewById(R.id.mainlayout);
        Constants.overrideFonts(context,mainlayout);
        final HomeTechModel hometech = techlist.get(position);
        final SelectableRoundedImageView imageView = (SelectableRoundedImageView) imageLayout.findViewById(R.id.image);
        final TextView txttitle=(TextView)imageLayout.findViewById(R.id.txt_title);
        final TextView txtcomplete=(TextView)imageLayout.findViewById(R.id.txt_completed);
        final TextView txttotal=(TextView)imageLayout.findViewById(R.id.txt_total);
        SeekBar seekbar=(SeekBar)imageLayout.findViewById(R.id.seekbar);
        txttitle.setText(hometech.getName());
        txttotal.setText(hometech.getSession_count()+" Sessions");
        txtcomplete.setText(hometech.getComplete_session()+"/");
        seekbar.setMax(Integer.parseInt(hometech.getSession_count()));
        seekbar.setProgress(Integer.parseInt(hometech.getComplete_session()));
        seekbar.setFocusable(false);
        seekbar.setEnabled(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            imageView.setClipToOutline(true);
        }
        view.addView(imageLayout, 0);
        String image=hometech.getImage();
        String imageurl="";

        if(!image.equalsIgnoreCase(""))
        {
            if(image.contains("http"))
            {
                imageurl=image;
            }
            else
            {
                imageurl= GlobalConstants.ImageUrl+"/"+image;
            }
            Picasso.with(context).load(imageurl).into(imageView);
        }
        else
        {
            Picasso.with(context).load(R.color.grayback).into(imageView);
        }
       // Picasso.with(context).load(hometech.getImage()).into(imageView);
        imageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, SwipeTechActivity.class);
                intent.putExtra(Constants.TechId,hometech.getId());
                intent.putExtra(Constants.pgetitle,hometech.getName());
                intent.putExtra(Constants.TechName,hometech.getName());
                intent.putExtra(Constants.TechImage,hometech.getImage());
                intent.putExtra(Constants.TechDesc,hometech.getDescription());
                intent.putExtra(Constants.TechSession,hometech.getSession_count());
                intent.putExtra(Constants.completed_session,hometech.getComplete_session());
                intent.putExtra("name","");
                context.startActivity(intent);
            }
        });

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}